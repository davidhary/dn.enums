# Open Source

This is a fork of the [dn.core], which was forked from [FastEnum].

* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Open-Source"></a>
## Open Source
Open source used by this software is described and licensed at the
following sites:  
[enums]  
[Enumeration Extensions]  
[Enum.Net]
[Exception Extension]  
[FastEnum]
[String Enumerator]
[Unconstrained Melody]

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  

[enums]: https://www.bitbucket.org/davidhary/dn.enums
[Enum.Net]: https://github.com/TylerBrinkley/Enums.NET
[Enumeration Extensions]: https://www.codeproject.com/Articles/37921/Enums-Flags-and-Csharp-Oh-My-Bad-Pun
[Exception Extension]: https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
[FastEnum]: https://github.com/xin9le/FastEnum
[String Enumerator]: http://www.codeproject.com/Articles/17472/StringEnumerator
[Unconstrained Melody]: https://github.com/jskeet/unconstrained-melody
[dn.core]: https://www.bitbucket.org/davidhary/dn.core
