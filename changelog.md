# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2.1.9083] - 2024-11-13 Preview 202304
* Update packages.
* Increment version to 9083.
* Upgrade Forms libraries and Test and Demo apps to .Net 9.0.

## [2.1.8956] - 2024-07-09 Preview 202304
* Set projects to target .NET Standard 2.0 alone.
* Update .NET standard 2.0 enhanced methods and attributes.

## [2.1.8949] - 2024-07-02 Preview 202304
* Change MSTest SDK to 3.4.3.
* Add using unit testing to the MS Test project files.
* Update libraries versions.
* Apply code analysis rules.
* Use ordinal instead of ordinal ignore case string comparisons.
* Apply constants style.
* Condition auto generating binding redirects on .NET frameworks 472 and 480.
* Apply code analysis rules.
* Generate assembly version attributes.
* Add toEnum( long ).

## [2.1.8935] - 2024-06-18 Preview 202304
* Apply code analysis rules.

## [2.1.8935] - 2024-06-18 Preview 202304
* Apply code analysis rules.
* Use MSTest SDK.

## [2.1.8934] - 2024-06-17 Preview 202304
* Update to .Net 8.
* Implement MS Test SDK project format.

## [2.1.8534] - 2023-05-16 Preview 202304
* Update documentations.

## [2.1.8523] - 2023-04-28 Preview 202304
* Split README.MD to attribution, cloning, open-source and read me files.
* Add code of conduct, contribution and security documents.

## [2.0.8502] - 2023-04-12 Preview 202304
* Uses cc.isr.Enums namespace.

## [1.7.8486] - 2023-03-27
* update project files. 

## [1.7.8125] - 2022-03-31
* Reference System.Runtime.CompilerServices.Unsafe revision 6.0.0 in project 
reference mode.
* Pass tests in project reference mode. 

## [1.7.8102] - 2022-03-08
* Forked from [dn.core].

&copy; 2012 Integrated Scientific Resources, Inc. All rights reserved.

[dn.core]: https://bitbucket.org/davidhary/dn.core
[2.1.9083]: https://bitbucket.org/davidhary/dn.enums
