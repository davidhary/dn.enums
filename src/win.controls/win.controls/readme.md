# About

[cc.isr.Enums.WinControls] is a .Net library providing Windows Forms enumeration 
extension methods for controls.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

[cc.isr.Enums.WinControls] is released as open source under the MIT license.
Bug reports and contributions are welcome at the [cc.isr.Enums] Repository.

[cc.isr.Enums]: https://bitbucket.org/davidhary/dn.enums
[cc.isr.Enums.WinControls]: https://bitbucket.org/davidhary/dn.enums/src/win.controls/
