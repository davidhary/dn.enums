using System.Diagnostics;
using cc.isr.Enums.WinControls.ComboBoxEnumExtensions;

namespace cc.isr.Enums.WinControls.MSTest;

/// <summary> Enum extensions tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-03-14 </para>
/// </remarks>
[TestClass]
public class EnumExtensionsTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}";
            Console.WriteLine( methodFullName );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    { }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    { }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    { }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " combo box tests "

    /// <summary> (Unit Test Method) tests combo box trace event type enum. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod]
    public void ComboBoxTraceEventTypeEnumTest()
    {
        TraceEventType includeMask = TraceEventType.Critical | TraceEventType.Error | TraceEventType.Information | TraceEventType.Verbose | TraceEventType.Warning;
        TraceEventType excludeMask = TraceEventType.Critical;
        using Form panel = new();
        using ComboBox comboBox = new();
        // comboBox.CreateControl()
        panel.Controls.Add( comboBox );
        // panel.PerformLayout()
        // comboBox.PerformLayout()

        // list enum names
        comboBox.ListEnumNames( includeMask, excludeMask );
        int expectedItemCount = 4;
        Assert.AreEqual( expectedItemCount, comboBox.Items.Count, "Expected item count" );

        // selected item
        comboBox.SelectedIndex = 0;
        TraceEventType expectedValue = TraceEventType.Error;
        KeyValuePair<TraceEventType, string> actualItem = comboBox.SelectedEnumItem<TraceEventType>();
        Assert.AreEqual( expectedValue, actualItem.Key, "Selected item at index zero" );

        // select value
        expectedValue = TraceEventType.Information;
        actualItem = comboBox.SelectValue( expectedValue );
        Assert.AreEqual( expectedValue, actualItem.Key, "Selected item by value" );
        actualItem = comboBox.SelectedEnumItem<TraceEventType>();
        Assert.AreEqual( expectedValue, actualItem.Key, "Selected item by key value pair" );
        Assert.AreEqual( expectedValue, comboBox.SelectedEnumValue<TraceEventType>(), "Generic selected value" );
        Assert.AreEqual( expectedValue, comboBox.SelectedEnumValue<TraceEventType>( TraceEventType.Warning ), "Generic selected value with default" );
    }

    /// <summary> (Unit Test Method) tests combo box notify synchronize level enum. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod]
    public void ComboBoxNotifySyncLevelEnumTest()
    {
        NotifySyncLevel includeMask = NotifySyncLevel.Async | NotifySyncLevel.Sync;
        NotifySyncLevel excludeMask = NotifySyncLevel.None;
        using Form panel = new();
        using ComboBox comboBox = new();
        // comboBox.CreateControl()
        panel.Controls.Add( comboBox );
        // panel.PerformLayout()
        // comboBox.PerformLayout()

        // list enum names
        comboBox.ListEnumNames( includeMask, excludeMask );
        int expectedItemCount = 2;
        Assert.AreEqual( expectedItemCount, comboBox.Items.Count, "Expected item count" );

        // selected item
        comboBox.SelectedIndex = 0;
        NotifySyncLevel expectedValue = NotifySyncLevel.Sync;
        KeyValuePair<NotifySyncLevel, string> actualItem = comboBox.SelectedEnumItem<NotifySyncLevel>();
        Assert.AreEqual( expectedValue, actualItem.Key, "Selected item at index zero" );

        // select value
        expectedValue = NotifySyncLevel.Async;
        actualItem = comboBox.SelectValue( expectedValue );
        Assert.AreEqual( expectedValue, actualItem.Key, "Selected item by value" );
        actualItem = comboBox.SelectedEnumItem<NotifySyncLevel>();
        Assert.AreEqual( expectedValue, actualItem.Key, "Selected item by key value pair" );
        Assert.AreEqual( expectedValue, comboBox.SelectedEnumValue<NotifySyncLevel>(), "Generic selected value" );
        Assert.AreEqual( expectedValue, comboBox.SelectedEnumValue<NotifySyncLevel>( NotifySyncLevel.None ), "Generic selected value with default" );
    }

    /// <summary>
    /// (Unit Test Method) tests tool strip combo box notify synchronize level enum.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod]
    public void ToolStripComboBoxNotifySyncLevelEnumTest()
    {
        NotifySyncLevel includeMask = NotifySyncLevel.Async | NotifySyncLevel.Sync;
        NotifySyncLevel excludeMask = NotifySyncLevel.None;
        using Form panel = new();
        using ToolStrip toolStrip = new();
        panel.Controls.Add( toolStrip );
        using ToolStripComboBox toolStripComboBox = new();
        _ = toolStrip.Items.Add( toolStripComboBox );
        toolStrip.ResumeLayout();
        toolStrip.PerformLayout();
        toolStrip.Invalidate();
        // list enum names
        toolStripComboBox.ComboBox.ListEnumNames( includeMask, excludeMask );
        int expectedItemCount = 2;
        Assert.AreEqual( expectedItemCount, toolStripComboBox.Items.Count, "Expected item count" );

        // selected item
        toolStripComboBox.SelectedIndex = 0;
        NotifySyncLevel expectedValue = NotifySyncLevel.Sync;
        KeyValuePair<NotifySyncLevel, string> actualItem = toolStripComboBox.SelectedEnumItem<NotifySyncLevel>();
        Assert.AreEqual( expectedValue, actualItem.Key, "Selected item at index zero" );

        // select value
        expectedValue = NotifySyncLevel.Async;
        actualItem = toolStripComboBox.SelectValue( expectedValue );
        Assert.AreEqual( expectedValue, actualItem.Key, "Selected item by value" );
        actualItem = toolStripComboBox.SelectedEnumItem<NotifySyncLevel>();
        Assert.AreEqual( expectedValue, actualItem.Key, "Selected item by key value pair" );
        Assert.AreEqual( expectedValue, toolStripComboBox.SelectedEnumValue<NotifySyncLevel>(), "Generic selected value" );
        Assert.AreEqual( expectedValue, toolStripComboBox.SelectedEnumValue( NotifySyncLevel.None ), "Generic selected value with default" );
    }

    /// <summary> Values that represent notify Synchronization levels. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public enum NotifySyncLevel
    {
        /// <summary> . </summary>
        [System.ComponentModel.Description( "No notification" )]
        None = 0,
        /// <summary> An enum constant representing the sync] option. </summary>
        [System.ComponentModel.Description( "Synchronize" )]
        Sync,
        /// <summary> An enum constant representing the async] option. </summary>
        [System.ComponentModel.Description( "A-Synchronize" )]
        Async
    }

    #endregion
}
