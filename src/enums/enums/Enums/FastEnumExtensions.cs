using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace cc.isr.Enums;
/// <summary>
/// Provides <see cref="Enum"/> extension methods.
/// </summary>
public static partial class FastEnumExtensions
{
    /// <summary>
    /// Converts to the member information of the constant in the specified enumeration value.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static Member<T> ToMember<T>( this T value ) where T : struct, Enum
    {
        return FastEnum.GetMember( value );
    }

    /// <summary>
    /// Converts to the name of the constant in the specified enumeration value.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static string ToName<T>( this T value ) where T : struct, Enum
    {
        return FastEnum.GetName( value );
    }

    /// <summary>
    /// Returns an indication whether a constant with a specified value exists in a specified enumeration.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static bool IsDefined<T>( this T value )
        where T : struct, Enum
    {
        return FastEnum.IsDefined( value );
    }

    /// <summary>
    /// Gets the <see cref="EnumMemberAttribute.Value"/> of specified enumeration member.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <param name="throwIfNotFound"></param>
    /// <returns></returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static string? GetEnumMemberValue<T>( this T value, bool throwIfNotFound = true )
        where T : struct, Enum
    {
        EnumMemberAttribute? attribute = value.ToMember().EnumMemberAttribute;
        return attribute is not null
            ? attribute.Value
            : throwIfNotFound
            ? throw new NotFoundException( $"{nameof( EnumMemberAttribute )} is not found.", nameof( EnumMemberAttribute ) )
            : default;
    }

    /// <summary>
    /// Gets the <see cref="LabelAttribute.Value"/> of specified enumeration member.
    /// </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <exception cref="NotFoundException">        Thrown when the requested element is not present. </exception>
    /// <typeparam name="T">    . </typeparam>
    /// <param name="member">           . </param>
    /// <param name="index">            (Optional) Zero-based index of the. </param>
    /// <param name="throwIfNotFound">  (Optional) </param>
    /// <returns>   The label. </returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static string? GetLabel<T>( this Member<T> member, int index = 0, bool throwIfNotFound = true )
        where T : struct, Enum
    {
        return member is null
            ? throw new ArgumentNullException( nameof( member ) )
            : member.Labels.TryGetValue( index, out string? label )
              ? label
              : throwIfNotFound
                ? throw new NotFoundException( $"{nameof( LabelAttribute )} that is specified index {index} is not found.", nameof( LabelAttribute ) )
                : default;
    }

    /// <summary>
    /// Gets the <see cref="LabelAttribute.Value"/> of specified enumeration member.
    /// </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="T">    . </typeparam>
    /// <param name="value">            . </param>
    /// <param name="index">            (Optional) Zero-based index of the. </param>
    /// <param name="throwIfNotFound">  (Optional) </param>
    /// <returns>   The label. </returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static string? GetLabel<T>( this T value, int index = 0, bool throwIfNotFound = true )
        where T : struct, Enum
    {
        return value.ToMember().GetLabel( index, throwIfNotFound );
    }
}
