using System.Runtime.Serialization;

namespace cc.isr.Enums;
/// <summary>
/// Represents an exception that is thrown when not found something.
/// </summary>
public sealed class NotFoundException : Exception
{
    /// <summary>   Creates instance. </summary>
    /// <remarks>   2023-04-17. </remarks>
    /// <param name="message">      . </param>
    /// <param name="elementName">  The name of the element. </param>
    internal NotFoundException( string message, string elementName ) : base( message ) => this.ElementName = elementName;

    /// <summary>   Specialized constructor for use only by derived class. </summary>
    /// <remarks>   David, 2021-03-22. </remarks>
    /// <param name="info">     The information. </param>
    /// <param name="context"> The <see cref="StreamingContext" />
    /// that contains contextual information about the source or destination.
    /// </param>
#if NET8_0_OR_GREATER
#pragma warning disable CA1041 // Provide ObsoleteAttribute message
    [Obsolete( DiagnosticId = "SYSLIB0051" )] // add this attribute to the serialization ctor
#pragma warning restore CA1041 // Provide ObsoleteAttribute message
#endif
    internal NotFoundException( SerializationInfo info, StreamingContext context ) : base( info, context ) => this.ElementName = ( string ) info.GetValue( nameof( this.ElementName ), typeof( string ) );

    /// <summary>   Gets or sets the name of the element. </summary>
    /// <value> The name of the element. </value>
    public string ElementName { get; set; }

    /// <summary>
    /// When overridden in a derived class, sets the <see cref="SerializationInfo">
    /// </see> with information about the exception.
    /// </summary>
    /// <remarks>   2023-04-17. </remarks>
    /// <param name="info">     The <see cref="SerializationInfo"></see>
    ///                         that holds the serialized object data about the exception being
    ///                         thrown. </param>
    /// <param name="context">  The <see cref="StreamingContext"></see>
    ///                         that contains contextual information about the source or destination. </param>
#if NET8_0_OR_GREATER
#pragma warning disable CA1041 // Provide ObsoleteAttribute message
    [Obsolete( DiagnosticId = "SYSLIB0051" )] // add this attribute to the serialization ctor
#pragma warning restore CA1041 // Provide ObsoleteAttribute message
#endif
    public override void GetObjectData( SerializationInfo info, StreamingContext context )
    {
        if ( info is not null )
        {
            info.AddValue( $"{nameof( NotFoundException.ElementName )}", this.ElementName );
            base.GetObjectData( info, context );
        }
    }


}
