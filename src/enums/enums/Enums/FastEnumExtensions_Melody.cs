namespace cc.isr.Enums;

/// <summary>   Provides high performance utilities for enum type. </summary>
/// <remarks>   David, 2021-02-20. </remarks>
public static partial class FastEnumExtensions
{
    /// <summary>   Determines whether the given value only uses bits covered by named values. </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="values">   Combination to test. </param>
    /// <returns>   True if valid combination, false if not. </returns>
    /// <exception cref="TypeArgumentException">    <typeparamref name="TEnum"/> is not a flags enum. </exception>
    public static bool IsValidCombination<TEnum>( this TEnum values ) where TEnum : struct, Enum
    {
        FastEnum.ThrowIfNotFlags<TEnum>();
        return values.And( FastEnum.EnumInternals<TEnum>.UnusedBits ).IsEmpty();
    }

    /// <summary>   Determines whether the two specified values have any flags in common. </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">        Value to test. </param>
    /// <param name="desiredFlags"> Flags we wish to find. </param>
    /// <returns>   Whether the two specified values have any flags in common. </returns>
    /// <exception cref="TypeArgumentException">    <typeparamref name="TEnum"/> is not a flags enum. </exception>
    public static bool HasAny<TEnum>( this TEnum value, TEnum desiredFlags ) where TEnum : struct, Enum
    {
        FastEnum.ThrowIfNotFlags<TEnum>();
        return value.And( desiredFlags ).IsNotEmpty();
    }

    /// <summary>   Determines whether all of the flags in <paramref name="desiredFlags"/> </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Generic type parameter. </typeparam>
    /// <param name="value">        Value to test. </param>
    /// <param name="desiredFlags"> Flags we wish to find. </param>
    /// <returns>
    /// Whether all the flags in <paramref name="desiredFlags"/> are in <paramref name="value"/>.
    /// </returns>
    /// <exception cref="TypeArgumentException">    <typeparamref name="TEnum"/> is not a flags enum. </exception>
    public static bool HasAll<TEnum>( this TEnum value, TEnum desiredFlags ) where TEnum : struct, Enum
    {
        FastEnum.ThrowIfNotFlags<TEnum>();
        return FastEnum.EnumInternals<TEnum>.Equality( value.And( desiredFlags ), desiredFlags );
    }

    /// <summary>   Returns the bitwise "and" of two values. </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="first">    First value. </param>
    /// <param name="second">   Second value. </param>
    /// <returns>   The bitwise "and" of the two values. </returns>
    /// <exception cref="TypeArgumentException">    <typeparamref name="TEnum"/> is not a flags enum. </exception>
    public static TEnum And<TEnum>( this TEnum first, TEnum second ) where TEnum : struct, Enum
    {
        FastEnum.ThrowIfNotFlags<TEnum>();
        return FastEnum.EnumInternals<TEnum>.And( first, second );
    }

    /// <summary>   Returns the bitwise "or" of two values. </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="first">    First value. </param>
    /// <param name="second">   Second value. </param>
    /// <returns>   The bitwise "or" of the two values. </returns>
    /// <exception cref="TypeArgumentException">    <typeparamref name="TEnum"/> is not a flags enum. </exception>
    public static TEnum Or<TEnum>( this TEnum first, TEnum second ) where TEnum : struct, Enum
    {
        FastEnum.ThrowIfNotFlags<TEnum>();
        return FastEnum.EnumInternals<TEnum>.Or( first, second );
    }

    /// <summary>
    /// Returns the inverse of a value, with no consideration for which bits are used by values
    /// within the enum (i.e. a simple bitwise negation).
    /// </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Enum type. </typeparam>
    /// <param name="value">    Value to invert. </param>
    /// <returns>   The bitwise negation of the value. </returns>
    /// <exception cref="TypeArgumentException">    <typeparamref name="TEnum"/> is not a flags enum. </exception>
    public static TEnum AllBitsInverse<TEnum>( this TEnum value ) where TEnum : struct, Enum
    {
        FastEnum.ThrowIfNotFlags<TEnum>();
        return FastEnum.EnumInternals<TEnum>.Not( value );
    }

    /// <summary>
    /// Returns the inverse of a value, but limited to those bits which are used by values within the
    /// enum.
    /// </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Enum type. </typeparam>
    /// <param name="value">    Value to invert. </param>
    /// <returns>   The restricted inverse of the value. </returns>
    /// <exception cref="TypeArgumentException">    <typeparamref name="TEnum"/> is not a flags enum. </exception>
    public static TEnum UsedBitsInverse<TEnum>( this TEnum value ) where TEnum : struct, Enum
    {
        FastEnum.ThrowIfNotFlags<TEnum>();
        return value.AllBitsInverse().And( FastEnum.EnumInternals<TEnum>.UsedBits );
    }

    /// <summary>   Returns whether this value is an empty set of fields, i.e. the zero value. </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Enum type. </typeparam>
    /// <param name="value">    Value to test. </param>
    /// <returns>   True if the value is empty (zero); False otherwise. </returns>
    /// <exception cref="TypeArgumentException">    <typeparamref name="TEnum"/> is not a flags enum. </exception>
    public static bool IsEmpty<TEnum>( this TEnum value ) where TEnum : struct, Enum
    {
        FastEnum.ThrowIfNotFlags<TEnum>();
        return FastEnum.EnumInternals<TEnum>.IsEmpty( value );
    }

    /// <summary>   Returns whether this value has any fields set, i.e. is not zero. </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Enum type. </typeparam>
    /// <param name="value">    Value to test. </param>
    /// <returns>   True if the value is non-empty (not zero); False otherwise. </returns>
    /// <exception cref="TypeArgumentException">    <typeparamref name="TEnum"/> is not a flags enum. </exception>
    public static bool IsNotEmpty<TEnum>( this TEnum value ) where TEnum : struct, Enum
    {
        FastEnum.ThrowIfNotFlags<TEnum>();
        return !value.IsEmpty();
    }

    /// <summary>
    /// Checks whether the value is a named value for the type.
    /// </summary>
    /// <remarks>
    /// For flags Enums, it is possible for a value to be a valid
    /// combination of other values without being a named value
    /// in itself. To test for this possibility, use IsValidCombination.
    /// </remarks>
    /// <typeparam name="T">Enum type</typeparam>
    /// <param name="value">Value to test</param>
    /// <returns>True if this value has a name, False otherwise.</returns>
    public static bool IsNamedValue<T>( this T value ) where T : struct, Enum
    {
        // TODO: Speed this up for big Enums
        return FastEnum.GetValues<T>().Contains( value );
    }


}
