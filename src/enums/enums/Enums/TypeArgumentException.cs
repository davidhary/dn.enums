using System.Runtime.Serialization;

namespace cc.isr.Enums;
/// <summary>
/// Exception thrown to indicate that an inappropriate type argument was used for
/// a type parameter to a generic type or method.
/// </summary>
public class TypeArgumentException : Exception
{
    /// <summary>   Constructs a new instance of TypeArgumentException with the given message. </summary>
    /// <remarks>   2023-04-17. </remarks>
    /// <param name="message">  Message for the exception. </param>
    /// <param name="typeName"> The name of the type. </param>
    internal TypeArgumentException( string message, string typeName ) : base( message ) => this.TypeName = typeName;

    /// <summary>   Specialized constructor for use only by derived class. </summary>
    /// <remarks>   David, 2021-03-22. </remarks>
    /// <param name="info">     The information. </param>
    /// <param name="context"> The <see cref="StreamingContext" />
    /// that contains contextual information about the source or destination.
    /// </param>
#if NET8_0_OR_GREATER
#pragma warning disable CA1041 // Provide ObsoleteAttribute message
    [Obsolete( DiagnosticId = "SYSLIB0051" )] // add this attribute to the serialization ctor
#pragma warning restore CA1041 // Provide ObsoleteAttribute message
#endif
    internal TypeArgumentException( SerializationInfo info, StreamingContext context ) : base( info, context ) => this.TypeName = ( string ) info.GetValue( nameof( this.TypeName ), typeof( string ) );

    /// <summary>   Gets or sets the name of the type. </summary>
    /// <value> The name of the type. </value>
    public string TypeName { get; set; }

    /// <summary>
    /// When overridden in a derived class, sets the <see cref="SerializationInfo">
    /// </see> with information about the exception.
    /// </summary>
    /// <remarks>   2023-04-17. </remarks>
    /// <param name="info">     The <see cref="SerializationInfo"></see>
    ///                         that holds the serialized object data about the exception being
    ///                         thrown. </param>
    /// <param name="context">  The <see cref="StreamingContext"></see>
    ///                         that contains contextual information about the source or destination. </param>
#if NET8_0_OR_GREATER
#pragma warning disable CA1041 // Provide ObsoleteAttribute message
    [Obsolete( DiagnosticId = "SYSLIB0051" )] // add this attribute to the serialization ctor
#pragma warning restore CA1041 // Provide ObsoleteAttribute message
#endif
    public override void GetObjectData( SerializationInfo info, StreamingContext context )
    {
        if ( info is not null )
        {
            info.AddValue( $"{nameof( TypeArgumentException.TypeName )}", this.TypeName );
            base.GetObjectData( info, context );
        }
    }
}
