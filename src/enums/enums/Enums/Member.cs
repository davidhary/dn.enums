using System.Reflection;
using System.Runtime.Serialization;
using cc.isr.Enums.Internals;

namespace cc.isr.Enums;
/// <summary>
/// Represents the member information of the constant in the specified enumeration.
/// </summary>
/// <typeparam name="T">Enum type</typeparam>
public sealed partial class Member<T> where T : struct, Enum
{
    #region " properties "

    /// <summary>
    /// Gets the value of specified enumeration member.
    /// </summary>
    public T Value { get; }

    /// <summary>
    /// Gets the name of specified enumeration member.
    /// </summary>
    public string Name { get; }

    /// <summary>
    /// Gets the <see cref="System.Reflection.FieldInfo"/> of specified enumeration member.
    /// </summary>
    public FieldInfo FieldInfo { get; }

    /// <summary>
    /// Gets the <see cref="System.Runtime.Serialization.EnumMemberAttribute"/> of specified enumeration member.
    /// </summary>
    public EnumMemberAttribute? EnumMemberAttribute { get; }

    /// <summary>
    /// Gets the labels of specified enumeration member.
    /// </summary>
    internal FrozenInt32KeyDictionary<string?> Labels { get; }

    #endregion

    #region " constructtion "

    /// <summary>
    /// Creates instance.
    /// </summary>
    /// <param name="name"></param>
    internal Member( string name )
    {
        this.Value
            = Enum.TryParse( name, out T value )
            ? value
            : throw new ArgumentException( "name is not found.", nameof( name ) );
        this.Name = name;
        this.FieldInfo = typeof( T ).GetField( name )!;
        this.EnumMemberAttribute = this.FieldInfo.GetCustomAttribute<EnumMemberAttribute>();
        this.Labels
            = this.FieldInfo
            .GetCustomAttributes<LabelAttribute>()
            .ToFrozenInt32KeyDictionary( x => x.Index, x => x.Value );
        // ISR
        this.DescriptionAttribute = this.FieldInfo.GetCustomAttribute<System.ComponentModel.DescriptionAttribute>();
        this.Description = this.DescriptionAttribute is not null ? this.DescriptionAttribute.Description : string.Empty;
        this.DescriptionOrName = this.DescriptionAttribute is not null ? this.DescriptionAttribute.Description : this.Name;
        this.DescriptionWithin = Member<T>.ExtractBetween( this.Description, '(', ')' );
    }

    /// <summary>
    /// Deconstruct into name and value.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="value"></param>
    public void Deconstruct( out string name, out T value )
    {
        name = this.Name;
        value = this.Value;
    }

    #endregion

    #region " classes "

    /// <summary>
    /// Provides <see cref="IEqualityComparer{T}"/> by <see cref="Value"/>.
    /// </summary>
    internal sealed class ValueComparer : IEqualityComparer<Member<T>>
    {
        #region " iequalitycomparer implementations "
        public bool Equals( Member<T>? x, Member<T>? y )
        {
            return x is null ? y is null : y is not null && EqualityComparer<T>.Default.Equals( x.Value, y.Value );
        }

        public int GetHashCode( Member<T> obj )
        {
            return EqualityComparer<T>.Default.GetHashCode( obj.Value );
        }
        #endregion
    }

    #endregion
}
