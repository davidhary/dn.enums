using System.Runtime.CompilerServices;

namespace cc.isr.Enums;
/// <summary>
/// Provides <see cref="Enum"/> extension methods.
/// </summary>
public static partial class FastEnumExtensions
{
    /// <summary>
    /// Gets the <see cref="System.ComponentModel.DescriptionAttribute"/> of specified enumeration
    /// member.
    /// </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <exception cref="NotFoundException">    Thrown when the requested element is not present. </exception>
    /// <typeparam name="TEnum">  The underlying enum value type. </typeparam>
    /// <param name="value">  The Enum value. </param>
    /// <param name="throwIfNotFound">  (Optional) True to throw if not found. </param>
    /// <returns>   The description value. </returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static string? GetDescriptionValue<TEnum>( this TEnum value, bool throwIfNotFound = false ) where TEnum : struct, Enum
    {
        System.ComponentModel.DescriptionAttribute? attribute = value.ToMember().DescriptionAttribute;
        return attribute is not null
            ? attribute.Description
            : throwIfNotFound
              ? throw new NotFoundException( $"{nameof( System.ComponentModel.DescriptionAttribute )} is not found.",
                                             nameof( System.ComponentModel.DescriptionAttribute ) )
              : default;
    }

    /// <summary>   An T extension method that returns the Enum description. </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Generic type parameter. </typeparam>
    /// <param name="value">            The Enum value. </param>
    /// <returns>   A <see cref="string" />? </returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static string Description<TEnum>( this TEnum value ) where TEnum : struct, Enum
    {
        return value.ToMember().Description;
    }

    /// <summary>   A TEnum extension method that returns the Enum description or name. </summary>
    /// <remarks>   David, 2021-02-22. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    The Enum value. </param>
    /// <returns>   A <see cref="string" />. </returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static string DescriptionOrName<TEnum>( this TEnum value ) where TEnum : struct, Enum
    {
        return value.ToMember().DescriptionOrName;
    }

}
