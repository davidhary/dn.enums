namespace cc.isr.Enums;
/// <summary>
/// Provides the label annotation to be tagged to enum type fields.
/// </summary>
/// <remarks>
/// Creates instance.
/// </remarks>
/// <param name="value"></param>
/// <param name="index"></param>
[AttributeUsage( AttributeTargets.Field, AllowMultiple = true, Inherited = false )]
public sealed class LabelAttribute( string? value, int index = 0 ) : Attribute
{
    #region " properties "

    /// <summary>
    /// Gets the value.
    /// </summary>
    public string? Value { get; } = value;

    /// <summary>
    /// Gets the index.
    /// </summary>
    public int Index { get; } = index;

    #endregion
    #region " constructtion "
    #endregion
}
