using cc.isr.Enums.Internals;

namespace cc.isr.Enums;
/// <summary>
/// Provides high performance utilities for enum type.
/// </summary>
public static partial class FastEnum
{
    private static class Cache_Type<T>
        where T : struct, Enum
    {
        public static readonly Type Type;
        public static readonly Type UnderlyingType;

        static Cache_Type()
        {
            Type = typeof( T );
            UnderlyingType = Enum.GetUnderlyingType( Type );
        }
    }

    private static class Cache_Values<T> where T : struct, Enum
    {
        public static readonly ReadOnlyArray<T> Values;
        public static readonly bool IsEmpty;

        static Cache_Values()
        {
            Type type = Cache_Type<T>.Type;
            T[] values = ( T[] ) Enum.GetValues( type );
            Values = values.AsReadOnly();
            IsEmpty = values.Length == 0;
        }
    }

    private static class Cache_Names<T> where T : struct, Enum
    {
        public static readonly ReadOnlyArray<string> Names;

        static Cache_Names()
        {
            Type type = Cache_Type<T>.Type;
            Names = Enum.GetNames( type ).ToReadOnlyArray();
        }
    }

    private static class Cache_Members<T> where T : struct, Enum
    {
        public static readonly ReadOnlyArray<Member<T>> Members;

        static Cache_Members() => Members
                = Cache_Names<T>.Names
                .Select( x => new Member<T>( x ) )
                .ToReadOnlyArray();
    }

    private static class Cache_MinMaxValues<T> where T : struct, Enum
    {
        public static readonly T MinValue;
        public static readonly T MaxValue;

        static Cache_MinMaxValues()
        {
            ReadOnlyArray<T> values = Cache_Values<T>.Values;
            MinValue = values.DefaultIfEmpty().Min();
            MaxValue = values.DefaultIfEmpty().Max();
        }
    }

    private static class Cache_IsFlags<T> where T : struct, Enum
    {
        public static readonly bool IsFlags;

        static Cache_IsFlags()
        {
            Type type = Cache_Type<T>.Type;
            IsFlags = Attribute.IsDefined( type, typeof( FlagsAttribute ) );
        }
    }

    private static class Cache_MembersByName<T> where T : struct, Enum
    {
        public static readonly FrozenStringKeyDictionary<Member<T>> MemberByName;

        static Cache_MembersByName()
            => MemberByName = Cache_Members<T>.Members.ToFrozenStringKeyDictionary( x => x.Name );
    }
    private static class Cache_UnderlyingOperation<T> where T : struct, Enum
    {
        public static readonly Type UnderlyingType;
        public static readonly IUnderlyingOperation<T> UnderlyingOperation;

        static Cache_UnderlyingOperation()
        {
            Type type = Cache_Type<T>.Type;
            T min = Cache_MinMaxValues<T>.MinValue;
            T max = Cache_MinMaxValues<T>.MaxValue;
            Member<T>[] distinct = Cache_Members<T>.Members.OrderBy( x => x.Value ).Distinct( new Member<T>.ValueComparer() ).ToArray();
            UnderlyingType = Cache_Type<T>.UnderlyingType;
            UnderlyingOperation
                = Type.GetTypeCode( type ) switch
                {
                    TypeCode.SByte => SByteOperation<T>.Create( min, max, distinct ),
                    TypeCode.Byte => ByteOperation<T>.Create( min, max, distinct ),
                    TypeCode.Int16 => Int16Operation<T>.Create( min, max, distinct ),
                    TypeCode.UInt16 => UInt16Operation<T>.Create( min, max, distinct ),
                    TypeCode.Int32 => Int32Operation<T>.Create( min, max, distinct ),
                    TypeCode.UInt32 => UInt32Operation<T>.Create( min, max, distinct ),
                    TypeCode.Int64 => Int64Operation<T>.Create( min, max, distinct ),
                    TypeCode.UInt64 => UInt64Operation<T>.Create( min, max, distinct ),
                    _ => throw new InvalidOperationException(),
                };
        }
    }
}
