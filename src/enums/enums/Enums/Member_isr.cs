namespace cc.isr.Enums;

public sealed partial class Member<T> where T : struct, Enum
{
    /// <summary>
    /// Gets the <see cref="DescriptionAttribute"/> of specified enumeration member.
    /// </summary>
    public System.ComponentModel.DescriptionAttribute? DescriptionAttribute { get; private set; }

    /// <summary>
    /// Gets the description of specified enumeration member. Returns the Name if no description.
    /// </summary>
    public string DescriptionOrName { get; private set; }

    /// <summary>
    /// Gets the description of specified enumeration member. Empty if no description.
    /// </summary>
    public string Description { get; private set; }

    /// <summary>
    /// Gets the description Withing a description of specified enumeration member.
    /// </summary>
    public string? DescriptionWithin { get; private set; }

    /// <summary> Extracts a delimited value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">          The value. </param>
    /// <param name="startDelimiter"> The start delimiter. </param>
    /// <param name="endDelimiter">   The end delimiter. </param>
    /// <returns> The extracted between. </returns>
    private static string ExtractBetween( string value, char startDelimiter, char endDelimiter )
    {
        if ( string.IsNullOrWhiteSpace( value ) )
        {
            return string.Empty;
        }
        else
        {
            int startingIndex = value.IndexOf( startDelimiter );
            int endingIndex = value.LastIndexOf( endDelimiter );
            return startingIndex > 0 && endingIndex > startingIndex ? value.Substring( startingIndex + 1, endingIndex - startingIndex - 1 ) : value;
        }
    }

}
