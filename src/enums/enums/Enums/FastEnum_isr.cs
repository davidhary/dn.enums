namespace cc.isr.Enums;
/// <summary>
/// Provides high performance utilities for enum type.
/// </summary>
public static partial class FastEnum
{
    /// <summary>
    /// Helper method used by almost all methods to make sure the type argument is really a flags
    /// enum.
    /// </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <exception cref="TypeArgumentException">    Thrown when a Type Argument error condition
    ///                                             occurs. </exception>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    internal static void ThrowIfNotFlags<TEnum>() where TEnum : struct, Enum
    {
        if ( !FastEnum.EnumInternals<TEnum>.IsFlags )
        {
            throw new TypeArgumentException( $"Can't call this method for a non-flags enum {typeof( TEnum )}", typeof( TEnum ).ToString() );
        }
    }

    /// <summary>   Returns all the bits used in any flag values. </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Generic type parameter. </typeparam>
    /// <returns>   A flag value with all the bits set that are ever set in any defined value. </returns>
    /// <exception cref="TypeArgumentException">    <typeparamref name="TEnum"/> is not a flags enum. </exception>
    public static TEnum GetUsedBits<TEnum>() where TEnum : struct, Enum
    {
        ThrowIfNotFlags<TEnum>();
        return FastEnum.EnumInternals<TEnum>.UsedBits;
    }

    /// <summary>
    /// Converts the string representation of the description of one or more enumerated constants to
    /// an equivalent enumerated object. The return value indicates whether the conversion succeeded.
    /// </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Enum type. </typeparam>
    /// <param name="description">  The description. </param>
    /// <param name="result">       [out] The result. </param>
    /// <returns>   True if it succeeds; otherwise, false. </returns>
    public static bool TryParseDescription<TEnum>( string description, out TEnum result ) where TEnum : struct, Enum
    {
        foreach ( Member<TEnum> member in FastEnum.Cache_Members<TEnum>.Members )
        {
            if ( string.Equals( description, member.DescriptionOrName, StringComparison.Ordinal ) )
            {
                result = member.Value;
                return true;
            }
        }
        result = default;
        return false;
    }

    /// <summary>   Attempts to parse description within a T from the given string. </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Generic type parameter. </typeparam>
    /// <param name="descriptionWithin">    The description within. </param>
    /// <param name="result">               [out] The result. </param>
    /// <returns>   True if it succeeds; otherwise, false. </returns>
    public static bool TryParseDescriptionWithin<TEnum>( string descriptionWithin, out TEnum result ) where TEnum : struct, Enum
    {
        foreach ( Member<TEnum> member in FastEnum.Cache_Members<TEnum>.Members )
        {
            if ( !string.IsNullOrEmpty( descriptionWithin ) &&
                 string.Equals( descriptionWithin, member.DescriptionWithin, StringComparison.Ordinal ) )
            {
                result = member.Value;
                return true;
            }
        }
        result = default;
        return false;
    }

    /// <summary>
    /// Converts the string representation of the label of one or more enumerated constants to an
    /// equivalent enumerated object. The return value indicates whether the conversion succeeded.
    /// </summary>
    /// <remarks>   David, 2021-02-20. </remarks>
    /// <typeparam name="TEnum">    Enum type. </typeparam>
    /// <param name="label">    The label. </param>
    /// <param name="result">   [out] The result. </param>
    /// <returns>   True if it succeeds; otherwise, false. </returns>
    public static bool TryParseLabel<TEnum>( string label, out TEnum result ) where TEnum : struct, Enum
    {
        foreach ( Member<TEnum> member in FastEnum.Cache_Members<TEnum>.Members )
        {
            if ( !string.IsNullOrEmpty( label ) &&
                  member.Labels is not null && label.Equals( member.Labels[0], StringComparison.Ordinal ) )
            {
                result = member.Value;
                return true;
            }
        }
        result = default;
        return false;
    }
}
