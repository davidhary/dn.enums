using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace cc.isr.Enums;

/// <summary>   Includes extensions for enumerations and enumerated types. </summary>
/// <remarks>
/// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2009-04-09, 1.1.3386 </para><para>
/// David, 2009-07-07, 1.2.3475 </para><para>
/// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
/// </para><para>
/// Under The Creative Commons Attribution-ShareAlike 2.5 License.</para><para>
/// todo: Fast Enum functionality not yet implemented in this class.
/// </para>
/// </remarks>
public static class EnumExtensions
{
    #region " private functions "

    /// <summary>   Converts a value to a long. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   Value as a Long. </returns>
    private static long ToLong( Enum value )
    {
        return Convert.ToInt64( value, System.Globalization.CultureInfo.CurrentCulture );
    }

    /// <summary>   Converts a value to a long. </summary>
    /// <remarks>   David, 2021-02-22. </remarks>
    /// <typeparam name="TEnum">    Generic type parameter. </typeparam>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   Value as a Long. </returns>
    public static long ToLong<TEnum>( this TEnum value )
    {
        return Unsafe.As<TEnum, long>( ref value );
    }

    #endregion

    #region " values: filter "

    /// <summary>   Returns a filtered list of this collection. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="values">           The values. </param>
    /// <param name="inclusionMask">    Specifies a mask for selecting the items to include in the
    ///                                 list. </param>
    /// <param name="exclusionMask">    Specifies a mask for selecting the items to Exclude in the
    ///                                 list. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process filter in this collection.
    /// </returns>
    public static IEnumerable<TEnum> Filter<TEnum>( this IEnumerable<TEnum> values, TEnum inclusionMask, TEnum exclusionMask )
    {
        return values is null
            ? throw new ArgumentNullException( nameof( values ) )
            : values.Where( x => 0L != (x.ToLong() & inclusionMask.ToLong() & ~exclusionMask.ToLong()) );
    }

    /// <summary>   Returns a filtered list of this collection. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="values">           The values. </param>
    /// <param name="inclusionMask">    Specifies a mask for selecting the items to include in the
    ///                                 list. </param>
    /// <param name="exclusionMask">    Specifies a mask for selecting the items to Exclude in the
    ///                                 list. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process filter in this collection.
    /// </returns>
    public static IEnumerable<Enum> Filter( this IEnumerable<Enum> values, Enum inclusionMask, Enum exclusionMask )
    {
        return values is null
            ? throw new ArgumentNullException( nameof( values ) )
            : values.Where( x => 0L != (ToLong( x ) & ToLong( inclusionMask ) & ~ToLong( exclusionMask )) );
    }

    #endregion

    #region " values: include filter "

    /// <summary>
    /// Filters the this collection to include only item keys matching the inclusion mask.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="values">           The values. </param>
    /// <param name="inclusionMask">    Specifies a mask for selecting the items to include in the
    ///                                 list. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process include filter in this collection.
    /// </returns>
    public static IEnumerable<long> IncludeFilter( this IEnumerable<long> values, long inclusionMask )
    {
        return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Where( x => 0L != (x & inclusionMask) );
    }

    /// <summary>
    /// Filters the this collection to include only item keys matching the inclusion mask.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="values">           The values. </param>
    /// <param name="inclusionMask">    Specifies a mask for selecting the items to include in the
    ///                                 list. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process include filter in this collection.
    /// </returns>
    public static IEnumerable<Enum> IncludeFilter( this IEnumerable<Enum> values, long inclusionMask )
    {
        return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Where( x => 0L != (ToLong( x ) & inclusionMask) );
    }

    /// <summary>
    /// Filters the this collection to include only item keys matching the inclusion mask.
    /// </summary>
    /// <remarks>   David, 2020-09-18. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="values">           The values. </param>
    /// <param name="includedValues">   The included values. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process include filter in this collection.
    /// </returns>
    public static IEnumerable<Enum> IncludeFilter( this IEnumerable<Enum> values, Enum[] includedValues )
    {
        return values is null
            ? throw new ArgumentNullException( nameof( values ) )
            : includedValues is null
            ? throw new ArgumentNullException( nameof( includedValues ) )
            : values.Where( x => includedValues.Contains( x ) ).ToList();
    }

    /// <summary>
    /// Filters the this collection to include only item keys matching the inclusion mask.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="values">           The values. </param>
    /// <param name="includedValues">   The included values. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process include filter in this collection.
    /// </returns>
    public static IEnumerable<Enum> IncludeFilter( this IEnumerable<Enum> values, IEnumerable<Enum> includedValues )
    {
        return values is null
            ? throw new ArgumentNullException( nameof( values ) )
            : includedValues is null
            ? throw new ArgumentNullException( nameof( includedValues ) )
            : values.Where( x => includedValues.Contains( x ) ).ToList();
    }

    #endregion

    #region " values: exclude filter "

    /// <summary>
    /// Filters the this collection to Exclude item keys matching the exclusion mask.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="values">           The values. </param>
    /// <param name="exclusionMask">    Specifies a mask for selecting the items to Exclude in the
    ///                                 list. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process Exclude filter in this collection.
    /// </returns>
    public static IEnumerable<long> ExcludeFilter( this IEnumerable<long> values, long exclusionMask )
    {
        return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Where( x => 0L == (x & exclusionMask) ).ToList();
    }

    /// <summary>
    /// Filters the this collection to Exclude only item keys matching the exclusion mask.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="values">           The values. </param>
    /// <param name="exclusionMask">    Specifies a mask for selecting the items to Exclude in the
    ///                                 list. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process Exclude filter in this collection.
    /// </returns>
    public static IEnumerable<Enum> ExcludeFilter( this IEnumerable<Enum> values, long exclusionMask )
    {
        return values is null
            ? throw new ArgumentNullException( nameof( values ) )
            : values.Where( x => 0L == (ToLong( x ) & exclusionMask) ).ToList();
    }

    /// <summary>
    /// Filters the this collection to Exclude only item keys matching the exclusion mask.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="values">           The values. </param>
    /// <param name="excludedValues">   The Excluded values. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process Exclude filter in this collection.
    /// </returns>
    public static IEnumerable<Enum> ExcludeFilter( this IEnumerable<Enum> values, IEnumerable<Enum> excludedValues )
    {
        return values is null
            ? throw new ArgumentNullException( nameof( values ) )
            : excludedValues is null
            ? throw new ArgumentNullException( nameof( excludedValues ) )
            : values.Where( x => !excludedValues.Contains( x ) ).ToList();
    }

    #endregion

    #region " to binding list "

    /// <summary>
    /// Converts enumerated
    /// <see cref="KeyValuePair{TKey, TValue}">key value
    /// pairs</see> to a binding list.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="pairs">    key value pairs. </param>
    /// <returns>
    /// A list of <see cref="Enum">enumeration</see> and <see cref="string">string</see>.
    /// </returns>
    public static BindingList<KeyValuePair<Enum, string>> ToBindingList( this IEnumerable<KeyValuePair<Enum, string>> pairs )
    {
        if ( pairs is null ) throw new ArgumentNullException( nameof( pairs ) );

        BindingList<KeyValuePair<Enum, string>> result = [.. pairs];

        return result;
    }

    #endregion

    #region " values: enum "

    /// <summary>   Enumerates enum values in this collection. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="enumItem"> An enum item representing the enumeration option. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process enum values in this collection.
    /// </returns>
    public static IEnumerable<Enum> EnumValues( this Enum enumItem )
    {
        return enumItem is null ? throw new ArgumentNullException( nameof( enumItem ) ) : enumItem.GetType().EnumValues();
    }

    /// <summary>   Enumerates enum values in this collection. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="enumType"> The <see cref="Enum"/> type. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process enum values in this collection.
    /// </returns>
    public static IEnumerable<Enum> EnumValues( this Type enumType )
    {
        return Enum.GetValues( enumType ).Cast<Enum>();
    }

    /// <summary>   Enumerates enum values in this collection. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="enumType"> The <see cref="Enum"/> type. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process enum values in this collection.
    /// </returns>
    public static IEnumerable<TEnum> EnumValues<TEnum>( this Type enumType ) where TEnum : struct, Enum
    {
        return Enum.GetValues( enumType ).Cast<TEnum>();
    }

    /// <summary>   Enumerates enum values in this collection. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <returns>
    /// An enumerator that allows for each to be used to process enum values in this collection.
    /// </returns>
    public static IEnumerable<TEnum> EnumValues<TEnum>() where TEnum : struct, Enum
    {
        return Enum.GetValues( typeof( TEnum ) ).Cast<TEnum>();
    }

    #endregion

    #region " values: long "

    /// <summary>   Enumerates values for the Enum type of the supplied Enum constant. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="enumItem"> An enum item representing the enumeration option. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process values in this collection.
    /// </returns>
    public static IEnumerable<long> Values( this Enum enumItem )
    {
        return enumItem is null ? throw new ArgumentNullException( nameof( enumItem ) ) : enumItem.GetType().Values();
    }

    /// <summary>   Enumerates values in this collection. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="enumType"> The <see cref="Enum"/> type. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process values in this collection.
    /// </returns>
    public static IEnumerable<long> Values( this Type enumType )
    {
        return Enum.GetValues( enumType ).Cast<Enum>().Select( x => Convert.ToInt64( x, System.Globalization.CultureInfo.CurrentCulture ) );
    }

    #endregion

    #region " values : integer "

    /// <summary>   Enumerates values for the Enum type of the supplied Enum constant. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="enumItem"> An enum item representing the enumeration option. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process values in this collection.
    /// </returns>
    public static IEnumerable<int> IntegerValues( this Enum enumItem )
    {
        return enumItem is null ? throw new ArgumentNullException( nameof( enumItem ) ) : enumItem.GetType().IntegerValues();
    }

    /// <summary>   Enumerates values in this collection. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="enumType"> The <see cref="Enum"/> type. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process values in this collection.
    /// </returns>
    public static IEnumerable<int> IntegerValues( this Type enumType )
    {
        return Enum.GetValues( enumType ).Cast<Enum>().Select( x => Convert.ToInt32( x, System.Globalization.CultureInfo.CurrentCulture ) );
    }

    #endregion

    #region " to values "

    /// <summary>
    /// Converts enumerated
    /// <see cref="KeyValuePair{TKey, TValue}">key value
    /// pairs</see> to a <see cref="IEnumerable{T}">list</see>
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="pairs">    key value pairs. </param>
    /// <returns>   A list of string values. </returns>
    public static IEnumerable<string> ToValues( this IEnumerable<KeyValuePair<Enum, string>> pairs )
    {
        return pairs is null ? throw new ArgumentNullException( nameof( pairs ) ) : pairs.Select( x => x.Value );
    }

    /// <summary>   Enumerates to keys in this collection. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="pairs">    key value pairs. </param>
    /// <returns>   Pairs as an IEnumerable(Of System.Enum) </returns>
    public static IEnumerable<Enum> ToKeys( this IEnumerable<KeyValuePair<Enum, string>> pairs )
    {
        return pairs is null ? throw new ArgumentNullException( nameof( pairs ) ) : pairs.Select( x => x.Key );
    }

    /// <summary>   Query if 'pairs' contains key. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="pairs">    The pairs. </param>
    /// <param name="key">      An enum constant representing the key option. </param>
    /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    public static bool ContainsKey( this IEnumerable<KeyValuePair<Enum, string>> pairs, Enum key )
    {
        return pairs is null ? throw new ArgumentNullException( nameof( pairs ) ) : pairs.ToKeys().Contains( key );
    }

    /// <summary>   Select pair. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="pairs">        The pairs. </param>
    /// <param name="defaultKey">   An enum constant representing the default key option. </param>
    /// <param name="description">  The description. </param>
    /// <returns>   A KeyValuePair(Of System.Enum, String) </returns>
    public static KeyValuePair<Enum, string> SelectPair( this IEnumerable<KeyValuePair<Enum, string>> pairs, Enum defaultKey, string description )
    {
        if ( pairs is null )
        {
            throw new ArgumentNullException( nameof( pairs ) );
        }

        KeyValuePair<Enum, string> def = new( defaultKey, defaultKey.Description() );
        KeyValuePair<Enum, string> found = pairs.FirstOrDefault( x => string.Equals( x.Value, description, StringComparison.Ordinal ) );
        return string.IsNullOrWhiteSpace( found.Value ) ? def : found;
    }

    /// <summary>   Select description. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="enumType">     The <see cref="Enum"/>
    ///                             type. </param>
    /// <param name="defaultValue"> An enum constant representing the default value option. </param>
    /// <param name="description">  The description. </param>
    /// <returns>   A System.Enum. </returns>
    public static Enum SelectDescription( this Type enumType, Enum defaultValue, string description )
    {
        return enumType.ValueDescriptionPairs().SelectPair( defaultValue, description ).Key;
    }

    #endregion

    #region " value enum value pairs "

    /// <summary>   Enumerates int 32 enum value pairs in this collection. </summary>
    /// <remarks>   David, 2021-03-01. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="values">   The values. </param>
    /// <returns>
    /// An enumerator that allows foreach to be used to process int 32 enum value pairs in this
    /// collection.
    /// </returns>
    public static IEnumerable<KeyValuePair<int, TEnum>> Int32EnumValuePairs<TEnum>( this IEnumerable<TEnum> values ) where TEnum : struct, Enum
    {
        return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Select( x => x.Int32EnumValuePair() );
    }

    #endregion

    #region " value description pairs "

    /// <summary>   Gets a Key Value Pair description item. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="enumItem"> The <see cref="Enum">enumeration</see> </param>
    /// <returns>   A list of. </returns>
    public static KeyValuePair<Enum, string> ValueDescriptionPair( this Enum enumItem )
    {
        return enumItem is null
            ? throw new ArgumentNullException( nameof( enumItem ) )
            : new KeyValuePair<Enum, string>( enumItem, enumItem.Description() );
    }

    /// <summary>   Gets a Key Value Pair description item. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="enumItem"> The <see cref="Enum">enumeration</see> </param>
    /// <returns>   A list of. </returns>
    public static KeyValuePair<TEnum, string> ValueDescriptionPair<TEnum>( this TEnum enumItem ) where TEnum : struct, Enum
    {
        return new KeyValuePair<TEnum, string>( enumItem, FastEnumExtensions.Description( enumItem ) );
    }

    /// <summary>   A TEnum extension method that int 32 enum value pair. </summary>
    /// <remarks>   David, 2021-03-01. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="enumItem"> The <see cref="Enum">enumeration</see> </param>
    /// <returns>   A KeyValuePair{int,TEnum}; </returns>
    public static KeyValuePair<int, TEnum> Int32EnumValuePair<TEnum>( this TEnum enumItem ) where TEnum : struct, Enum
    {
        return new KeyValuePair<int, TEnum>( enumItem.ToInt32(), enumItem );
    }

    /// <summary>   Enumerates the value description pairs of the <see cref="Enum"/> </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="values">   The values. </param>
    /// <returns>
    /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and
    /// description (value) pairs.
    /// </returns>
    public static IEnumerable<KeyValuePair<Enum, string>> ValueDescriptionPairs( this IEnumerable<Enum> values )
    {
        return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Select( x => x.ValueDescriptionPair() );
    }

    /// <summary>   Enumerates the value description pairs of the <see cref="Enum"/> </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="values">   The values. </param>
    /// <returns>
    /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and
    /// description (value) pairs.
    /// </returns>
    public static IEnumerable<KeyValuePair<TEnum, string>> ValueDescriptionPairs<TEnum>( this IEnumerable<TEnum> values ) where TEnum : struct, Enum
    {
        return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Select( x => x.ValueDescriptionPair() );
    }

    /// <summary>   Enumerates the value description pairs of the <see cref="Enum"/> </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="enumItem"> An enum item representing the enumeration option. </param>
    /// <returns>
    /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and
    /// description (value) pairs.
    /// </returns>
    public static IEnumerable<KeyValuePair<Enum, string>> ValueDescriptionPairs( this Enum enumItem )
    {
        return enumItem is null ? throw new ArgumentNullException( nameof( enumItem ) ) : enumItem.EnumValues().ValueDescriptionPairs();
    }

    /// <summary>   Enumerates the value description pairs of the <see cref="Enum"/> </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="enumType"> The <see cref="Enum"/> type. </param>
    /// <returns>
    /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and
    /// description (value) pairs.
    /// </returns>
    /// <example>
    /// Create an enumeration with descriptions:
    /// <code>
    /// Public Enum SimpleEnum2
    /// [System.ComponentModel.Description("Today")] Today
    /// [System.ComponentModel.Description("Last 7 days")] Last7
    /// [System.ComponentModel.Description("Last 14 days")] Last14
    /// [System.ComponentModel.Description("Last 30 days")] Last30
    /// [System.ComponentModel.Description("All")] All
    /// End Enum
    /// Dim combo As ComboBox = New ComboBox()
    /// combo.DataSource = GetType(SimpleEnum2).ValueDescriptionPairs().ToList
    /// combo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
    /// combo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
    /// </code>
    /// </example>
    public static IEnumerable<KeyValuePair<Enum, string>> ValueDescriptionPairs( this Type enumType )
    {
        return enumType.EnumValues().ValueDescriptionPairs();
    }

    #endregion

    #region " descriptions "

    /// <summary>
    /// Gets the <see cref="DescriptionAttribute"/> of an <see cref="Enum"/> type value.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   A <see cref="string" /> containing the text of the <see cref="DescriptionAttribute"/>. </returns>
    public static string Description( this Enum value )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( value, nameof( value ) );
#else
        if ( value is null ) throw new ArgumentNullException( nameof( value ) );
#endif

        string candidate = value.Name();
        System.Reflection.FieldInfo fieldInfo = value.GetType().GetField( candidate );
#pragma warning disable IDE0079 // Remove unnecessary suppression
#pragma warning disable CS8602 // Dereference of a possibly null reference.
        DescriptionAttribute[] attributes = ( DescriptionAttribute[] ) fieldInfo.GetCustomAttributes( typeof( DescriptionAttribute ), false );
#pragma warning restore IDE0079 // Remove unnecessary suppression
#pragma warning disable IDE0079 // Remove unnecessary suppression
#pragma warning restore CS8602 // Dereference of a possibly null reference.
        if ( attributes is not null && attributes.Length > 0 )
        {
            candidate = attributes[0].Description;
        }
#pragma warning restore IDE0079 // Remove unnecessary suppression

        return candidate;
    }

#if false
    /// <summary>
    /// Gets the <see cref="DescriptionAttribute"/> of an <see cref="Enum"/> type value.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   A <see cref="string" /> containing the text of the <see cref="DescriptionAttribute"/>. </returns>
    public static string Description<TEnum>( this TEnum value ) where TEnum : struct, Enum
    {
        string candidate = value.Name();
        var fieldInfo = value.GetType().GetField( candidate );
#pragma warning disable CS8602 // Dereference of a possibly null reference.
        DescriptionAttribute[] attributes = ( DescriptionAttribute[] ) fieldInfo.GetCustomAttributes( typeof( DescriptionAttribute ), false );
#pragma warning restore CS8602 // Dereference of a possibly null reference.
        if ( attributes is object && attributes.Length > 0 )
        {
            candidate = attributes[0].Description;
        }

        return candidate;
    }
#endif

    /// <summary>
    /// An extension method that returns the description or descriptions (in case of Flags Enum) of
    /// An enum value.
    /// </summary>
    /// <remarks>   David, 2020-10-28. </remarks>
    /// <typeparam name="TEnum">    Generic type parameter. </typeparam>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   Value as a string. </returns>
    public static string Descriptions<TEnum>( this TEnum value ) where TEnum : struct, Enum
    {
        System.Text.StringBuilder builder = new();
        foreach ( Member<TEnum> member in FastEnum.GetMembers<TEnum>() )
        {
            if ( value.HasAllFlags( member.Value ) )
            {
                string desc = member.Description;
                if ( !string.IsNullOrEmpty( desc ) )
                {
                    if ( builder.Length > 0 ) { _ = builder.Append( ", " ); }
                    _ = builder.Append( desc );
                }
            }
        }
        return builder.ToString();
    }

    /// <summary>   Returns the descriptions of an enumeration. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="values">   The values. </param>
    /// <returns>   A list of strings. </returns>
    public static IEnumerable<string> Descriptions( this IEnumerable<Enum> values )
    {
        return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Select( x => x.Description() );
    }

    /// <summary>   Returns the descriptions of an enumeration. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="enumItem"> An enum constant representing the enum type option. </param>
    /// <returns>   A list of strings. </returns>
    public static IEnumerable<string> Descriptions( this Enum enumItem )
    {
        return enumItem is null ? throw new ArgumentNullException( nameof( enumItem ) ) : enumItem.GetType().EnumValues().Descriptions();
    }

    /// <summary>   Returns the descriptions of an enumeration. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="enumType"> The <see cref="Enum"/> type. </param>
    /// <returns>   A list of strings. </returns>
    public static IEnumerable<string> Descriptions( this Type enumType )
    {
        return enumType.EnumValues().Descriptions();
    }

    /// <summary>   Query if 'type' has description. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="enumType">     The <see cref="Enum"/>
    ///                             type. </param>
    /// <param name="description">  The description. </param>
    /// <returns>   <c>true</c> if description; otherwise <c>false</c> </returns>
    public static bool HasDescription( this Type enumType, string description )
    {
        return enumType.Descriptions().Contains( description, StringComparer.OrdinalIgnoreCase );
    }

    #endregion

    #region " string value "

    /// <summary>
    /// An enum extension method that gets the name or names (if Flags type) using (ToString()) of
    /// the given value.
    /// </summary>
    /// <remarks>   David, 2020-10-14. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string Names( this Enum value )
    {
        return value is null ? throw new ArgumentNullException( nameof( value ) ) : value.ToString();
    }

    /// <summary>
    /// An enum extension method that gets the name or names (if Flags type) using (ToString()) of
    /// the given value.
    /// </summary>
    /// <remarks>   David, 2020-10-14. </remarks>
    /// <typeparam name="TEnum">    Generic type parameter. </typeparam>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string Names<TEnum>( this TEnum value ) where TEnum : struct, Enum
    {
        return value.ToString();
    }

    #endregion

    #region " name "

    /// <summary>   An enum extension method that gets the name of the given value. </summary>
    /// <remarks>   David, 2020-10-14. </remarks>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string Name( this Enum value )
#pragma warning disable IDE0079 // Remove unnecessary suppression
    {
#pragma warning disable CS8603 // Possible null reference return.
        return Enum.GetName( value.GetType(), value );
#pragma warning restore CS8603 // Possible null reference return.
    }
#pragma warning restore IDE0079 // Remove unnecessary suppression
    /// <summary>   An enum extension method that gets the name the given value. </summary>
    /// <remarks>   David, 2020-10-14. </remarks>
    /// <typeparam name="TEnum">    Generic type parameter. </typeparam>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string Name<TEnum>( this TEnum value ) where TEnum : struct, Enum
    {
        return FastEnum.GetName( value );
    }

    #endregion

    #region " names "

    /// <summary>   Gets a Key Value Pair Name item. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="value">    The <see cref="Enum">enumeration</see> item. </param>
    /// <returns>   a value name pair. </returns>
    public static KeyValuePair<Enum, string> ValueNamePair( this Enum value )
    {
        return value is null ? throw new ArgumentNullException( nameof( value ) ) : new KeyValuePair<Enum, string>( value, value.ToString() );
    }

    /// <summary>   Gets a Key Value Pair Name item. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    The <see cref="Enum">enumeration</see> item. </param>
    /// <returns>   a value name pair. </returns>
    public static KeyValuePair<TEnum, string> ValueNamePair<TEnum>( this TEnum value ) where TEnum : struct, Enum
    {
        return new KeyValuePair<TEnum, string>( value, value.ToString() );
    }

    /// <summary>   Enumerates the value name pairs of the <see cref="Enum"/> </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="values">   The values. </param>
    /// <returns>
    /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and
    /// Name (value)
    /// pairs.
    /// </returns>
    public static IEnumerable<KeyValuePair<Enum, string>> ValueNamePairs( this IEnumerable<Enum> values )
    {
        return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Select( x => x.ValueNamePair() );
    }

    /// <summary>   Enumerates the value name pairs of the <see cref="Enum"/> </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="values">   The values. </param>
    /// <returns>
    /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and
    /// Name (value)
    /// pairs.
    /// </returns>
    public static IEnumerable<KeyValuePair<TEnum, string>> ValueNamePairs<TEnum>( this IEnumerable<TEnum> values ) where TEnum : struct, Enum
    {
        return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Select( x => x.ValueNamePair() );
    }

    /// <summary>
    /// Converts the <see cref="Enum"/> type to an <see cref="IEnumerable{KeyValuePair}"/>
    /// compatible object.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="enumType"> The <see cref="Enum"/> type. </param>
    /// <returns>
    /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and
    /// Name (value)
    /// pairs.
    /// </returns>
    /// <example>
    /// The first step is to add a Name attribute to your Enum.
    /// <code>
    /// Public Enum SimpleEnum2
    /// [System.ComponentModel.Name("Today")] Today
    /// [System.ComponentModel.Name("Last 7 days")] Last7
    /// [System.ComponentModel.Name("Last 14 days")] Last14
    /// [System.ComponentModel.Name("Last 30 days")] Last30
    /// [System.ComponentModel.Name("All")] All
    /// End Enum
    /// Dim combo As ComboBox = New ComboBox()
    /// combo.DataSource = GetType(SimpleEnum2).ValueNamePairs().ToList
    /// combo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
    /// combo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
    /// </code>
    /// </example>
    public static IEnumerable<KeyValuePair<Enum, string>> ValueNamePairs( this Type enumType )
    {
        return enumType.EnumValues().ValueNamePairs();
    }

    #endregion

    #region " delimited description "

    /// <summary>   The start delimiter. </summary>
    private const char START_DELIMITER = '(';

    /// <summary>   The end delimiter. </summary>
    private const char END_DELIMITER = ')';

    /// <summary>   The embedded value format. </summary>
    private const string EMBEDDED_VALUE_FORMAT = "({0})";

    /// <summary>
    /// Builds a delimited value. This helps parsing enumerated values that include delimited value
    /// strings in their descriptions.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="value">    The value. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildDelimitedValue( this string value )
    {
        return string.Format( System.Globalization.CultureInfo.CurrentCulture, EMBEDDED_VALUE_FORMAT, value );
    }

    /// <summary>   Returns the description up to the <paramref name="startDelimiter"/> </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="value">            The value. </param>
    /// <param name="startDelimiter">   The start delimiter. </param>
    /// <returns>   The description up to the <paramref name="startDelimiter"/> </returns>
    public static string DescriptionUntil( this string value, char startDelimiter )
    {
        if ( string.IsNullOrWhiteSpace( value ) )
        {
            return string.Empty;
        }
        else
        {
            int startingIndex = value.IndexOf( startDelimiter );
            return startingIndex > 0 ? value[..startingIndex].TrimEnd() : value;
        }
    }

    /// <summary>   Returns the description up to the <see cref="START_DELIMITER"/> </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="value">    The value. </param>
    /// <returns>   The description up to the <see cref="START_DELIMITER"/> </returns>
    public static string DescriptionUntil( this string value )
    {
        return value.DescriptionUntil( START_DELIMITER );
    }

    /// <summary>   Returns the description up to the <paramref name="startDelimiter"/> </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="value">            The <see cref="Enum"/> item. </param>
    /// <param name="startDelimiter">   The start delimiter. </param>
    /// <returns>   The description up to the <paramref name="startDelimiter"/> </returns>
    public static string DescriptionUntil( this Enum value, char startDelimiter )
    {
        return value is null ? string.Empty : value.Description().DescriptionUntil( startDelimiter );
    }

    /// <summary>   Returns the description up to the <see cref="START_DELIMITER"/> </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="value">    The <see cref="Enum">enumeration</see> </param>
    /// <returns>   The description up to the <see cref="START_DELIMITER"/> </returns>
    public static string DescriptionUntil( this Enum value )
    {
        return value is null ? string.Empty : value.DescriptionUntil( START_DELIMITER );
    }

    /// <summary>   Extracts a delimited value. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="value">            The value. </param>
    /// <param name="startDelimiter">   The start delimiter. </param>
    /// <param name="endDelimiter">     The end delimiter. </param>
    /// <returns>   The extracted between. </returns>
    public static string ExtractBetween( this string value, char startDelimiter, char endDelimiter )
    {
        if ( string.IsNullOrWhiteSpace( value ) )
        {
            return string.Empty;
        }
        else
        {
            int startingIndex = value.IndexOf( startDelimiter );
            int endingIndex = value.LastIndexOf( endDelimiter );
            return startingIndex > 0 && endingIndex > startingIndex ? value.Substring( startingIndex + 1, endingIndex - startingIndex - 1 ) : value;
        }
    }

    /// <summary>   Extracts a delimited value from value. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="value">    The value. </param>
    /// <returns>   The extracted between. </returns>
    public static string ExtractBetween( this string value )
    {
        return value.ExtractBetween( START_DELIMITER, END_DELIMITER );
    }

    /// <summary>
    /// Extracts a delimited value from the description of the relevant enumerated value.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="value">            The <see cref="Enum"/> item. </param>
    /// <param name="startDelimiter">   The start delimiter. </param>
    /// <param name="endDelimiter">     The end delimiter. </param>
    /// <returns>   The extracted between. </returns>
    public static string ExtractBetween( this Enum value, char startDelimiter, char endDelimiter )
    {
        return value is null ? string.Empty : value.Description().ExtractBetween( startDelimiter, endDelimiter );
    }

    /// <summary>
    /// Extracts a delimited value from the description of the relevant enumerated value.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="value">    The <see cref="Enum">enumeration</see> </param>
    /// <returns>   The extracted between. </returns>
    public static string ExtractBetween( this Enum value )
    {
        return value is null ? string.Empty : value.ExtractBetween( START_DELIMITER, END_DELIMITER );
    }

    #endregion

    #region " webdev_hb. under license from "

    /// <summary>   Returns true if the specified bits are set. </summary>
    /// <remarks>   David, 2010-06-05, 1.2.3807.x. Returns true if equals (IS). </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="enumItem"> The <see cref="Enum">enumeration</see> </param>
    /// <param name="value">    Specifies the bit values which to compare. </param>
    /// <returns>   <c>true</c> if set; otherwise <c>false</c> </returns>
    public static bool IsSet<TEnum>( this Enum enumItem, TEnum value ) where TEnum : struct, Enum
    {
        return enumItem.Has( value );
    }

    /// <summary>   Returns true if the enumerated flag has the specified bits. </summary>
    /// <remarks>
    /// David, 2009-07-07, 1.2.3475. <para>
    /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
    /// Creative Commons Attribution-ShareAlike 2.5 License  </para><para>
    /// David, 2010-06-05, 1.2.3807. Returns true if equals (IS). </para>
    /// </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="enumItem"> The <see cref="Enum"/> item. </param>
    /// <param name="value">    Specifies the bit values which to compare. </param>
    /// <returns>   True if it succeeds; otherwise, false. </returns>
    public static bool Has<TEnum>( this Enum enumItem, TEnum value ) where TEnum : struct, Enum
    {
        try
        {
            int oldValue = ConvertToInteger<TEnum>( enumItem );
            int bitsValue = ConvertToInteger( value );
            return oldValue == bitsValue || (oldValue & bitsValue) == bitsValue;
        }
        catch
        {
            return false;
        }
    }

    /// <summary>   Returns true if value is only the provided type. </summary>
    /// <remarks>
    /// David, 2009-07-07, 1.2.3475.x.
    /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
    /// Creative Commons Attribution-ShareAlike 2.5 License.
    /// </remarks>
    /// <typeparam name="TEnum">    . </typeparam>
    /// <param name="enumItem"> The <see cref="Enum"/> item. </param>
    /// <param name="value">    Specifies the bit values which to compare. </param>
    /// <returns>   True if it succeeds; otherwise, false. </returns>
    public static bool Is<TEnum>( this Enum enumItem, TEnum value ) where TEnum : struct, Enum
    {
        try
        {
            int oldValue = ConvertToInteger<TEnum>( enumItem );
            int bitsValue = ConvertToInteger( value );
            return oldValue == bitsValue;
        }
        catch
        {
            return false;
        }
    }

    /// <summary>   Set the specified bits. </summary>
    /// <remarks>   David, 2021-02-22. </remarks>
    /// <typeparam name="TEnum">    . </typeparam>
    /// <param name="enumItem"> The <see cref="Enum"/> item. </param>
    /// <param name="value">    Specifies the bit values which to add. </param>
    /// <returns>   A TEnum. </returns>
    public static TEnum Set<TEnum>( this Enum enumItem, TEnum value ) where TEnum : struct, Enum
    {
        return enumItem.Add( value );
    }

    /// <summary>   Appends a value. </summary>
    /// <remarks>
    /// David, 2009-07-07, 1.2.3475.x.
    /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
    /// Creative Commons Attribution-ShareAlike 2.5 License.
    /// </remarks>
    /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
    ///                                         illegal values. </exception>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="enumItem"> The <see cref="Enum"/> item. </param>
    /// <param name="value">    Specifies the bit values which to add. </param>
    /// <returns>   A T. </returns>
    public static TEnum Add<TEnum>( this Enum enumItem, TEnum value ) where TEnum : struct, Enum
    {
        try
        {
            int oldValue = ConvertToInteger<TEnum>( enumItem );
            int bitsValue = ConvertToInteger( value );
            int newValue = oldValue | bitsValue;
            return ( TEnum ) ( object ) newValue;
        }
        catch ( Exception ex )
        {
            throw new ArgumentException( $"Could not append value from enumerated type '{typeof( TEnum ).Name}'.", ex );
        }
    }

    /// <summary>   Clears the specified bits. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="enumConstant"> The <see cref="Enum"/> constant. </param>
    /// <param name="value">        Specifies the bit values which to remove. </param>
    /// <returns>   A T. </returns>
    public static TEnum Clear<TEnum>( this Enum enumConstant, TEnum value ) where TEnum : struct, Enum
    {
        return enumConstant.Remove( value );
    }

    /// <summary>   Completely removes the value. </summary>
    /// <remarks>
    /// David, 2009-07-07, 1.2.3475.x.
    /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
    /// Creative Commons Attribution-ShareAlike 2.5 License.
    /// </remarks>
    /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
    ///                                         illegal values. </exception>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="enumItem"> The <see cref="Enum"/> item. </param>
    /// <param name="value">    Specifies the bit values which to remove. </param>
    /// <returns>   A T. </returns>
    public static TEnum Remove<TEnum>( this Enum enumItem, TEnum value ) where TEnum : struct, Enum
    {
        try
        {
            int oldValue = ConvertToInteger<TEnum>( enumItem );
            int bitsValue = ConvertToInteger( value );
            int newValue = oldValue & ~bitsValue;
            return ( TEnum ) ( object ) newValue;
        }
        catch ( Exception ex )
        {
            throw new ArgumentException( $"Could not remove value from enumerated type '{typeof( TEnum ).Name}'.", ex );
        }
    }

    /// <summary>   Converts a value to an integer. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    Specifies the bit values which to compare. </param>
    /// <returns>   The given data converted to an integer. </returns>
    private static int ConvertToInteger<TEnum>( TEnum value ) where TEnum : struct, Enum
    {
        return Convert.ToInt32( value );
    }

    /// <summary>   Converts a type to an integer. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="enumConstant"> The <see cref="Enum"/> constant. </param>
    /// <returns>   The given data converted to an integer. </returns>
    private static int ConvertToInteger<TEnum>( Enum enumConstant ) where TEnum : struct, Enum
    {
        return Convert.ToInt32( enumConstant );
    }

    #endregion

    #region " parse "

    /// <summary>   Converts a value to an enum. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    The value. </param>
    /// <returns>   Value as a T. </returns>
    public static TEnum ToEnum<TEnum>( this string value ) where TEnum : struct, Enum
    {
        return ( TEnum ) Enum.Parse( typeof( TEnum ), value );
    }

    /// <summary>   Converts a value to an enum. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    The value. </param>
    /// <returns>   Value as a T. </returns>
    public static TEnum ToEnum<TEnum>( this int value ) where TEnum : struct, Enum
    {
        return ( TEnum ) Enum.Parse( typeof( TEnum ), value.ToString( System.Globalization.CultureInfo.CurrentCulture ), true );
    }

    /// <summary>   Converts a value to an enum. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    The value. </param>
    /// <returns>   Value as a T. </returns>
    public static TEnum ToEnum<TEnum>( this long value ) where TEnum : struct, Enum
    {
        return ( TEnum ) Enum.Parse( typeof( TEnum ), value.ToString( System.Globalization.CultureInfo.CurrentCulture ), true );
    }

    /// <summary>   Converts a value to a nullable enum based on the underlying enum type. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   Value as a Nullable(Of T) </returns>
    public static TEnum? ToNullableEnum<TEnum>( this long value ) where TEnum : struct
    {
        switch ( Enum.GetUnderlyingType( typeof( TEnum ) ) )
        {
            case var @case when @case == typeof( int ):
            case var case1 when case1 == typeof( int ):
                {
                    return ToNullableEnumThis<TEnum>( ( int ) value );
                }

            case var case2 when case2 == typeof( short ):
                {
                    return ToNullableEnumThis<TEnum>( ( short ) value );
                }

            case var case3 when case3 == typeof( long ):
            case var case4 when case4 == typeof( long ):
                {
                    return ToNullableEnumThis<TEnum>( value );
                }

            case var case5 when case5 == typeof( byte ):
                {
                    return ToNullableEnumThis<TEnum>( ( byte ) value );
                }

            default:
                {
                    return ToNullableEnumThis<TEnum>( ( int ) value );
                }
        }
    }

    /// <summary>   Converts a value to a nullable enum based on the underlying enum type. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   Value as a Nullable(Of T) </returns>
    public static TEnum? ToNullableEnum<TEnum>( this int value ) where TEnum : struct
    {
        switch ( Enum.GetUnderlyingType( typeof( TEnum ) ) )
        {
            case var @case when @case == typeof( int ):
            case var case1 when case1 == typeof( int ):
                {
                    return ToNullableEnumThis<TEnum>( value );
                }

            case var case2 when case2 == typeof( short ):
                {
                    return ToNullableEnumThis<TEnum>( ( short ) value );
                }

            case var case3 when case3 == typeof( long ):
            case var case4 when case4 == typeof( long ):
                {
                    return ToNullableEnumThis<TEnum>( ( long ) value );
                }

            case var case5 when case5 == typeof( byte ):
                {
                    return ToNullableEnumThis<TEnum>( ( byte ) value );
                }

            default:
                {
                    return ToNullableEnumThis<TEnum>( value );
                }
        }
    }

    /// <summary>   Converts long to enum assuming enum underlying type is long. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   The enum converted long. </returns>
    private static TEnum? ToNullableEnumThis<TEnum>( long value ) where TEnum : struct
    {
        return Enum.IsDefined( typeof( TEnum ), value ) ? ( TEnum ) Enum.ToObject( typeof( TEnum ), value ) : new TEnum?();
    }

    /// <summary>   Converts integer to enum assuming enum underlying type is integer. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   The enum converted integer. </returns>
    private static TEnum? ToNullableEnumThis<TEnum>( int value ) where TEnum : struct
    {
        return Enum.IsDefined( typeof( TEnum ), value ) ? ( TEnum ) Enum.ToObject( typeof( TEnum ), value ) : new TEnum?();
    }

    /// <summary>   Converts Short to enum assuming enum underlying type is Short. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   The enum converted Short. </returns>
    private static TEnum? ToNullableEnumThis<TEnum>( short value ) where TEnum : struct
    {
        return Enum.IsDefined( typeof( TEnum ), value ) ? ( TEnum ) Enum.ToObject( typeof( TEnum ), value ) : new TEnum?();
    }

    /// <summary>   Converts Byte to enum assuming enum underlying type is Byte. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEnum">    Type of the enum. </typeparam>
    /// <param name="value">    The <see cref="Enum"/> value. </param>
    /// <returns>   The enum converted Byte. </returns>
    private static TEnum? ToNullableEnumThis<TEnum>( byte value ) where TEnum : struct
    {
        return Enum.IsDefined( typeof( TEnum ), value ) ? ( TEnum ) Enum.ToObject( typeof( TEnum ), value ) : new TEnum?();
    }

    #endregion
}
