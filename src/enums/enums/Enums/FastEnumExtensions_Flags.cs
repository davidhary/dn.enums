using System.Runtime.CompilerServices;

namespace cc.isr.Enums;
/// <summary>
/// Provides <see cref="Enum"/> extension methods for Flags
/// </summary>
public static partial class FastEnumExtensions
{
    /// <summary>
    /// Indicates if <paramref name="value"/> has any flags.
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="value">The flags enum value.</param>
    /// <returns>Indication if <paramref name="value"/> has any flags.</returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static bool HasAnyFlags<TEnum>( this TEnum value )
        where TEnum : struct, Enum
    {
        int size = UnsafeUtility.SizeOf<TEnum>();
        return size == 1
            ? UnsafeUtility.As<TEnum, byte>( ref value ) != default
            : size == 2
                ? UnsafeUtility.As<TEnum, ushort>( ref value ) != default
                : size == 4 ? UnsafeUtility.As<TEnum, uint>( ref value ) != default : UnsafeUtility.As<TEnum, ulong>( ref value ) != default;
    }

    /// <summary>
    /// Indicates if <paramref name="value"/> has any flags that are in <paramref name="otherFlags"/>.
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="value">The flags enum value.</param>
    /// <param name="otherFlags">The other flags enum value.</param>
    /// <returns>Indication if <paramref name="value"/> has any flags that are in <paramref name="otherFlags"/>.</returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static bool HasAnyFlags<TEnum>( this TEnum value, TEnum otherFlags )
        where TEnum : struct, Enum
    {
        int size = UnsafeUtility.SizeOf<TEnum>();
        return size == 1
            ? (UnsafeUtility.As<TEnum, byte>( ref value ) & UnsafeUtility.As<TEnum, byte>( ref otherFlags )) != default
            : size == 2
                ? (UnsafeUtility.As<TEnum, ushort>( ref value ) & UnsafeUtility.As<TEnum, ushort>( ref otherFlags )) != default
                : size == 4
                                ? (UnsafeUtility.As<TEnum, uint>( ref value ) & UnsafeUtility.As<TEnum, uint>( ref otherFlags )) != default
                                : (UnsafeUtility.As<TEnum, ulong>( ref value ) & UnsafeUtility.As<TEnum, ulong>( ref otherFlags )) != default;
    }

    /// <summary>
    /// Indicates if <paramref name="value"/> has all of the flags that are in <paramref name="otherFlags"/>.
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="value">The flags enum value.</param>
    /// <param name="otherFlags">The other flags enum value.</param>
    /// <returns>Indication if <paramref name="value"/> has all of the flags that are in <paramref name="otherFlags"/>.</returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static bool HasAllFlags<TEnum>( this TEnum value, TEnum otherFlags )
        where TEnum : struct, Enum
    {
        int size = UnsafeUtility.SizeOf<TEnum>();
        return size == 1
            ? (UnsafeUtility.As<TEnum, byte>( ref value ) & UnsafeUtility.As<TEnum, byte>( ref otherFlags )) == UnsafeUtility.As<TEnum, byte>( ref otherFlags )
            : size == 2
                ? (UnsafeUtility.As<TEnum, ushort>( ref value ) & UnsafeUtility.As<TEnum, ushort>( ref otherFlags )) == UnsafeUtility.As<TEnum, ushort>( ref otherFlags )
                : size == 4
                                ? (UnsafeUtility.As<TEnum, uint>( ref value ) & UnsafeUtility.As<TEnum, uint>( ref otherFlags )) == UnsafeUtility.As<TEnum, uint>( ref otherFlags )
                                : (UnsafeUtility.As<TEnum, ulong>( ref value ) & UnsafeUtility.As<TEnum, ulong>( ref otherFlags )) == UnsafeUtility.As<TEnum, ulong>( ref otherFlags );
    }

    /// <summary>
    /// Returns <paramref name="value"/> while toggling the flags that are in <paramref name="otherFlags"/>. Equivalent to the bitwise "xor" operator.
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="value">The flags enum value.</param>
    /// <param name="otherFlags">The other flags enum value.</param>
    /// <returns><paramref name="value"/> while toggling the flags that are in <paramref name="otherFlags"/>.</returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static TEnum ToggleFlags<TEnum>( TEnum value, TEnum otherFlags )
        where TEnum : struct, Enum
    {
        int size = UnsafeUtility.SizeOf<TEnum>();
        if ( size == 1 )
        {
            byte result = ( byte ) (UnsafeUtility.As<TEnum, byte>( ref value ) ^ UnsafeUtility.As<TEnum, byte>( ref otherFlags ));
            return UnsafeUtility.As<byte, TEnum>( ref result );
        }
        else if ( size == 2 )
        {
            ushort result = ( ushort ) (UnsafeUtility.As<TEnum, ushort>( ref value ) ^ UnsafeUtility.As<TEnum, ushort>( ref otherFlags ));
            return UnsafeUtility.As<ushort, TEnum>( ref result );
        }
        else if ( size == 4 )
        {
            uint result = UnsafeUtility.As<TEnum, uint>( ref value ) ^ UnsafeUtility.As<TEnum, uint>( ref otherFlags );
            return UnsafeUtility.As<uint, TEnum>( ref result );
        }
        else
        {
            ulong result = UnsafeUtility.As<TEnum, ulong>( ref value ) ^ UnsafeUtility.As<TEnum, ulong>( ref otherFlags );
            return UnsafeUtility.As<ulong, TEnum>( ref result );
        }
    }

    /// <summary>
    /// Returns <paramref name="value"/> with only the flags that are also in <paramref name="otherFlags"/>. Equivalent to the bitwise "and" operation.
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="value">The flags enum value.</param>
    /// <param name="otherFlags">The other flags enum value.</param>
    /// <returns><paramref name="value"/> with only the flags that are also in <paramref name="otherFlags"/>.</returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static TEnum CommonFlags<TEnum>( this TEnum value, TEnum otherFlags )
        where TEnum : struct, Enum
    {
        int size = UnsafeUtility.SizeOf<TEnum>();
        if ( size == 1 )
        {
            byte result = ( byte ) (UnsafeUtility.As<TEnum, byte>( ref value ) & UnsafeUtility.As<TEnum, byte>( ref otherFlags ));
            return UnsafeUtility.As<byte, TEnum>( ref result );
        }
        else if ( size == 2 )
        {
            ushort result = ( ushort ) (UnsafeUtility.As<TEnum, ushort>( ref value ) & UnsafeUtility.As<TEnum, ushort>( ref otherFlags ));
            return UnsafeUtility.As<ushort, TEnum>( ref result );
        }
        else if ( size == 4 )
        {
            uint result = UnsafeUtility.As<TEnum, uint>( ref value ) & UnsafeUtility.As<TEnum, uint>( ref otherFlags );
            return UnsafeUtility.As<uint, TEnum>( ref result );
        }
        else
        {
            ulong result = UnsafeUtility.As<TEnum, ulong>( ref value ) & UnsafeUtility.As<TEnum, ulong>( ref otherFlags );
            return UnsafeUtility.As<ulong, TEnum>( ref result );
        }
    }

    /// <summary>
    /// Combines the flags of <paramref name="value"/> and <paramref name="otherFlags"/>. Equivalent to the bitwise "or" operation.
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="value">The flags enum value.</param>
    /// <param name="otherFlags">The other flags enum value.</param>
    /// <returns>Combination of <paramref name="value"/> with the flags in <paramref name="otherFlags"/>.</returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static TEnum CombineFlags<TEnum>( this TEnum value, TEnum otherFlags )
        where TEnum : struct, Enum
    {
        int size = UnsafeUtility.SizeOf<TEnum>();
        if ( size == 1 )
        {
            byte result = ( byte ) (UnsafeUtility.As<TEnum, byte>( ref value ) | UnsafeUtility.As<TEnum, byte>( ref otherFlags ));
            return UnsafeUtility.As<byte, TEnum>( ref result );
        }
        else if ( size == 2 )
        {
            ushort result = ( ushort ) (UnsafeUtility.As<TEnum, ushort>( ref value ) | UnsafeUtility.As<TEnum, ushort>( ref otherFlags ));
            return UnsafeUtility.As<ushort, TEnum>( ref result );
        }
        else if ( size == 4 )
        {
            uint result = UnsafeUtility.As<TEnum, uint>( ref value ) | UnsafeUtility.As<TEnum, uint>( ref otherFlags );
            return UnsafeUtility.As<uint, TEnum>( ref result );
        }
        else
        {
            ulong result = UnsafeUtility.As<TEnum, ulong>( ref value ) | UnsafeUtility.As<TEnum, ulong>( ref otherFlags );
            return UnsafeUtility.As<ulong, TEnum>( ref result );
        }
    }

    /// <summary>
    /// Combines the flags of <paramref name="flag0"/>, <paramref name="flag1"/>, and <paramref name="flag2"/>.
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="flag0">The first flags enum value.</param>
    /// <param name="flag1">The second flags enum value.</param>
    /// <param name="flag2">The third flags enum value.</param>
    /// <returns>Combination of the flags of <paramref name="flag0"/>, <paramref name="flag1"/>, and <paramref name="flag2"/>.</returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static TEnum CombineFlags<TEnum>( TEnum flag0, TEnum flag1, TEnum flag2 )
        where TEnum : struct, Enum
    {
        int size = UnsafeUtility.SizeOf<TEnum>();
        if ( size == 1 )
        {
            byte result = ( byte ) (UnsafeUtility.As<TEnum, byte>( ref flag0 ) | UnsafeUtility.As<TEnum, byte>( ref flag1 ) | UnsafeUtility.As<TEnum, byte>( ref flag2 ));
            return UnsafeUtility.As<byte, TEnum>( ref result );
        }
        else if ( size == 2 )
        {
            ushort result = ( ushort ) (UnsafeUtility.As<TEnum, ushort>( ref flag0 ) | UnsafeUtility.As<TEnum, ushort>( ref flag1 ) | UnsafeUtility.As<TEnum, ushort>( ref flag2 ));
            return UnsafeUtility.As<ushort, TEnum>( ref result );
        }
        else if ( size == 4 )
        {
            uint result = UnsafeUtility.As<TEnum, uint>( ref flag0 ) | UnsafeUtility.As<TEnum, uint>( ref flag1 ) | UnsafeUtility.As<TEnum, uint>( ref flag2 );
            return UnsafeUtility.As<uint, TEnum>( ref result );
        }
        else
        {
            ulong result = UnsafeUtility.As<TEnum, ulong>( ref flag0 ) | UnsafeUtility.As<TEnum, ulong>( ref flag1 ) | UnsafeUtility.As<TEnum, ulong>( ref flag2 );
            return UnsafeUtility.As<ulong, TEnum>( ref result );
        }
    }

    /// <summary>
    /// Combines the flags of <paramref name="flag0"/>, <paramref name="flag1"/>, <paramref name="flag2"/>, and <paramref name="flag3"/>.
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="flag0">The first flags enum value.</param>
    /// <param name="flag1">The second flags enum value.</param>
    /// <param name="flag2">The third flags enum value.</param>
    /// <param name="flag3">The fourth flags enum value.</param>
    /// <returns>Combination of the flags of <paramref name="flag0"/>, <paramref name="flag1"/>, <paramref name="flag2"/>, and <paramref name="flag3"/>.</returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static TEnum CombineFlags<TEnum>( TEnum flag0, TEnum flag1, TEnum flag2, TEnum flag3 )
        where TEnum : struct, Enum
    {
        int size = UnsafeUtility.SizeOf<TEnum>();
        if ( size == 1 )
        {
            byte result = ( byte ) (UnsafeUtility.As<TEnum, byte>( ref flag0 ) | UnsafeUtility.As<TEnum, byte>( ref flag1 ) | UnsafeUtility.As<TEnum, byte>( ref flag2 ) | UnsafeUtility.As<TEnum, byte>( ref flag3 ));
            return UnsafeUtility.As<byte, TEnum>( ref result );
        }
        else if ( size == 2 )
        {
            ushort result = ( ushort ) (UnsafeUtility.As<TEnum, ushort>( ref flag0 ) | UnsafeUtility.As<TEnum, ushort>( ref flag1 ) | UnsafeUtility.As<TEnum, ushort>( ref flag2 ) | UnsafeUtility.As<TEnum, ushort>( ref flag3 ));
            return UnsafeUtility.As<ushort, TEnum>( ref result );
        }
        else if ( size == 4 )
        {
            uint result = UnsafeUtility.As<TEnum, uint>( ref flag0 ) | UnsafeUtility.As<TEnum, uint>( ref flag1 ) | UnsafeUtility.As<TEnum, uint>( ref flag2 ) | UnsafeUtility.As<TEnum, uint>( ref flag3 );
            return UnsafeUtility.As<uint, TEnum>( ref result );
        }
        else
        {
            ulong result = UnsafeUtility.As<TEnum, ulong>( ref flag0 ) | UnsafeUtility.As<TEnum, ulong>( ref flag1 ) | UnsafeUtility.As<TEnum, ulong>( ref flag2 ) | UnsafeUtility.As<TEnum, ulong>( ref flag3 );
            return UnsafeUtility.As<ulong, TEnum>( ref result );
        }
    }

    /// <summary>
    /// Combines the flags of <paramref name="flag0"/>, <paramref name="flag1"/>, <paramref name="flag2"/>, <paramref name="flag3"/>, and <paramref name="flag4"/>.
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="flag0">The first flags enum value.</param>
    /// <param name="flag1">The second flags enum value.</param>
    /// <param name="flag2">The third flags enum value.</param>
    /// <param name="flag3">The fourth flags enum value.</param>
    /// <param name="flag4">The fifth flags enum value.</param>
    /// <returns>Combination of the flags of <paramref name="flag0"/>, <paramref name="flag1"/>, <paramref name="flag2"/>, <paramref name="flag3"/>, and <paramref name="flag4"/>.</returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static TEnum CombineFlags<TEnum>( TEnum flag0, TEnum flag1, TEnum flag2, TEnum flag3, TEnum flag4 )
        where TEnum : struct, Enum
    {
        int size = UnsafeUtility.SizeOf<TEnum>();
        if ( size == 1 )
        {
            byte result = ( byte ) (UnsafeUtility.As<TEnum, byte>( ref flag0 ) | UnsafeUtility.As<TEnum, byte>( ref flag1 ) | UnsafeUtility.As<TEnum, byte>( ref flag2 ) | UnsafeUtility.As<TEnum, byte>( ref flag3 ) | UnsafeUtility.As<TEnum, byte>( ref flag4 ));
            return UnsafeUtility.As<byte, TEnum>( ref result );
        }
        else if ( size == 2 )
        {
            ushort result = ( ushort ) (UnsafeUtility.As<TEnum, ushort>( ref flag0 ) | UnsafeUtility.As<TEnum, ushort>( ref flag1 ) | UnsafeUtility.As<TEnum, ushort>( ref flag2 ) | UnsafeUtility.As<TEnum, ushort>( ref flag3 ) | UnsafeUtility.As<TEnum, ushort>( ref flag4 ));
            return UnsafeUtility.As<ushort, TEnum>( ref result );
        }
        else if ( size == 4 )
        {
            uint result = UnsafeUtility.As<TEnum, uint>( ref flag0 ) | UnsafeUtility.As<TEnum, uint>( ref flag1 ) | UnsafeUtility.As<TEnum, uint>( ref flag2 ) | UnsafeUtility.As<TEnum, uint>( ref flag3 ) | UnsafeUtility.As<TEnum, uint>( ref flag4 );
            return UnsafeUtility.As<uint, TEnum>( ref result );
        }
        else
        {
            ulong result = UnsafeUtility.As<TEnum, ulong>( ref flag0 ) | UnsafeUtility.As<TEnum, ulong>( ref flag1 ) | UnsafeUtility.As<TEnum, ulong>( ref flag2 ) | UnsafeUtility.As<TEnum, ulong>( ref flag3 ) | UnsafeUtility.As<TEnum, ulong>( ref flag4 );
            return UnsafeUtility.As<ulong, TEnum>( ref result );
        }
    }

    /// <summary>
    /// Combines all of the flags of <paramref name="flags"/>.
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="flags">The flags enum values.</param>
    /// <returns>Combination of all of the flags of <paramref name="flags"/>.</returns>
    public static TEnum CombineFlags<TEnum>( params TEnum[]? flags )
        where TEnum : struct, Enum
    {
        return CombineFlags( ( IEnumerable<TEnum>? ) flags );
    }

    /// <summary>
    /// Combines all of the flags of <paramref name="flags"/>.
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="flags">The flags enum values.</param>
    /// <returns>Combination of all of the flags of <paramref name="flags"/>.</returns>
    public static TEnum CombineFlags<TEnum>( IEnumerable<TEnum>? flags )
        where TEnum : struct, Enum
    {
        if ( flags == null )
        {
            return default;
        }
        int size = UnsafeUtility.SizeOf<TEnum>();
        if ( size == 1 )
        {
            byte result = default;
            foreach ( TEnum flag in flags )
            {
                TEnum f = flag;
                result |= UnsafeUtility.As<TEnum, byte>( ref f );
            }
            return UnsafeUtility.As<byte, TEnum>( ref result );
        }
        else if ( size == 2 )
        {
            ushort result = default;
            foreach ( TEnum flag in flags )
            {
                TEnum f = flag;
                result |= UnsafeUtility.As<TEnum, ushort>( ref f );
            }
            return UnsafeUtility.As<ushort, TEnum>( ref result );
        }
        else if ( size == 4 )
        {
            uint result = default;
            foreach ( TEnum flag in flags )
            {
                TEnum f = flag;
                result |= UnsafeUtility.As<TEnum, uint>( ref f );
            }
            return UnsafeUtility.As<uint, TEnum>( ref result );
        }
        else
        {
            ulong result = default;
            foreach ( TEnum flag in flags )
            {
                TEnum f = flag;
                result |= UnsafeUtility.As<TEnum, ulong>( ref f );
            }
            return UnsafeUtility.As<ulong, TEnum>( ref result );
        }
    }

    /// <summary>
    /// Returns <paramref name="value"/> without the flags specified in <paramref name="otherFlags"/>.
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="value">The flags enum value.</param>
    /// <param name="otherFlags">The other flags enum value.</param>
    /// <returns><paramref name="value"/> without the flags specified in <paramref name="otherFlags"/>.</returns>
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static TEnum RemoveFlags<TEnum>( this TEnum value, TEnum otherFlags )
        where TEnum : struct, Enum
    {
        int size = UnsafeUtility.SizeOf<TEnum>();
        if ( size == 1 )
        {
            byte result = ( byte ) (UnsafeUtility.As<TEnum, byte>( ref value ) & ~UnsafeUtility.As<TEnum, byte>( ref otherFlags ));
            return UnsafeUtility.As<byte, TEnum>( ref result );
        }
        else if ( size == 2 )
        {
            ushort result = ( ushort ) (UnsafeUtility.As<TEnum, ushort>( ref value ) & ~UnsafeUtility.As<TEnum, ushort>( ref otherFlags ));
            return UnsafeUtility.As<ushort, TEnum>( ref result );
        }
        else if ( size == 4 )
        {
            uint result = UnsafeUtility.As<TEnum, uint>( ref value ) & ~UnsafeUtility.As<TEnum, uint>( ref otherFlags );
            return UnsafeUtility.As<uint, TEnum>( ref result );
        }
        else
        {
            ulong result = UnsafeUtility.As<TEnum, ulong>( ref value ) & ~UnsafeUtility.As<TEnum, ulong>( ref otherFlags );
            return UnsafeUtility.As<ulong, TEnum>( ref result );
        }
    }

}
