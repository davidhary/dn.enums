
using System.Security;

namespace System.Runtime.CompilerServices;

internal static class UnsafeUtility
{
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    [SecuritySafeCritical]
    public static int SizeOf<T>()
    {
        return Unsafe.SizeOf<T>();
    }

    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    [SecuritySafeCritical]
    public static ref TTo As<TFrom, TTo>( ref TFrom source )
    {
        return ref Unsafe.As<TFrom, TTo>( ref source );
    }

    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    [SecuritySafeCritical]
    public static T As<T>( object? value ) where T : class
    {
        return value is null ? throw new ArgumentNullException( nameof( value ) ) : Unsafe.As<T>( value );
    }
}
