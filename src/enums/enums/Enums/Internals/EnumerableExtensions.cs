namespace cc.isr.Enums.Internals;
/// <summary>
/// Provides <see cref="IEnumerable{T}"/> extension methods.
/// </summary>
internal static partial class EnumerableExtensions
{
    /// <summary>
    /// Gets collection count if <paramref name="source"/> is materialized, otherwise null.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <returns></returns>
    public static int? CountIfMaterialized<T>( this IEnumerable<T> source )
    {
        return source is null
            ? throw new ArgumentNullException( nameof( source ) )
            : source == Enumerable.Empty<T>()
            ? 0
            : source == Array.Empty<T>()
            ? 0
            : source is ICollection<T> a ? a.Count : source is IReadOnlyCollection<T> b ? b.Count : ( int? ) null;
    }

    /// <summary>
    /// Gets collection if <paramref name="source"/> is materialized, otherwise ToArray();ed collection.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <param name="nullToEmpty"></param>
    public static IEnumerable<T> Materialize<T>( this IEnumerable<T> source, bool nullToEmpty = true )
    {
        return source is null
            ? nullToEmpty ? [] : throw new ArgumentNullException( nameof( source ) )
            : source is ICollection<T> ? source : source is IReadOnlyCollection<T> ? source : source.ToArray();
    }
}
