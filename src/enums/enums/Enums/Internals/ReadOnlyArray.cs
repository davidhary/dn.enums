using System.Collections;

namespace cc.isr.Enums.Internals;
/// <summary>
/// Provides the read only array.
/// </summary>
/// <typeparam name="T"></typeparam>
/// <remarks>
/// Creates instance.
/// </remarks>
/// <param name="source"></param>
internal sealed class ReadOnlyArray<T>( T[] source ) : IReadOnlyList<T>
{
    #region " fields "
    private readonly T[] _source = source ?? throw new ArgumentNullException( nameof( source ) );

    #endregion

    #region " custom enumerator "
    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    public Enumerator GetEnumerator()
    {
        return new( this._source );
    }
    #endregion

    #region " ireadonlylist<t> implementations "
    /// <summary>
    /// Gets the element at the specified index in the read-only list.
    /// </summary>
    /// <param name="index">The zero-based index of the element to get.</param>
    /// <returns>The element at the specified index in the read-only list.</returns>
    public T this[int index]
        => this._source[index];

    /// <summary>
    /// Gets the number of elements in the collection.
    /// </summary>
    public int Count
        => this._source.Length;

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
        return new RefEnumerator( this._source );
    }

    /// <summary>
    /// Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>An <see cref="IEnumerator"/> object that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
        return this._source.GetEnumerator();
    }
    #endregion

    #region " enumerator "
    /// <summary>
    /// Provides an enumerator as value type that iterates through the collection.
    /// </summary>
    public struct Enumerator : IEnumerator<T>
    {
        #region " fields "
        private readonly T[] _source;
        private int _index;
        #endregion

        #region " constructtion "
        internal Enumerator( T[] source )
        {
            this._source = source;
            this._index = -1;
        }
        #endregion

        #region " ienumerator<t> implementations "

        public readonly T Current
            => this._source[this._index];

        readonly object? IEnumerator.Current
            => this.Current;


        public readonly void Dispose()
        { }


        public bool MoveNext()
        {
            this._index++;
            return ( uint ) this._index < ( uint ) this._source.Length;
        }


        public void Reset()
        {
            this._index = -1;
        }
        #endregion
    }

    /// <summary>
    /// Provides an enumerator as reference type that iterates through the collection.
    /// </summary>
    internal class RefEnumerator : IEnumerator<T>
    {
        #region " fields "
        private readonly T[] _source;
        private int _index;
        #endregion

        #region " constructtion "
        internal RefEnumerator( T[] source )
        {
            this._source = source;
            this._index = -1;
        }
        #endregion

        #region " ienumerator<t> implementations "
        public T Current
            => this._source[this._index];


        object? IEnumerator.Current
            => this.Current;


        public void Dispose()
        { }


        public bool MoveNext()
        {
            this._index++;
            return ( uint ) this._index < ( uint ) this._source.Length;
        }


        public void Reset()
        {
            this._index = -1;
        }
        #endregion
    }
    #endregion
}
/// <summary>
/// Provides <see cref="ReadOnlyArray{T}"/> extension methods.
/// </summary>
internal static class ReadOnlyArrayExtensions
{
    /// <summary>
    /// Converts to <see cref="ReadOnlyArray{T}"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <returns></returns>
    public static ReadOnlyArray<T> ToReadOnlyArray<T>( this IEnumerable<T> source )
    {
        return source is null
            ? throw new ArgumentNullException( nameof( source ) )
            : source is T[] array
            ? new( array )
            : new( source.ToArray() );
    }

    /// <summary>
    /// Converts to <see cref="ReadOnlyArray{T}"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <returns></returns>
    public static ReadOnlyArray<T> AsReadOnly<T>( this T[] source )
    {
        return new( source );
    }
}
