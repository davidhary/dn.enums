# About

[cc.isr.Enums] extends the [FastEnum] library with code from [Enum.Net], [Unconstrained Melody] and [isr.Core].

## How to Use

See [FastEnum]

## ISR extensions:

_IncludeFilter_ extension example:
```
/// <summary> Lists the arm sources. </summary>
/// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
/// <param name="listControl">         The list control. </param>
/// <param name="supportedArmSources"> The supported arm sources. </param>
public static void ListSupportedArmSources( this ListControl listControl, ArmSources supportedArmSources )
{
    if ( listControl is null )
        throw new ArgumentNullException( nameof( listControl ) );
    listControl.DataSource = null;
    listControl.DataSource = typeof( ArmSources ).EnumValues().IncludeFilter( ( long ) supportedArmSources ).ValueDescriptionPairs().ToList();
    listControl.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
    listControl.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
}
```

_ValueDescriptionPairs_ example:
```
this._TraceShowLevelComboBox.ComboBox.DataSource = TraceEventType.Error.ValueDescriptionPairs();
this._TraceShowLevelComboBox.ComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
this._TraceShowLevelComboBox.ComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
this._TraceShowLevelComboBox.ComboBox.BindingContext = this.BindingContext;
// this is necessary because the combo box binding does not set the data source value on it item change event
this._TraceShowLevelComboBox.ComboBox.SelectedValueChanged += this.HandleTraceShowLevelComboBoxValueChanged;
```

_ValueDescriptionPair_ example:
```
/// <summary> Select adapter type. </summary>
/// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
/// <param name="comboBox">    The list control. </param>
/// <param name="adapterType"> The adapter type. </param>
/// <returns> A VI.Scpi.AdapterType. </returns>
public static AdapterTypes SelectAdapterType( this ComboBox comboBox, AdapterTypes? adapterType )
{
    if ( comboBox is null )
        throw new ArgumentNullException( nameof( comboBox ) );
    if ( adapterType.HasValue && adapterType.Value != AdapterTypes.None && adapterType.Value != comboBox.SelectedAdapterType() )
    {
        comboBox.SelectedItem = adapterType.Value.ValueDescriptionPair();
    }

    return comboBox.SelectedAdapterType();
}
```

## Key Features

* Enum Descriptions;
* Enum Value description pairs;

# Main Types

The main types provided by this library are:

* _FastEnum_ Provides high performance utilities for enum type.

# Feedback

[cc.isr.Enums] is released as open source under the MIT license.
Bug reports and contributions are welcome at the [cc.isr.Enums] repository.

# Open Source
Open source used by this software is described and licensed at the following sites:  
[Enumeration Extensions]  
[Enum.Net]  
[FastEnum]  
[cc.isr.Enums]  
[Unconstrained Melody]  

[Enumeration Extensions]: https://www.codeproject.com/Articles/37921/Enums-Flags-and-Csharp-Oh-My-Bad-Pun
[Enum.Net]: https://github.com/TylerBrinkley/Enums.NET
[FastEnum]: https://github.com/xin9le/FastEnum
[Unconstrained Melody]: https://github.com/jskeet/unconstrained-melody
[cc.isr.Enums]: https://bitbucket.org/davidhary/dn.enums
[isr.Core]: https://bitbucket.org/davidhary/vs.core.git
