using TEnum = cc.isr.Enums.MSTest.Models.SameValueEnum;
using TUnderlying = System.Byte;

namespace cc.isr.Enums.MSTest.Cases;

public class CornerCase
{
    [Fact]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    public void GetValues()
    {
#pragma warning disable CA2263
        Array expect = Enum.GetValues( typeof( TEnum ) );
#pragma warning restore CA2263
        IReadOnlyList<TEnum> actual = FastEnum.GetValues<TEnum>();
        // this no longer works upon update to Fluent Assertions 6.0: _ = actual.Should().BeEquivalentTo( expect );
        _ = actual.Should().BeEquivalentTo( ( IReadOnlyList<TEnum> ) expect );
    }


    [Fact]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    public void GetNames()
    {
#pragma warning disable CA2263
        string[] expect = Enum.GetNames( typeof( TEnum ) );
#pragma warning restore CA2263
        IReadOnlyList<string> actual = FastEnum.GetNames<TEnum>();
        _ = actual.Should().BeEquivalentTo( expect );
    }


    [Fact]
    public void GetMembers()
    {
        Member<TEnum>[] expect =
        [
            new Member<TEnum>(nameof(TEnum.MinValue)),
            new Member<TEnum>(nameof(TEnum.Zero)),
            new Member<TEnum>(nameof(TEnum.MaxValue)),
        ];
        IReadOnlyList<Member<TEnum>> actual = FastEnum.GetMembers<TEnum>();

        _ = actual.Count.Should().Be( expect.Length );
        for ( int i = 0; i < expect.Length; i++ )
        {
            Member<TEnum> a = actual[i];
            Member<TEnum> e = expect[i];
            _ = a.Value.Should().Be( e.Value );
            _ = a.Name.Should().Be( e.Name );
            _ = a.FieldInfo.Should().Be( e.FieldInfo );

            (string name, TEnum value) = a;
            _ = value.Should().Be( e.Value );
            _ = name.Should().Be( e.Name );
        }
    }


    [Fact]
    public void IsDefined()
    {
        _ = FastEnum.IsDefined( TEnum.MinValue ).Should().BeTrue();
        _ = FastEnum.IsDefined( TEnum.Zero ).Should().BeTrue();
        _ = FastEnum.IsDefined( TEnum.MaxValue ).Should().BeTrue();
        _ = FastEnum.IsDefined( ( TEnum ) 123 ).Should().BeFalse();

        _ = TEnum.MinValue.IsDefined().Should().BeTrue();
        _ = TEnum.Zero.IsDefined().Should().BeTrue();
        _ = TEnum.MaxValue.IsDefined().Should().BeTrue();

        _ = FastEnum.IsDefined<TEnum>( nameof( TEnum.MinValue ) ).Should().BeTrue();
        _ = FastEnum.IsDefined<TEnum>( nameof( TEnum.Zero ) ).Should().BeTrue();
        _ = FastEnum.IsDefined<TEnum>( nameof( TEnum.MaxValue ) ).Should().BeTrue();
        _ = FastEnum.IsDefined<TEnum>( "123" ).Should().BeFalse();
        _ = FastEnum.IsDefined<TEnum>( nameof( TEnum.MinValue ).ToLowerInvariant() ).Should().BeFalse();

        _ = FastEnum.IsDefined<TEnum>( TUnderlying.MinValue ).Should().BeTrue();
        _ = FastEnum.IsDefined<TEnum>( TUnderlying.MaxValue ).Should().BeTrue();
        _ = FastEnum.IsDefined<TEnum>( ( TUnderlying ) 123 ).Should().BeFalse();
        _ = FluentActions
            .Invoking( () => FastEnum.IsDefined<TEnum>( ( sbyte ) 123 ) )
            .Should()
            .Throw<ArgumentException>();
    }


    [Fact]
    public void Parse()
    {
        (TEnum value, string name, string valueString)[] parameters =
        [
            (value: TEnum.MinValue, name: nameof(TEnum.MinValue), valueString: ((TUnderlying)TEnum.MinValue).ToString( System.Globalization.CultureInfo.CurrentCulture )),
            (value: TEnum.Zero,     name: nameof(TEnum.Zero),     valueString: ((TUnderlying)TEnum.Zero)    .ToString( System.Globalization.CultureInfo.CurrentCulture )),
            (value: TEnum.MaxValue, name: nameof(TEnum.MaxValue), valueString: ((TUnderlying)TEnum.MaxValue).ToString( System.Globalization.CultureInfo.CurrentCulture )),
        ];
        foreach ( (TEnum value, string name, string valueString) in parameters )
        {
            _ = FastEnum.Parse<TEnum>( name ).Should().Be( value );
            _ = FastEnum.Parse<TEnum>( valueString ).Should().Be( value );
            _ = FastEnum.Parse<TEnum>( valueString.ToLower( System.Globalization.CultureInfo.CurrentCulture ) ).Should().Be( value );
            _ = FastEnum.Parse<TEnum>( valueString.ToUpper( System.Globalization.CultureInfo.CurrentCulture ) ).Should().Be( value );
            _ = FluentActions.Invoking( () => FastEnum.Parse<TEnum>( name.ToLower( System.Globalization.CultureInfo.CurrentCulture ) ) ).Should().Throw<ArgumentException>();
            _ = FluentActions.Invoking( () => FastEnum.Parse<TEnum>( name.ToUpper( System.Globalization.CultureInfo.CurrentCulture ) ) ).Should().Throw<ArgumentException>();
        }
        _ = FluentActions.Invoking( () => FastEnum.Parse<TEnum>( "ABCDE" ) ).Should().Throw<ArgumentException>();
    }


    [Fact]
    public void ParseIgnoreCase()
    {
        (TEnum value, string name, string valueString)[] parameters =
        [
            (value: TEnum.MinValue, name: nameof(TEnum.MinValue), valueString: ((TUnderlying)TEnum.MinValue).ToString( System.Globalization.CultureInfo.CurrentCulture )),
            (value: TEnum.Zero,     name: nameof(TEnum.Zero),     valueString: ((TUnderlying)TEnum.Zero)    .ToString( System.Globalization.CultureInfo.CurrentCulture )),
            (value: TEnum.MaxValue, name: nameof(TEnum.MaxValue), valueString: ((TUnderlying)TEnum.MaxValue).ToString( System.Globalization.CultureInfo.CurrentCulture )),
        ];
        foreach ( (TEnum value, string name, string valueString) in parameters )
        {
            _ = FastEnum.Parse<TEnum>( name ).Should().Be( value );
            _ = FastEnum.Parse<TEnum>( name.ToLower( System.Globalization.CultureInfo.CurrentCulture ), true ).Should().Be( value );
            _ = FastEnum.Parse<TEnum>( name.ToUpper( System.Globalization.CultureInfo.CurrentCulture ), true ).Should().Be( value );
            _ = FastEnum.Parse<TEnum>( valueString ).Should().Be( value );
            _ = FastEnum.Parse<TEnum>( valueString.ToLower( System.Globalization.CultureInfo.CurrentCulture ), true ).Should().Be( value );
            _ = FastEnum.Parse<TEnum>( valueString.ToUpper( System.Globalization.CultureInfo.CurrentCulture ), true ).Should().Be( value );
        }
        _ = FluentActions.Invoking( () => FastEnum.Parse<TEnum>( "ABCDE", true ) ).Should().Throw<ArgumentException>();
    }


    [Fact]
    public void TryParse()
    {
        (TEnum value, string name, string valueString)[] parameters =
        [
            (value: TEnum.MinValue, name: nameof(TEnum.MinValue), valueString: ((TUnderlying)TEnum.MinValue).ToString( System.Globalization.CultureInfo.CurrentCulture )),
            (value: TEnum.Zero,     name: nameof(TEnum.Zero),     valueString: ((TUnderlying)TEnum.Zero)    .ToString( System.Globalization.CultureInfo.CurrentCulture )),
            (value: TEnum.MaxValue, name: nameof(TEnum.MaxValue), valueString: ((TUnderlying)TEnum.MaxValue).ToString( System.Globalization.CultureInfo.CurrentCulture )),
        ];
        foreach ( (TEnum value, string name, string valueString) in parameters )
        {
            _ = FastEnum.TryParse( name, out TEnum r1 ).Should().BeTrue();
            _ = r1.Should().Be( value );

            _ = FastEnum.TryParse( valueString, out TEnum r2 ).Should().BeTrue();
            _ = r2.Should().Be( value );

            _ = FastEnum.TryParse( valueString.ToLower( System.Globalization.CultureInfo.CurrentCulture ), out TEnum r3 ).Should().BeTrue();
            _ = r3.Should().Be( value );

            _ = FastEnum.TryParse( valueString.ToUpper( System.Globalization.CultureInfo.CurrentCulture ), out TEnum r4 ).Should().BeTrue();
            _ = r4.Should().Be( value );

            _ = FastEnum.TryParse<TEnum>( name.ToLower( System.Globalization.CultureInfo.CurrentCulture ), out _ ).Should().BeFalse();
            _ = FastEnum.TryParse<TEnum>( name.ToUpper( System.Globalization.CultureInfo.CurrentCulture ), out _ ).Should().BeFalse();
        }
        _ = FastEnum.TryParse<TEnum>( "ABCDE", out _ ).Should().BeFalse();
    }


    [Fact]
    public void TryParseIgnoreCase()
    {
        (TEnum value, string name, string valueString)[] parameters =
        [
            (value: TEnum.MinValue, name: nameof(TEnum.MinValue), valueString: ((TUnderlying)TEnum.MinValue).ToString( System.Globalization.CultureInfo.CurrentCulture )),
            (value: TEnum.Zero,     name: nameof(TEnum.Zero),     valueString: ((TUnderlying)TEnum.Zero)    .ToString( System.Globalization.CultureInfo.CurrentCulture )),
            (value: TEnum.MaxValue, name: nameof(TEnum.MaxValue), valueString: ((TUnderlying)TEnum.MaxValue).ToString( System.Globalization.CultureInfo.CurrentCulture )),
        ];
        foreach ( (TEnum value, string name, string valueString) in parameters )
        {
            _ = FastEnum.TryParse( name, true, out TEnum r1 ).Should().BeTrue();
            _ = r1.Should().Be( value );

            _ = FastEnum.TryParse( name.ToLower( System.Globalization.CultureInfo.CurrentCulture ), true, out TEnum r2 ).Should().BeTrue();
            _ = r2.Should().Be( value );

            _ = FastEnum.TryParse( name.ToUpper( System.Globalization.CultureInfo.CurrentCulture ), true, out TEnum r3 ).Should().BeTrue();
            _ = r3.Should().Be( value );

            _ = FastEnum.TryParse( valueString, true, out TEnum r4 ).Should().BeTrue();
            _ = r4.Should().Be( value );

            _ = FastEnum.TryParse( valueString.ToLower( System.Globalization.CultureInfo.CurrentCulture ), true, out TEnum r5 ).Should().BeTrue();
            _ = r5.Should().Be( value );

            _ = FastEnum.TryParse( valueString.ToUpper( System.Globalization.CultureInfo.CurrentCulture ), true, out TEnum r6 ).Should().BeTrue();
            _ = r6.Should().Be( value );
        }

        _ = FastEnum.TryParse<TEnum>( "ABCDE", true, out _ ).Should().BeFalse();
    }


    [Fact]
    public void ToMember()
    {
        {
            TEnum value = TEnum.MinValue;
            string name = nameof( TEnum.MinValue );
            Member<TEnum> member = value.ToMember();
            _ = member.Name.Should().Be( name );
            _ = member.Value.Should().Be( value );
            _ = member.FieldInfo.Should().Be( typeof( TEnum ).GetField( name ) );
        }
        {
            TEnum value = TEnum.Zero;
            string name = nameof( TEnum.MinValue );  // If the same value exists, we can't control what is correct.
            Member<TEnum> member = value.ToMember();
            _ = member.Name.Should().Be( name );
            _ = member.Value.Should().Be( value );
            _ = member.FieldInfo.Should().Be( typeof( TEnum ).GetField( name ) );
        }
        {
            TEnum value = TEnum.MaxValue;
            string name = nameof( TEnum.MaxValue );
            Member<TEnum> member = value.ToMember();
            _ = member.Name.Should().Be( name );
            _ = member.Value.Should().Be( value );
            _ = member.FieldInfo.Should().Be( typeof( TEnum ).GetField( name ) );
        }
    }


    [Fact]
    public void ToName()
    {
        string[] zeroStrings =
        [
            nameof(TEnum.MinValue),
            nameof(TEnum.Zero),
        ];
        _ = TEnum.MinValue.ToName().Should().ContainAny( zeroStrings );
        _ = TEnum.Zero.ToName().Should().ContainAny( zeroStrings );
        _ = TEnum.MaxValue.ToName().Should().Be( nameof( TEnum.MaxValue ) );
    }
}
