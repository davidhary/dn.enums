using cc.isr.Enums.Internals;

namespace cc.isr.Enums.MSTest.Cases;

public class ReadOnlyArrayTest
{
    private const int LOOP_COUNT = 100;
    private readonly int[] _sourceArray = Enumerable.Range( 0, LOOP_COUNT ).ToArray();


    [Fact]
    public void Count()
    {
        ReadOnlyArray<int> roa = this._sourceArray.ToReadOnlyArray();
        _ = roa.Count.Should().Be( this._sourceArray.Length );
    }


    [Fact]
    public void ForEach()
    {
        int sum = 0;
        foreach ( int x in this._sourceArray.ToReadOnlyArray() )
            sum += x;

        _ = sum.Should().Be( this._sourceArray.Sum() );
    }


    [Fact]
    public void SequenceEquals()
    {
        ReadOnlyArray<int> roa = this._sourceArray.ToReadOnlyArray();
        _ = roa.Should().BeEquivalentTo( this._sourceArray );
        _ = roa.Should().ContainInOrder( this._sourceArray );
    }
}
