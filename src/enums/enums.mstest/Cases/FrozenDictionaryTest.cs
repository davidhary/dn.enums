using cc.isr.Enums.Internals;

namespace cc.isr.Enums.MSTest.Cases;

public class FrozenDictionaryTest
{
    private const int ELEMENT_COUNT = 100;

    [Fact]
    public void GenericsKey()
    {
        IEnumerable<int> values = Enumerable.Range( 0, ELEMENT_COUNT );
        FrozenDictionary<int, int> dic = values.ToFrozenDictionary( x => x );

        _ = dic.Count.Should().Be( ELEMENT_COUNT );
        _ = dic.TryGetValue( -1, out _ ).Should().BeFalse();
        foreach ( int x in values )
        {
            _ = dic.TryGetValue( x, out int result ).Should().BeTrue();
            _ = result.Should().Be( x );
        }
    }

    [Fact]
    public void IntKey()
    {
        IEnumerable<int> values = Enumerable.Range( 0, ELEMENT_COUNT );
        FrozenInt32KeyDictionary<int> dic = values.ToFrozenInt32KeyDictionary( x => x );

        _ = dic.Count.Should().Be( ELEMENT_COUNT );
        _ = dic.TryGetValue( -1, out _ ).Should().BeFalse();
        foreach ( int x in values )
        {
            _ = dic.TryGetValue( x, out int result ).Should().BeTrue();
            _ = result.Should().Be( x );
        }
    }

    [Fact]
    public void StringKey()
    {
        string[] values
            = Enumerable.Range( 0, ELEMENT_COUNT )
            .Select( _ => Guid.NewGuid().ToString() )
            .ToArray();
        FrozenStringKeyDictionary<string> dic = values.ToFrozenStringKeyDictionary( x => x );

        _ = dic.Count.Should().Be( ELEMENT_COUNT );
        _ = dic.TryGetValue( string.Empty, out _ ).Should().BeFalse();
        foreach ( string? x in values )
        {
            _ = dic.TryGetValue( x, out string? result ).Should().BeTrue();
            _ = result.Should().Be( x );
        }
    }
}
