using TEnum = cc.isr.Enums.MSTest.Models.EmptyEnum;

namespace cc.isr.Enums.MSTest.Cases;

public class EmptyTest
{
    [Fact]
    public void GetValues()
    {
        _ = FastEnum.GetValues<TEnum>().Should().BeEmpty();
    }

    [Fact]
    public void GetNames()
    {
        _ = FastEnum.GetNames<TEnum>().Should().BeEmpty();
    }

    [Fact]
    public void GetMembers()
    {
        _ = FastEnum.GetMembers<TEnum>().Should().BeEmpty();
    }

    [Fact]
    public void GetMinValue()
    {
        _ = FastEnum.GetMinValue<TEnum>().Should().BeNull();
    }

    [Fact]
    public void GetMaxValue()
    {
        _ = FastEnum.GetMaxValue<TEnum>().Should().BeNull();
    }

    [Fact]
    public void IsEmpty()
    {
        _ = FastEnum.IsEmpty<TEnum>().Should().BeTrue();
    }

    [Fact]
    public void IsContinuous()
    {
        _ = FastEnum.IsContinuous<TEnum>().Should().BeFalse();
    }

    [Fact]
    public void IsDefined()
    {
        _ = FastEnum.IsDefined( ( TEnum ) 123 ).Should().BeFalse();
        _ = FastEnum.IsDefined<TEnum>( "123" ).Should().BeFalse();
        _ = FluentActions
            .Invoking( () => FastEnum.IsDefined<TEnum>( ( sbyte ) 123 ) )
            .Should()
            .Throw<ArgumentException>();
    }


    [Fact]
    public void Parse()
    {
        _ = FluentActions.Invoking( () => FastEnum.Parse<TEnum>( "ABCDE" ) ).Should().Throw<ArgumentException>();
    }

    [Fact]
    public void ParseIgnoreCase()
    {
        _ = FluentActions.Invoking( () => FastEnum.Parse<TEnum>( "ABCDE", true ) ).Should().Throw<ArgumentException>();
    }

    [Fact]
    public void TryParse()
    {
        _ = FastEnum.TryParse( "ABCDE", out TEnum _ ).Should().BeFalse();
    }

    [Fact]
    public void TryParseIgnoreCase()
    {
        _ = FastEnum.TryParse( "ABCDE", true, out TEnum _ ).Should().BeFalse();
    }
}
