namespace cc.isr.Enums.MSTest.Cases2;

[Flags]
internal enum Int64Enum : long
{
    MinusOne = -1,
    Zero = 0,
    Max = 0x7FFFFFFFFFFFFFFF
}
