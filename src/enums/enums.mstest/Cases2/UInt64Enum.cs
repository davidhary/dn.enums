namespace cc.isr.Enums.MSTest.Cases2;

[Flags]
internal enum UInt64Enum : ulong
{
    Zero = 0,
    BigValue = 0xFFFFFFFFFFFFFFFF
}
