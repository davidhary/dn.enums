namespace cc.isr.Enums.MSTest.Cases2;

/// <summary>   (Unit Test Class) the enums tests. </summary>
/// <remarks>   David, 2021-02-18. </remarks>
public class EnumsTests
{
    /// <summary>   (Unit Test Method) gets values should return singleton. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void GetValuesShouldReturnSingleton()
    {
        IReadOnlyList<Number> numbers = FastEnum.GetValues<Number>();
        IReadOnlyList<Number> numbers2 = FastEnum.GetValues<Number>();
        Xunit.Assert.Same( numbers, numbers2 );
    }

    /// <summary>   (Unit Test Method) gets values should return read only list. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void GetValuesShouldReturnReadOnlyList()
    {
        IReadOnlyList<Number> numbers = FastEnum.GetValues<Number>();
        _ = Xunit.Assert.IsAssignableFrom<IReadOnlyList<Number>>( numbers );
    }

    /// <summary>   (Unit Test Method) gets values should return correct values. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void GetValuesShouldReturnCorrectValues()
    {
        IReadOnlyList<Number> numbers = FastEnum.GetValues<Number>();
        Xunit.Assert.True( numbers.SequenceEqual( [Number.One, Number.Two, Number.Three] ) );
    }

    /// <summary>   (Unit Test Method) gets names should return singleton. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void GetNamesShouldReturnSingleton()
    {
        IReadOnlyList<string> names = FastEnum.GetNames<Number>();
        IReadOnlyList<string> names2 = FastEnum.GetNames<Number>();
        Xunit.Assert.Same( names, names2 );
    }

    /// <summary>   (Unit Test Method) gets names should return read only list. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void GetNamesShouldReturnReadOnlyList()
    {
        IReadOnlyList<string> names = FastEnum.GetNames<Number>();
        _ = Xunit.Assert.IsAssignableFrom<Internals.ReadOnlyArray<string>>( names );
    }

    /// <summary>   (Unit Test Method) gets names should return correct values. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void GetNamesShouldReturnCorrectValues()
    {
        IReadOnlyList<string> names = FastEnum.GetNames<Number>();
        Xunit.Assert.True( names.SequenceEqual( ["One", "Two", "Three"] ) );
    }

    /// <summary>   (Unit Test Method) flag should be named value. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void FlagShouldBeNamedValue()
    {
        Xunit.Assert.True( BitFlags.Flag24.IsNamedValue() );
    }

    /// <summary>   (Unit Test Method) enum should be named value. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void EnumShouldBeNamedValue()
    {
        Xunit.Assert.True( Number.One.IsNamedValue() );
    }

    /// <summary>   (Unit Test Method) flags combination should not be named value. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void FlagsCombinationShouldNotBeNamedValue()
    {
        Xunit.Assert.False( (BitFlags.Flag1 | BitFlags.Flag2).IsNamedValue() );
    }

    /// <summary>   (Unit Test Method) undefined enum should not be named value. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void UndefinedEnumShouldNotBeNamedValue()
    {
        Xunit.Assert.False( FastEnumExtensions.IsNamedValue<Number>( 0 ) );
    }

    /// <summary>
    /// (Unit Test Method) attempts to parse a name non flags should parse from the given data,
    /// returning a default value rather than throwing an exception if it fails.
    /// </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void TryParseNameNonFlagsShouldParse()
    {
        Xunit.Assert.True( FastEnum.TryParse( "One", out Number number ) );
        Xunit.Assert.Equal( Number.One, number );
    }

    /// <summary>
    /// (Unit Test Method) attempts to parse a name non flags value should not parse from the given
    /// data, returning a default value rather than throwing an exception if it fails.
    /// </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void TryParseNameNonFlagsValueShouldNotParse()
    {
        Xunit.Assert.True( FastEnum.TryParse<Number>( "1", out _ ) );
    }

    /// <summary>
    /// (Unit Test Method) attempts to parse a name non flags value should equal zero from the given
    /// data, returning a default value rather than throwing an exception if it fails.
    /// </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void TryParseNameNonFlagsValueShouldEqualZero()
    {
        _ = FastEnum.TryParse( "1", out Number number );
        Xunit.Assert.Equal( Number.One, number );
    }

    /// <summary>
    /// (Unit Test Method) attempts to parse a name non flags unknown should not parse from the given
    /// data, returning a default value rather than throwing an exception if it fails.
    /// </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void TryParseNameNonFlagsUnknownShouldNotParse()
    {
        Xunit.Assert.False( FastEnum.TryParse( "rubbish", out Number number ) );
        Xunit.Assert.Equal( ( Number ) 0, number );
    }

    /// <summary>
    /// (Unit Test Method) attempts to parse a name non flags unknown value should be zero from the
    /// given data, returning a default value rather than throwing an exception if it fails.
    /// </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void TryParseNameNonFlagsUnknownValueShouldBeZero()
    {
        _ = FastEnum.TryParse( "rubbish", out Number number );
        Xunit.Assert.Equal( ( Number ) 0, number );
    }

    /// <summary>
    /// (Unit Test Method) attempts to parse a name flags from the given data, returning a default
    /// value rather than throwing an exception if it fails.
    /// </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void TryParseNameFlags()
    {
        Xunit.Assert.True( FastEnum.TryParse( "Flag24", out BitFlags result ) );
        Xunit.Assert.Equal( BitFlags.Flag24, result );
        Xunit.Assert.True( FastEnum.TryParse( "1", out result ) );
        Xunit.Assert.Equal( BitFlags.Flag1, result );
        Xunit.Assert.False( FastEnum.TryParse( "rubbish", out result ) );
        Xunit.Assert.Equal( ( BitFlags ) 0, result );
        Xunit.Assert.False( FastEnum.TryParse( "Flag2,Flag4", out result ) );
        Xunit.Assert.Equal( ( BitFlags ) 0, result );
    }

    /// <summary>   (Unit Test Method) parse name invalid value. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void ParseNameInvalidValue()
    {
        _ = Xunit.Assert.Throws<ArgumentException>( () => FastEnum.Parse<Number>( "rubbish" ) );
    }

    /// <summary>   (Unit Test Method) parse name. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void ParseName()
    {
        Xunit.Assert.Equal( Number.Two, FastEnum.Parse<Number>( "Two" ) );
        Xunit.Assert.Equal( BitFlags.Flag24, FastEnum.Parse<BitFlags>( "Flag24" ) );
    }

    /// <summary>   (Unit Test Method) gets underlying type should equal. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void GetUnderlyingTypeShouldEqual()
    {
        Xunit.Assert.Equal( typeof( byte ), FastEnum.GetUnderlyingType<ByteEnum>() );
        Xunit.Assert.Equal( typeof( int ), FastEnum.GetUnderlyingType<Number>() );
        Xunit.Assert.Equal( typeof( long ), FastEnum.GetUnderlyingType<Int64Enum>() );
        Xunit.Assert.Equal( typeof( ulong ), FastEnum.GetUnderlyingType<UInt64Enum>() );
    }

    /// <summary>   (Unit Test Method) gets description should equal. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void GetDescriptionShouldEqual()
    {
        Xunit.Assert.Equal( "First description", Number.One.Description() );
    }

    /// <summary>
    /// (Unit Test Method) gets description when value has no description should be null.
    /// </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void GetDescriptionWhenValueHasNoDescriptionShouldBeNull()
    {
        Xunit.Assert.Null( Number.Two.GetDescriptionValue( false ) );
    }

    /// <summary>   (Unit Test Method) gets description for invalid value should throw. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void GetDescriptionForInvalidValueShouldThrow()
    {
        _ = Xunit.Assert.Throws<IndexOutOfRangeException>( () => (( Number ) 4).GetDescriptionValue( true ) );
    }

    /// <summary>
    /// (Unit Test Method) attempts to parse a unique description should succeed from the given data,
    /// returning a default value rather than throwing an exception if it fails.
    /// </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void TryParseUniqueDescriptionShouldSucceed()
    {
        Xunit.Assert.True( FastEnum.TryParseDescription( "Third description", out Number number ) );
        Xunit.Assert.Equal( Number.Three, number );
    }

    /// <summary>
    /// (Unit Test Method) attempts to parse a duplicate description should return the first match
    /// from the given data, returning a default value rather than throwing an exception if it
    /// fails.
    /// </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void TryParseDuplicateDescriptionShouldReturnTheFirstMatch()
    {
        Xunit.Assert.True( FastEnum.TryParseDescription( "Duplicate description", out BitFlags result ) );
        Xunit.Assert.Equal( BitFlags.Flag2, result );
    }

    /// <summary>
    /// (Unit Test Method) attempts to parse a missing description should be false from the given
    /// data, returning a default value rather than throwing an exception if it fails.
    /// </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void TryParseMissingDescriptionShouldBeFalse()
    {
        Xunit.Assert.False( FastEnum.TryParseDescription<Number>( "Doesn't exist", out _ ) );
    }
}
