using System.Diagnostics;

namespace cc.isr.Enums.MSTest.Cases2;
/// <summary>
/// This is a test class for EnumExtensionsTest and is intended to contain all EnumExtensionsTest
/// Unit Tests.
/// </summary>
/// <remarks> David, 2020-09-18. </remarks>
public class EnumExtensionsTests
{
    #region " enum extension test: values "


    /// <summary> (Unit Test Method) tests enum values include. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [Fact]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    public void EnumValuesIncludeTest()
    {
        IEnumerable<Enum> allValues = typeof( TraceEventType ).EnumValues();
        Xunit.Assert.True( allValues.Any(), $"{typeof( TraceEventType )}.EnumValues(of System.Enum) has values" );
#pragma warning disable CA2263
        _ = allValues.Count().Should().Be( Enum.GetValues( typeof( TraceEventType ) ).Length, $"{typeof( TraceEventType )} count matches the extension result" );
#pragma warning restore CA2263
        Xunit.Assert.True( allValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the enum values of {typeof( TraceEventType )}" );

        IEnumerable<long> allLongs = typeof( TraceEventType ).Values();
        Xunit.Assert.True( allLongs.Any(), $"{typeof( TraceEventType )}.EnumValues(of Long) has values" );
#pragma warning disable CA2263
        _ = allValues.Count().Should().Be( Enum.GetValues( typeof( TraceEventType ) ).Length, $"{typeof( TraceEventType )}.EnumValues(of Long) count matches the extension result" );
#pragma warning restore CA2263

        long includeMask = ( long ) (TraceEventType.Critical | TraceEventType.Error);
        IEnumerable<Enum> filteredValues = allValues.IncludeFilter( includeMask );
        int expectedCount = 2;
        Xunit.Assert.True( filteredValues.Any(), $"{typeof( TraceEventType )} filtered Values has values" );
        _ = filteredValues.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered values has expected count" );
        Xunit.Assert.True( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the filtered values of {typeof( TraceEventType )}" );
        Xunit.Assert.False( filteredValues.Contains( TraceEventType.Information ), $"{TraceEventType.Information} is not included in the filtered values of {typeof( TraceEventType )}" );

        IEnumerable<long> filteredLongs = allLongs.IncludeFilter( includeMask );
        Xunit.Assert.True( filteredLongs.Any(), $"{typeof( TraceEventType )} filtered Longs has Longs" );
        _ = filteredLongs.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered Longs has expected count" );
        Xunit.Assert.True( filteredLongs.Contains( ( long ) TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the filtered Longs of {typeof( TraceEventType )}" );
        Xunit.Assert.False( filteredLongs.Contains( ( long ) TraceEventType.Information ), $"{TraceEventType.Information} is not included in the filtered Longs of {typeof( TraceEventType )}" );

        IList<Enum> filter = [TraceEventType.Critical, TraceEventType.Error];
        filteredValues = allValues.IncludeFilter( filter );
        // filteredValues = allValues.IncludeFilter(new Enum[] { TraceEventType.Critical, TraceEventType.Error });
        expectedCount = 2;
        Xunit.Assert.True( filteredValues.Any(), $"{typeof( TraceEventType )} filtered Values has list of values" );
        _ = filteredValues.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered lists of values has expected count" );
        Xunit.Assert.True( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the filtered list of values of {typeof( TraceEventType )}" );
        Xunit.Assert.False( filteredValues.Contains( TraceEventType.Information ), $"{TraceEventType.Information} is not included in the filtered list of values of {typeof( TraceEventType )}" );
    }

    /// <summary> (Unit Test Method) tests enum values exclude. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [Fact]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    public void EnumValuesExcludeTest()
    {
        IEnumerable<Enum> allValues = typeof( TraceEventType ).EnumValues();
        Xunit.Assert.True( allValues.Any(), $"{typeof( TraceEventType )}.EnumValues(of System.Enum) has values" );
#pragma warning disable CA2263
        _ = allValues.Count().Should().Be( Enum.GetValues( typeof( TraceEventType ) ).Length, $"{typeof( TraceEventType )} count matches the extension result" );
#pragma warning restore CA2263
        Xunit.Assert.True( allValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the enum values of {typeof( TraceEventType )}" );

        IEnumerable<long> allLongs = typeof( TraceEventType ).Values();
        Xunit.Assert.True( allLongs.Any(), $"{typeof( TraceEventType )}.EnumValues(of Long) has values" );
#pragma warning disable CA2263
        _ = allValues.Count().Should().Be( Enum.GetValues( typeof( TraceEventType ) ).Length, $"{typeof( TraceEventType )}.EnumValues(of Long) count matches the extension result" );
#pragma warning restore CA2263
        long excludeMask = ( long ) (TraceEventType.Critical | TraceEventType.Error);
        IEnumerable<Enum> filteredValues = allValues.ExcludeFilter( excludeMask );
        int expectedCount = allValues.Count() - 2;
        Xunit.Assert.True( filteredValues.Any(), $"{typeof( TraceEventType )} filtered Values has values" );
        _ = filteredValues.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered values has expected count" );
        Xunit.Assert.False( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the filtered values of {typeof( TraceEventType )}" );
        Xunit.Assert.True( filteredValues.Contains( TraceEventType.Information ), $"{TraceEventType.Information} is not Excluded in the filtered values of {typeof( TraceEventType )}" );

        IEnumerable<long> filteredLongs = allLongs.ExcludeFilter( excludeMask );
        Xunit.Assert.True( filteredLongs.Any(), $"{typeof( TraceEventType )} filtered Longs has Longs" );
        _ = filteredLongs.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered Longs has expected count" );
        Xunit.Assert.False( filteredLongs.Contains( ( long ) TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the filtered Longs of {typeof( TraceEventType )}" );
        Xunit.Assert.True( filteredLongs.Contains( ( long ) TraceEventType.Information ), $"{TraceEventType.Information} is not Excluded in the filtered Longs of {typeof( TraceEventType )}" );

        IList<Enum> filter = [TraceEventType.Critical, TraceEventType.Error];
        filteredValues = allValues.ExcludeFilter( filter );
        // filteredValues = allValues.ExcludeFilter( allValues );
        // filteredValues = allValues.ExcludeFilter( (new TraceEventType[] { TraceEventType.Critical, TraceEventType.Error } );
        expectedCount = allValues.Count() - 2;
        Xunit.Assert.True( filteredValues.Any(), $"{typeof( TraceEventType )} filtered Values has list of values" );
        _ = filteredValues.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered lists of values has expected count" );
        Xunit.Assert.False( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the filtered list of values of {typeof( TraceEventType )}" );
        Xunit.Assert.True( filteredValues.Contains( TraceEventType.Information ), $"{TraceEventType.Information} is not Excluded in the filtered list of values of {typeof( TraceEventType )}" );

        allValues = TraceEventType.Critical.EnumValues();
        Xunit.Assert.True( allValues.Any(), $"{typeof( TraceEventType )}.EnumValues(of System.Enum) has values" );
#pragma warning disable CA2263
        _ = allValues.Count().Should().Be( Enum.GetValues( typeof( TraceEventType ) ).Length, $"{typeof( TraceEventType )} count matches the extension result" );
#pragma warning restore CA2263
        Xunit.Assert.True( allValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the enum values of {typeof( TraceEventType )}" );

        TraceEventType inclusionMask = TraceEventType.Critical | TraceEventType.Error | TraceEventType.Information | TraceEventType.Warning;
        TraceEventType exclusionMask = TraceEventType.Verbose;
        expectedCount = 4;
        filteredValues = TraceEventType.Critical.EnumValues().Filter( inclusionMask, exclusionMask );
        Xunit.Assert.True( filteredValues.Any(), $"{typeof( TraceEventType )} Filtered Enum Values has values" );
        _ = filteredValues.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered count matches the expected count" );
        Xunit.Assert.True( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the filtered" );
        Xunit.Assert.False( filteredValues.Contains( TraceEventType.Verbose ), $"{TraceEventType.Verbose} is Excluded in the filtered" );
    }

    private enum TraceEventTypeWithDescription
    {
        [System.ComponentModel.Description( "Critical Trace Event Type" )] Critical,
        [System.ComponentModel.Description( "Error Trace Event Type" )] Error,
        [System.ComponentModel.Description( "Information Trace Event Type" )] Information,
        [System.ComponentModel.Description( "Verbose Trace Event Type" )] Verbose
    }

    /// <summary> (Unit Test Method) tests value descriptions. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [Fact]
    public void ValueDescriptionsTest()
    {
        IEnumerable<KeyValuePair<Enum, string>> pairs = typeof( TraceEventTypeWithDescription ).ValueDescriptionPairs();
        TraceEventTypeWithDescription expectedValue = TraceEventTypeWithDescription.Critical;
        Xunit.Assert.True( pairs.Any(), $"{typeof( TraceEventTypeWithDescription )}.{nameof( EnumExtensions.ValueDescriptionPairs )} has values" );
        Xunit.Assert.True( pairs.ContainsKey( expectedValue ), $"{typeof( TraceEventTypeWithDescription )} contains {expectedValue}" );

        KeyValuePair<Enum, string> pair = pairs.SelectPair( expectedValue, expectedValue.Description() );
        // no longer working in Fluent Assertions 6.0: _ = expectedValue.Should().Be( pair.Key, $"{typeof( TraceEventTypeWithDescription )} found {expectedValue}" );
        _ = expectedValue.Should().Be( ( TraceEventTypeWithDescription ) pair.Key, $"{typeof( TraceEventTypeWithDescription )} found {expectedValue}" );

        TraceEventTypeWithDescription defaultValue = TraceEventTypeWithDescription.Error;
        string description = "incorrect Description";
        pair = pairs.SelectPair( defaultValue, "incorrect Description" );
        _ = pair.Key.Should().Be( defaultValue, $"{typeof( TraceEventTypeWithDescription )} found {defaultValue} due to {description}" );

        pairs = typeof( TraceEventTypeWithDescription ).ValueNamePairs();
        expectedValue = TraceEventTypeWithDescription.Critical;
        Xunit.Assert.True( pairs.Any(), $"{typeof( TraceEventTypeWithDescription )}.{nameof( EnumExtensions.ValueDescriptionPairs )} has value names" );
        Xunit.Assert.True( pairs.ContainsKey( expectedValue ), $"{typeof( TraceEventTypeWithDescription )} value names contains {expectedValue}" );

        string expectedDescription = TraceEventTypeWithDescription.Critical.Description();
        string descriptions = TraceEventTypeWithDescription.Critical.Descriptions();
        Xunit.Assert.True( descriptions.Length > 0, $"{typeof( TraceEventTypeWithDescription )}.{nameof( descriptions )} has description" );
        Xunit.Assert.True( descriptions.Contains( expectedDescription ), $"{typeof( TraceEventTypeWithDescription )} value descriptions contains {expectedDescription}" );

        IEnumerable<string> names = pairs.ToValues();
        Xunit.Assert.True( names.Any(), $"{typeof( TraceEventTypeWithDescription )}.{nameof( names )} has name" );

        string expectedName = TraceEventTypeWithDescription.Critical.ToString();
        Xunit.Assert.True( names.Contains( expectedName ), $"{typeof( TraceEventTypeWithDescription )} value names contains {expectedName}" );
    }

    [Fact]
    public void TraceEventTypeValueDescriptionsTest()
    {
        IEnumerable<KeyValuePair<Enum, string>> pairs = typeof( TraceEventType ).ValueDescriptionPairs();
        TraceEventType expectedValue = TraceEventType.Critical;
        Xunit.Assert.True( pairs.Any(), $"{typeof( TraceEventType )}.{nameof( EnumExtensions.ValueDescriptionPairs )} has values" );
        Xunit.Assert.True( pairs.ContainsKey( expectedValue ), $"{typeof( TraceEventType )} contains {expectedValue}" );

        KeyValuePair<Enum, string> pair = pairs.SelectPair( expectedValue, expectedValue.Description() );
        // not working in Fluent assertions 6.0: _ = expectedValue.Should().Be( pair.Key, $"{typeof( TraceEventType )} found {expectedValue}" );
        _ = expectedValue.Should().Be( ( TraceEventType ) pair.Key, $"{typeof( TraceEventType )} found {expectedValue}" );

        TraceEventType defaultValue = TraceEventType.Error;
        string description = "incorrect Description";
        pair = pairs.SelectPair( defaultValue, "incorrect Description" );
        _ = pair.Key.Should().Be( defaultValue, $"{typeof( TraceEventType )} found {defaultValue} due to {description}" );

        pairs = typeof( TraceEventType ).ValueNamePairs();
        expectedValue = TraceEventType.Critical;
        Xunit.Assert.True( pairs.Any(), $"{typeof( TraceEventType )}.{nameof( EnumExtensions.ValueDescriptionPairs )} has value names" );
        Xunit.Assert.True( pairs.ContainsKey( expectedValue ), $"{typeof( TraceEventType )} value names contains {expectedValue}" );

        string expectedDescription = TraceEventType.Critical.Description();
        string descriptions = TraceEventType.Critical.Descriptions();
        Xunit.Assert.False( descriptions.Length > 0, $"{typeof( TraceEventType )}.{nameof( descriptions )} does not have a description" );

        IEnumerable<string> names = pairs.ToValues();
        Xunit.Assert.True( names.Any(), $"{typeof( TraceEventType )}.{nameof( names )} has name" );

        string expectedName = TraceEventType.Critical.ToString();
        Xunit.Assert.True( names.Contains( expectedName ), $"{typeof( TraceEventType )} value names contains {expectedName}" );
    }

    #endregion

    #region " enum extension tests: name and description "

    [Flags]
    private enum TestArmEvents : long
    {
        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None = 0,

        /// <summary> An enum constant representing the source option. </summary>
        [System.ComponentModel.Description( "Source" )]
        Source = 1 << 1,

        [System.ComponentModel.Description( "Timer" )]
        Timer = 1 << 2

    }


    [Flags]
    private enum TestTriggerEvents : long
    {
        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None = 1 << 0,

        /// <summary> An enum constant representing the source option. </summary>
        [System.ComponentModel.Description( "Source" )]
        Source = 1 << 1,

        [System.ComponentModel.Description( "Timer" )]
        Timer = 1 << 2,

        [System.ComponentModel.Description( "Blender" )]
        Blender = 1L << 33

    }

    /// <summary>   (Unit Test Method) tests enum names. </summary>
    /// <remarks>   David, 2020-10-28. </remarks>
    [Fact]
    public void EnumNamesTest()
    {
        TraceEventType traceEvent = TraceEventType.Verbose;
        string expectedValue = "Verbose";
        string actualValue = traceEvent.ToString();
        _ = actualValue.Should().Be( expectedValue, $"ToString() of {nameof( TraceEventType.Verbose )}.{nameof( TraceEventType.Verbose )} should match" );

        actualValue = traceEvent.Names();
        _ = actualValue.Should().Be( expectedValue, $"{nameof( EnumExtensions.Names )} of {nameof( TraceEventType.Verbose )}.{nameof( TraceEventType.Verbose )} should match" );

        TestTriggerEvents triggerEvent = TestTriggerEvents.Blender;

        expectedValue = "Blender";
        actualValue = triggerEvent.Names();
        _ = actualValue.Should().Be( expectedValue, $"{nameof( EnumExtensions.Names )} of {nameof( TestTriggerEvents )}.{nameof( TestTriggerEvents.Blender )} should match" );

        TestArmEvents armEvent = TestArmEvents.Source;
        expectedValue = "Source";
        actualValue = armEvent.Names();
        _ = actualValue.Should().Be( expectedValue, $"{nameof( EnumExtensions.Names )} of {nameof( TestArmEvents )}.{nameof( TestArmEvents.Source )} should match" );

        armEvent = TestArmEvents.Source | TestArmEvents.Timer;
        expectedValue = "Source, Timer";
        actualValue = armEvent.Names();
        _ = actualValue.Should().Be( expectedValue, $"{nameof( EnumExtensions.Names )} of {nameof( TestArmEvents )}.({nameof( TestArmEvents.Source )} or {nameof( TestArmEvents.Timer )}) should match" );
    }

    /// <summary> (Unit Test Method) tests enum description. </summary>
    /// <remarks> David, 2020-10-14. </remarks>
    [Fact]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    public void EnumNameTest()
    {
        TraceEventType traceEvent = TraceEventType.Verbose;
        string expectedValue = "Verbose";
        string actualValue = traceEvent.ToString();
        _ = actualValue.Should().Be( expectedValue, $"ToString() of {nameof( TraceEventType.Verbose )}.{nameof( TraceEventType.Verbose )} should match" );

        TestTriggerEvents triggerEvent = TestTriggerEvents.Source;

        expectedValue = "Source";
        // this no longer gives the name of Blender as the Enum value is correctly set.
        actualValue = triggerEvent.ToString();
        _ = actualValue.Should().Be( expectedValue, $"ToString() value of  {nameof( TestTriggerEvents )}.{nameof( TestTriggerEvents.Source )} should match" );

        // this gives the name of Source
#pragma warning disable CA2263
        actualValue = Enum.GetName( typeof( TestTriggerEvents ), triggerEvent )!;
#pragma warning restore CA2263
        _ = actualValue.Should().Be( expectedValue, $"GetName() value of  {nameof( TestTriggerEvents )}.{nameof( TestTriggerEvents.Source )} should match" );

    }

    /// <summary> (Unit Test Method) tests enum description. </summary>
    /// <remarks> David, 2020-10-14. </remarks>
    [Fact]
    public void EnumDescriptionTest()
    {
        TraceEventType traceEvent = TraceEventType.Verbose;
        string expectedValue = "Verbose";
        string actualValue = traceEvent.DescriptionOrName();
        _ = actualValue.Should().Be( expectedValue, $"Description or name of {nameof( TraceEventType.Verbose )}.{nameof( TraceEventType.Verbose )} should match" );

        TestTriggerEvents triggerEvent = TestTriggerEvents.Source;

        expectedValue = "Source";
        actualValue = triggerEvent.Description();
        _ = actualValue.Should().Be( expectedValue, $"Description of {nameof( TestTriggerEvents )}.{nameof( TestTriggerEvents.Source )} should match" );

    }

    /// <summary>   A bit-field of flags for specifying arm sources. </summary>
    /// <remarks>   David, 2022-03-26. </remarks>
    [Flags]
    public enum TestArmSources
    {
        //
        // Summary:
        //     An enum constant representing the bus option.
        [System.ComponentModel.Description( "Bus (BUS)" )]
        Bus = 0x1,
        //
        // Summary:
        //     An enum constant representing the external option.
        [System.ComponentModel.Description( "External (EXT)" )]
        External = 0x2,
        //
        // Summary:
        //     An enum constant representing the hold option.
        [System.ComponentModel.Description( "Hold operation (HOLD)" )]
        Hold = 0x4,
        //
        // Summary:
        //     An enum constant representing the immediate option.
        [System.ComponentModel.Description( "Immediate (IMM)" )]
        Immediate = 0x8,
        //
        // Summary:
        //     An enum constant representing the manual option.
        [System.ComponentModel.Description( "Manual (MAN)" )]
        Manual = 0x10,
        //
        // Summary:
        //     An enum constant representing the timer option.
        [System.ComponentModel.Description( "Timer (TIM)" )]
        Timer = 0x20,
        //
        // Summary:
        //     Event detection for the arm layer is satisfied when either a positive-going or
        //     a negative-going pulse (via the SOT line of the Digital I/O) is received.
        [System.ComponentModel.Description( "SOT Pulsed High or Low (BSTES)" )]
        StartTestBoth = 0x40,
        //
        // Summary:
        //     Event detection for the arm layer is satisfied when a positive-going pulse (via
        //     the SOT line of the Digital I/O) is received.
        [System.ComponentModel.Description( "SOT Pulsed High (PSTES)" )]
        StartTestHigh = 0x80,
        //
        // Summary:
        //     Event detection for the arm layer is satisfied when a negative-going pulse (via
        //     the SOT line of the Digital I/O) is received.
        [System.ComponentModel.Description( "SOT Pulsed High (NSTES)" )]
        StartTestLow = 0x100,
        //
        // Summary:
        //     Event detection occurs when an input trigger via the Trigger Link input line
        //     is received. See “Trigger link,” 2400 manual page 11-19, For more information.
        //     With TLINk selected, you can Loop around the Arm Event Detector by setting the
        //     Event detector bypass.
        [System.ComponentModel.Description( "Trigger Link (TLIN)" )]
        TriggerLink = 0x200,
        //
        // Summary:
        //     An enum constant representing all option.
        [System.ComponentModel.Description( "All" )]
        All = 0x3FF
    }

    /// <summary>   Arm sources name descriptions should match. </summary>
    /// <remarks>   David, 2022-03-26. </remarks>
    [Fact]
    public void ArmSourcesNameDescriptionsShouldMatch()
    {
        TraceEventType traceEvent = TraceEventType.Verbose;

        string expectedValue = "Verbose";
        string actualValue = traceEvent.Names();
        _ = actualValue.Should().Be( expectedValue, $"{nameof( EnumExtensions.Names )} of {nameof( TraceEventType )}.{nameof( TraceEventType.Verbose )} should match" );

        TestArmSources armSource = TestArmSources.Bus;

        expectedValue = "Bus";
        actualValue = armSource.Names();
        _ = actualValue.Should().Be( expectedValue, $"{nameof( EnumExtensions.Names )} of {nameof( TestArmSources )}.{nameof( TestArmSources.Bus )} should match" );

        expectedValue = "Bus (BUS)";
        actualValue = armSource.Description();
        _ = actualValue.Should().Be( expectedValue, $"{nameof( EnumExtensions.Description )} of {nameof( TestArmSources )}.{nameof( TestArmSources.Bus )} should match" );

        expectedValue = "Bus (BUS)";
        actualValue = armSource.Descriptions();
        _ = actualValue.Should().Be( expectedValue, $"{nameof( EnumExtensions.Descriptions )} of {nameof( TestArmSources )}.{nameof( TestArmSources.Bus )} should match" );
        armSource = TestArmSources.Bus | TestArmSources.External;

        expectedValue = "Bus, External";
        actualValue = armSource.Names();
        _ = actualValue.Should().Be( expectedValue, $"{nameof( EnumExtensions.Names )} of {nameof( TestArmSources )}.({nameof( TestArmSources.Bus )} or {nameof( TestArmSources.External )}) should match" );

        expectedValue = "Bus (BUS), External (EXT)";
        actualValue = armSource.Descriptions();
        _ = actualValue.Should().Be( expectedValue, $"{nameof( EnumExtensions.Descriptions )} of {nameof( TestArmSources )}.({nameof( TestArmSources.Bus )} or {nameof( TestArmSources.External )}) should match" );
    }

    #endregion



}

