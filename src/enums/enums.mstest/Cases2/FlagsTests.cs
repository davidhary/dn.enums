namespace cc.isr.Enums.MSTest.Cases2;

/// <summary>   (Unit Test Class) the flags tests. </summary>
/// <remarks>   David, 2021-02-18. </remarks>
public class FlagsTests
{
    /// <summary>   (Unit Test Method) bit flags enum should be flags. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void BitFlagsEnumShouldBeFlags()
    {
        Xunit.Assert.True( FastEnum.IsFlags<BitFlags>() );
    }

    /// <summary>   (Unit Test Method) number enum should not be flags. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void NumberEnumShouldNotBeFlags()
    {
        Xunit.Assert.True( FastEnum.IsFlags<BitFlags>() );
        Xunit.Assert.False( FastEnum.IsFlags<Number>() );
    }

    /// <summary>   (Unit Test Method) flag combination should be valid. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void FlagCombinationShouldBeValid()
    {
        Xunit.Assert.True( BitFlags.Flag24.IsValidCombination() );
        Xunit.Assert.True( (BitFlags.Flag1 | BitFlags.Flag2).IsValidCombination() );
        Xunit.Assert.True( FastEnumExtensions.IsValidCombination<BitFlags>( 0 ) );
    }

    /// <summary>   (Unit Test Method) flag combination should not be valid. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void FlagCombinationShouldNotBeValid()
    {
        Xunit.Assert.False( (( BitFlags ) 100).IsValidCombination() );
    }

    /// <summary>   (Unit Test Method) ORed value should equal. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void OrValueShouldEqual()
    {
        Xunit.Assert.Equal( BitFlags.Flag1 | BitFlags.Flag2, BitFlags.Flag1.Or( BitFlags.Flag2 ) );
        Xunit.Assert.Equal( BitFlags.Flag1, BitFlags.Flag1.Or( BitFlags.Flag1 ) );
    }

    /// <summary>   (Unit Test Method) and values should equal. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void AndValuesShouldEqual()
    {
        Xunit.Assert.Equal( BitFlags.Flag2 & BitFlags.Flag24, BitFlags.Flag2.And( BitFlags.Flag24 ) );
        Xunit.Assert.Equal( BitFlags.Flag24 & BitFlags.Flag2, BitFlags.Flag24.And( BitFlags.Flag2 ) );
        Xunit.Assert.Equal( BitFlags.Flag1, BitFlags.Flag1.And( BitFlags.Flag1 ) );
    }

    /// <summary>   (Unit Test Method) gets used bits should equal. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void GetUsedBitsShouldEqual()
    {
        Xunit.Assert.Equal( BitFlags.Flag1 | BitFlags.Flag2 | BitFlags.Flag4, FastEnum.GetUsedBits<BitFlags>() );
    }

    /// <summary>   (Unit Test Method) inversed used bits should equal. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void InversedUsedBitsShouldEqual()
    {
        Xunit.Assert.Equal( BitFlags.Flag1, BitFlags.Flag24.UsedBitsInverse() );
        Xunit.Assert.Equal( BitFlags.Flag24, BitFlags.Flag1.UsedBitsInverse() );
    }

    /// <summary>   (Unit Test Method) inverse all bits should equal. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void InverseAllBitsShouldEqual()
    {
        Xunit.Assert.Equal( ~BitFlags.Flag1, BitFlags.Flag1.AllBitsInverse() );
        Xunit.Assert.Equal( ~BitFlags.Flag24, BitFlags.Flag24.AllBitsInverse() );
    }

    /// <summary>   (Unit Test Method) has any should be true. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void HasAnyShouldBeTrue()
    {
        Xunit.Assert.True( BitFlags.Flag2.HasAny( BitFlags.Flag24 ) );
        Xunit.Assert.True( BitFlags.Flag24.HasAny( BitFlags.Flag2 ) );
    }

    /// <summary>   (Unit Test Method) has any should be false. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void HasAnyShouldBeFalse()
    {
        Xunit.Assert.False( BitFlags.Flag2.HasAny( BitFlags.Flag1 ) );
    }

    /// <summary>   (Unit Test Method) has all should be true. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void HasAllShouldBeTrue()
    {
        Xunit.Assert.True( BitFlags.Flag24.HasAll( BitFlags.Flag2 ) );
        Xunit.Assert.True( BitFlags.Flag24.HasAll( BitFlags.Flag24 ) );
    }

    /// <summary>   (Unit Test Method) has all should be false. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void HasAllShouldBeFalse()
    {
        Xunit.Assert.False( BitFlags.Flag2.HasAll( BitFlags.Flag24 ) );
        Xunit.Assert.False( BitFlags.Flag2.HasAll( BitFlags.Flag1 ) );
    }

    #region " typeargumentexception tests (all very boring) "

    /// <summary>   Assert type argument exception. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    /// <param name="action">   The action. </param>
    private static void AssertTypeArgumentException( Action action )
    {
        _ = Xunit.Assert.Throws<TypeArgumentException>( action );
    }

    /// <summary>   (Unit Test Method) is valid combination for non flags should throw. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void IsValidCombinationForNonFlagsShouldThrow()
    {
        AssertTypeArgumentException( () => Number.Two.IsValidCombination() );
    }

    /// <summary>   (Unit Test Method) and for non flags should throw. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void AndForNonFlagsShouldThrow()
    {
        AssertTypeArgumentException( () => Number.Two.And( Number.One ) );
    }

    /// <summary>   (Unit Test Method) or for non flags should throw. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void OrForNonFlagsShouldThrow()
    {
        AssertTypeArgumentException( () => Number.Two.Or( Number.One ) );
    }

    /// <summary>   (Unit Test Method) used bits inverse for non flags should throw. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void UsedBitsInverseForNonFlagsShouldThrow()
    {
        AssertTypeArgumentException( () => Number.Two.UsedBitsInverse() );
    }

    /// <summary>   (Unit Test Method) all bits inverse for non flags should throw. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void AllBitsInverseForNonFlagsShouldThrow()
    {
        AssertTypeArgumentException( () => Number.Two.AllBitsInverse() );
    }

    /// <summary>   (Unit Test Method) is empty for non flags should throw. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void IsEmptyForNonFlagsShouldThrow()
    {
        AssertTypeArgumentException( () => Number.Two.IsEmpty() );
    }

    /// <summary>   (Unit Test Method) is not empty for non flags should throw. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void IsNotEmptyForNonFlagsShouldThrow()
    {
        AssertTypeArgumentException( () => Number.Two.IsNotEmpty() );
    }

    /// <summary>   (Unit Test Method) gets used bits for non flags should throw. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void GetUsedBitsForNonFlagsShouldThrow()
    {
        AssertTypeArgumentException( () => FastEnum.GetUsedBits<Number>() );
    }

    /// <summary>   (Unit Test Method) has any for non flags should throw. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void HasAnyForNonFlagsShouldThrow()
    {
        AssertTypeArgumentException( () => Number.One.HasAny( Number.Two ) );
    }

    /// <summary>   (Unit Test Method) has all for non flags should throw. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [Fact]
    public void HasAllForNonFlagsShouldThrow()
    {
        AssertTypeArgumentException( () => Number.One.HasAll( Number.Two ) );
    }
    #endregion
}
