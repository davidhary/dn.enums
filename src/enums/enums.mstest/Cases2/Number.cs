
namespace cc.isr.Enums.MSTest.Cases2;

internal enum Number
{
    [System.ComponentModel.Description( "First description" )]
    One = 1,
    Two = 2,
    [System.ComponentModel.Description( "Third description" )]
    Three = 3
}
