
namespace cc.isr.Enums.MSTest.Cases2;

[Flags]
internal enum BitFlags
{
    Flag1 = 1,
    [System.ComponentModel.Description( "Duplicate description" )]
    Flag2 = 2,
    [System.ComponentModel.Description( "Duplicate description" )]
    Flag4 = 4,
    Flag24 = Flag2 | Flag4
}
