using BenchmarkDotNet.Running;
using cc.isr.Enums.Benchmark;
using cc.isr.Enums.Benchmark.Scenarios;
BenchmarkSwitcher switcher = new( new[]
        {
            typeof(GetValuesBenchmark),
            typeof(GetNamesBenchmark),
            typeof(GetNameBenchmark),
            typeof(TryParseBenchmark),
            typeof(TryParseIgnoreCaseBenchmark),
            typeof(IsDefinedByteBenchmark),
            typeof(IsDefinedEnumBenchmark),
            typeof(IsDefinedStringBenchmark),
            typeof(ToStringBenchmark),
            typeof(DictionaryBenchmarks),
            typeof(DictionaryEnumKeyBenchmark),
            typeof(DictionaryInt32KeyBenchmark),
            typeof(DictionaryStringKeyBenchmark),
            typeof(EnumMemberAttributeBenchmark),
            typeof(ForEachBenchmark),
            typeof(StringEqualsBenchmark),
            typeof(InitializeBenchmark),
            typeof(HasFlagsBenchmark),
        } );
_ = switcher.Run( args, new BenchmarkConfig() );
