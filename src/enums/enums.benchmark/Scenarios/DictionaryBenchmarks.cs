using BenchmarkDotNet.Attributes;

namespace cc.isr.Enums.Benchmark.Scenarios;

#pragma warning disable IDE1006 // Naming Styles
/// <summary>   A dictionary benchmarks. </summary>
/// <remarks>   David, 2021-02-20. <para>
/// |               Method |      Mean |     Error |    StdDev | Ratio | RatioSD | </para><para>
/// |--------------------- |----------:|----------:|----------:|------:|--------:| </para><para>
/// |      Type_Dictionary | 16.413 ns | 0.2637 ns | 0.2590 ns |  1.00 |    0.00 | </para><para>
/// | Type_DictionarySlim1 |  9.623 ns | 0.0283 ns | 0.0303 ns |  0.59 |    0.01 | </para><para>
/// | Type_DictionarySlim2 | 15.946 ns | 0.1821 ns | 0.1703 ns |  0.97 |    0.02 | </para><para>
/// | Type_DictionarySlim3 | 16.354 ns | 0.0474 ns | 0.0420 ns |  1.00 |    0.02 | </para><para>
/// |       Int_Dictionary |  4.378 ns | 0.0360 ns | 0.0319 ns |  0.27 |    0.00 | </para><para>
/// |  Int_DictionarySlim1 |  3.137 ns | 0.0207 ns | 0.0203 ns |  0.19 |    0.00 | </para><para>
/// |  Int_DictionarySlim2 |  3.139 ns | 0.0160 ns | 0.0142 ns |  0.19 |    0.00 | </para><para>
/// |  Int_DictionarySlim3 |  3.484 ns | 0.0286 ns | 0.0224 ns |  0.21 |    0.00 | </para><para>
/// </para></remarks>
public class DictionaryBenchmarks
{
    private readonly Dictionary<Type, string> _typeDictionary;
    private readonly DictionarySlim1<Type, string, TypeEqualityComparer> _typeDictionarySlim1;
    private readonly DictionarySlim2<Type, string> _typeDictionarySlim2;
    private readonly DictionarySlim3<Type, string> _typeDictionarySlim3;
    private readonly Dictionary<int, string> _intDictionary;
    private readonly DictionarySlim1<int, string, DefaultEqualityComparer<int>> _intDictionarySlim1;
    private readonly DictionarySlim2<int, string> _intDictionarySlim2;
    private readonly DictionarySlim3<int, string> _intDictionarySlim3;

    public DictionaryBenchmarks()
    {
        this._typeDictionary = new Dictionary<Type, string>( 11 )
        {
            { typeof(AttributeTargets), nameof(AttributeTargets) },
            { typeof(Base64FormattingOptions), nameof(Base64FormattingOptions) },
            { typeof(ConsoleColor), nameof(ConsoleColor) },
            { typeof(ConsoleKey), nameof(ConsoleKey) },
            { typeof(ConsoleModifiers), nameof(ConsoleModifiers) },
            { typeof(ConsoleSpecialKey), nameof(ConsoleSpecialKey) },
            { typeof(DateTimeKind), nameof(DateTimeKind) },
            { typeof(DayOfWeek), nameof(DayOfWeek) },
            { typeof(EnvironmentVariableTarget), nameof(EnvironmentVariableTarget) },
            { typeof(GCCollectionMode), nameof(GCCollectionMode) },
            { typeof(GCNotificationStatus), nameof(GCNotificationStatus) }
        };
        this._typeDictionarySlim1 = new DictionarySlim1<Type, string, TypeEqualityComparer>( this._typeDictionary, this._typeDictionary.Count );
        this._typeDictionarySlim2 = new DictionarySlim2<Type, string>( this._typeDictionary, this._typeDictionary.Count );
        this._typeDictionarySlim3 = new DictionarySlim3<Type, string>( this._typeDictionary, this._typeDictionary.Count );

        this._intDictionary = new Dictionary<int, string>( 11 );
        for ( int i = 0; i < 11; ++i )
        {
            this._intDictionary.Add( i, i.ToString( System.Globalization.CultureInfo.CurrentCulture ) );
        }
        this._intDictionarySlim1 = new DictionarySlim1<int, string, DefaultEqualityComparer<int>>( this._intDictionary, this._intDictionary.Count );
        this._intDictionarySlim2 = new DictionarySlim2<int, string>( this._intDictionary, this._intDictionary.Count );
        this._intDictionarySlim3 = new DictionarySlim3<int, string>( this._intDictionary, this._intDictionary.Count );
    }

    [Benchmark( Baseline = true )]
    public bool TypeDictionary()
    {
        return this._typeDictionary.TryGetValue( typeof( DayOfWeek ), out _ );
    }

    [Benchmark]
    public bool TypeDictionarySlim1()
    {
        return this._typeDictionarySlim1.TryGetValue( typeof( DayOfWeek ), out _ );
    }

    [Benchmark]
    public bool TypeDictionarySlim2()
    {
        return this._typeDictionarySlim2.TryGetValue( typeof( DayOfWeek ), out _ );
    }

    [Benchmark]
    public bool TypeDictionarySlim3()
    {
        return this._typeDictionarySlim3.TryGetValue( typeof( DayOfWeek ), out _ );
    }

    [Benchmark]
    public bool IntDictionary()
    {
        return this._intDictionary.TryGetValue( 7, out _ );
    }

    [Benchmark]
    public bool IntDictionarySlim1()
    {
        return this._intDictionarySlim1.TryGetValue( 7, out _ );
    }

    [Benchmark]
    public bool IntDictionarySlim2()
    {
        return this._intDictionarySlim2.TryGetValue( 7, out _ );
    }

    [Benchmark]
    public bool IntDictionarySlim3()
    {
        return this._intDictionarySlim3.TryGetValue( 7, out _ );
    }

    internal sealed class DictionarySlim1<TKey, TValue, TKeyComparer>
        where TKey : notnull
        where TKeyComparer : struct, IEqualityComparer<TKey>
    {
        public readonly struct Entry( int next, TKey key, TValue value )
        {
            public readonly int Next = next;
            public readonly TKey Key = key;
            public readonly TValue Value = value;
        }

        private readonly int[] _buckets;
        internal readonly Entry[] _entries;

        public int Count { get; }

        public DictionarySlim1( IEnumerable<KeyValuePair<TKey, TValue>> dictionary, int count )
        {
            this.Count = count;
            int size = HashHelpers.PowerOf2( count );
            int[] buckets = new int[size];
            Entry[] entries = new Entry[size];
            int i = 0;
            if ( dictionary is not null )
            {
                foreach ( KeyValuePair<TKey, TValue> pair in dictionary )
                {
                    TKey value = pair.Key;
                    ref int bucket = ref buckets[value.GetHashCode() & (size - 1)];
                    entries[i] = new Entry( bucket - 1, value, pair.Value );
                    bucket = i + 1;
                    ++i;
                }
            }
            this._buckets = buckets;
            this._entries = entries;
        }

        internal bool TryGetValue( TKey key, out TValue value )
        {
            Entry[] entries = this._entries;
            for ( int i = this._buckets[key.GetHashCode() & (this._buckets.Length - 1)] - 1; i >= 0; i = entries[i].Next )
            {
                if ( default( TKeyComparer ).Equals( key, entries[i].Key ) )
                {
                    value = entries[i].Value;
                    return true;
                }
            }
            value = default!;
            return false;
        }
    }

    internal sealed class DictionarySlim2<TKey, TValue>
        where TKey : notnull
    {
        public readonly struct Entry( int next, TKey key, TValue value )
        {
            public readonly int Next = next;
            public readonly TKey Key = key;
            public readonly TValue Value = value;
        }

        private readonly int[] _buckets;
        internal readonly Entry[] _entries;

        public int Count { get; }

        public DictionarySlim2( IEnumerable<KeyValuePair<TKey, TValue>> dictionary, int count )
        {
            this.Count = count;
            int size = HashHelpers.PowerOf2( count );
            int[] buckets = new int[size];
            Entry[] entries = new Entry[size];
            int i = 0;
            if ( dictionary is not null )
            {
                foreach ( KeyValuePair<TKey, TValue> pair in dictionary )
                {
                    TKey value = pair.Key;
                    ref int bucket = ref buckets[value.GetHashCode() & (size - 1)];
                    entries[i] = new Entry( bucket - 1, value, pair.Value );
                    bucket = i + 1;
                    ++i;
                }
            }
            this._buckets = buckets;
            this._entries = entries;
        }

        internal bool TryGetValue( TKey key, out TValue value )
        {
            Entry[] entries = this._entries;
            for ( int i = this._buckets[key.GetHashCode() & (this._buckets.Length - 1)] - 1; i >= 0; i = entries[i].Next )
            {
                if ( EqualityComparer<TKey>.Default.Equals( key, entries[i].Key ) )
                {
                    value = entries[i].Value;
                    return true;
                }
            }
            value = default!;
            return false;
        }
    }

    internal sealed class DictionarySlim3<TKey, TValue>
        where TKey : notnull
    {
        public readonly struct Entry( int next, int hashCode, TKey key, TValue value )
        {
            public readonly int Next = next;
            public readonly int HashCode = hashCode;
            public readonly TKey Key = key;
            public readonly TValue Value = value;
        }

        private readonly int[] _buckets;
        internal readonly Entry[] _entries;

        public int Count { get; }

        public DictionarySlim3( IEnumerable<KeyValuePair<TKey, TValue>> dictionary, int count )
        {
            this.Count = count;
            int size = HashHelpers.PowerOf2( count );
            int[] buckets = new int[size];
            Entry[] entries = new Entry[size];
            int i = 0;
            if ( dictionary is not null )
            {
                foreach ( KeyValuePair<TKey, TValue> pair in dictionary )
                {
                    TKey value = pair.Key;
                    int hashCode = value.GetHashCode();
                    ref int bucket = ref buckets[hashCode & (size - 1)];
                    entries[i] = new Entry( bucket - 1, hashCode, value, pair.Value );
                    bucket = i + 1;
                    ++i;
                }
            }
            this._buckets = buckets;
            this._entries = entries;
        }

        internal bool TryGetValue( TKey key, out TValue value )
        {
            Entry[] entries = this._entries;
            int hashCode = key.GetHashCode();
            for ( int i = this._buckets[hashCode & (this._buckets.Length - 1)] - 1; i >= 0; i = entries[i].Next )
            {
                if ( hashCode == entries[i].HashCode && EqualityComparer<TKey>.Default.Equals( key, entries[i].Key ) )
                {
                    value = entries[i].Value;
                    return true;
                }
            }
            value = default!;
            return false;
        }
    }

    internal struct DefaultEqualityComparer<T> : IEqualityComparer<T> where T : struct, IEquatable<T>
    {
        public readonly bool Equals( T x, T y )
        {
            return x.Equals( y );
        }

        public readonly int GetHashCode( T obj )
        {
            return obj.GetHashCode();
        }
    }

    internal struct TypeEqualityComparer : IEqualityComparer<Type>
    {
        public readonly bool Equals( Type? x, Type? y )
        {
            return x is not null && x.Equals( y );
        }

        public readonly int GetHashCode( Type obj )
        {
            return obj.GetHashCode();
        }
    }

    internal static partial class HashHelpers
    {
        public static int PowerOf2( int v )
        {
            if ( (v & (v - 1)) == 0 && v >= 1 )
            {
                return v;
            }

            int i = 4;
            while ( i < v )
            {
                i <<= 1;
            }

            return i;
        }
    }
}
#pragma warning restore IDE1006 // Naming Styles

