using BenchmarkDotNet.Attributes;
using cc.isr.Enums.Benchmark.Models;
using _fastEnum = cc.isr.Enums.FastEnum;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   A try parse ignore case benchmark. </summary>
/// <remarks>
/// David, 2021-02-20. <para>
/// |       Method |     Mean | Error | Ratio | Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
/// |------------- |---------:|------:|------:|------:|------:|------:|----------:| </para><para>
/// |      NetCore | 80.20 ns |    NA |  1.00 |     - |     - |     - |         - | </para><para>
/// |     EnumsNet | 32.94 ns |    NA |  0.41 |     - |     - |     - |         - | </para><para>
/// |     FastEnum | 45.98 ns |    NA |  0.57 |     - |     - |     - |         - | </para><para>
/// Legacy:  </para><para>
/// | EnumExtender | 22.10 ns |    NA |  0.28 |     - |     - |     - |         - | </para><para>
/// |  MelodyEnums | 52.72 ns |    NA |  0.66 |     - |     - |     - |         - | </para><para>
/// </para>
/// </remarks>
public class TryParseIgnoreCaseBenchmark
{
    private const string FRUIT_NAME = nameof( Fruits.WaterMelon );

    [GlobalSetup]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void Setup()
    {
        _ = Enum.GetNames<Fruits>();
        _ = EnumsNET.Enums.GetValues<Fruits>();
        _ = _fastEnum.GetValues<Fruits>();
    }


    [Benchmark( Baseline = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool NetCore()
    {
        return Enum.TryParse<Fruits>( FRUIT_NAME, true, out _ );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool EnumsNet()
    {
        return EnumsNET.Enums.TryParse<Fruits>( FRUIT_NAME, true, out _ );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool FastEnum()
    {
        return _fastEnum.TryParse<Fruits>( FRUIT_NAME, true, out _ );
    }
}
