using BenchmarkDotNet.Attributes;
using cc.isr.Enums.Benchmark.Models;
using _fastEnum = cc.isr.Enums.FastEnum;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   A get name benchmark. </summary>
/// <remarks>   David, 2021-02-19. <para>
/// |       Method |        Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
/// |------------- |------------:|------:|------:|-------:|------:|------:|----------:| </para><para>
/// |      NetCore |  72.2193 ns |    NA | 1.000 | 0.0029 |     - |     - |      24 B | </para><para>
/// |     EnumsNet |   1.1490 ns |    NA | 0.016 |      - |     - |     - |         - | </para><para>
/// |     FastEnum |   0.2430 ns |    NA | 0.003 |      - |     - |     - |         - | </para><para>
/// Legacy:  </para><para>
/// | EnumExtender | 252.2026 ns |    NA | 3.492 | 0.0839 |     - |     - |     704 B | </para><para>
/// |  MelodyEnums |   6.8719 ns |    NA | 0.095 |      - |     - |     - |         - | </para><para>
/// </para> </remarks>
public class GetNameBenchmark
{
    private const Fruits SELECTED_FRUIT = Fruits.Pineapple;

    [GlobalSetup]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void Setup()
    {
        _ = Enum.GetNames<Fruits>();
        _ = EnumsNET.Enums.GetNames<Fruits>();
        _ = _fastEnum.GetNames<Fruits>();
        _ = SELECTED_FRUIT.HasAnyFlags();
    }


    [Benchmark( Baseline = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public string? NetCore()
    {
        return Enum.GetName<Fruits>( SELECTED_FRUIT );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public string? EnumsNet()
    {
        return EnumsNET.Enums.GetName( SELECTED_FRUIT );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public string FastEnum()
    {
        return _fastEnum.GetName( SELECTED_FRUIT );
    }
}
