using System.Reflection;
using System.Runtime.Serialization;
using BenchmarkDotNet.Attributes;
using cc.isr.Enums.Benchmark.Models;
using _fastEnum = cc.isr.Enums.FastEnum;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   An enum member attribute benchmark. </summary>
/// <remarks>   David, 2021-02-20. <para>
/// |   Method |          Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
/// |--------- |--------------:|------:|------:|-------:|------:|------:|----------:| </para><para>
/// |  NetCore | 1,462.1807 ns |    NA | 1.000 | 0.0305 |     - |     - |     264 B | </para><para>
/// | EnumsNet |     7.0265 ns |    NA | 0.005 |      - |     - |     - |         - | </para><para>
/// | FastEnum |     0.7346 ns |    NA | 0.001 |      - |     - |     - |         - | </para><para>
/// </para> </remarks>
public class EnumMemberAttributeBenchmark
{
    private const Fruits FRUIT_APPLE = Fruits.Apple;


    [GlobalSetup]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void Setup()
    {
        _ = Enum.GetNames<Fruits>();
        _ = EnumsNET.Enums.GetValues<Fruits>();
        _ = _fastEnum.GetNames<Fruits>();
    }


    [Benchmark( Baseline = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public string? NetCore()
    {
        Type type = typeof( Fruits );
        string name = Enum.GetName( type, FRUIT_APPLE )!;
        FieldInfo? info = type.GetField( name );
        EnumMemberAttribute? attr = info!.GetCustomAttribute<EnumMemberAttribute>();
        return attr?.Value;
    }


    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public string? EnumsNet()
    {
        EnumMemberAttribute? attr = EnumsNET.Enums.GetAttributes( FRUIT_APPLE )!.Get<EnumMemberAttribute>();
        return attr?.Value;
    }


    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public string? FastEnum()
    {
        return FRUIT_APPLE.GetEnumMemberValue();
    }
}
