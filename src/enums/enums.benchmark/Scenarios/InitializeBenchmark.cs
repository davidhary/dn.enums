using BenchmarkDotNet.Attributes;
using cc.isr.Enums.Benchmark.Models;
using cc.isr.Enums.Internals;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   An initialize benchmark. </summary>
/// <remarks>   David, 2021-02-20. <para>
/// |                            Method |         Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
/// |---------------------------------- |-------------:|------:|------:|-------:|------:|------:|----------:| </para><para>
/// |                 FastEnum_Init_all | 18,353.05 ns |    NA | 1.000 | 0.9155 |     - |     - |    7810 B | </para><para>
/// |                FastEnum_Init_Type |     35.70 ns |    NA | 0.002 |      - |     - |     - |         - | </para><para>
/// |              FastEnum_Init_Values |  1,215.20 ns |    NA | 0.066 | 0.0439 |     - |     - |     376 B | </para><para>
/// |               FastEnum_Init_Names |     65.05 ns |    NA | 0.004 | 0.0181 |     - |     - |     152 B | </para><para>
/// |             FastEnum_Init_Members | 11,707.83 ns |    NA | 0.638 | 0.3815 |     - |     - |    3265 B | </para><para>
/// |        FastEnum_Init_MinMaxValues |  1,667.81 ns |    NA | 0.091 | 0.0629 |     - |     - |     536 B | </para><para>
/// |             FastEnum_Init_IsFlags |  1,951.05 ns |    NA | 0.106 | 0.0343 |     - |     - |     312 B | </para><para>
/// |       FastEnum_Init_MembersByName | 12,322.42 ns |    NA | 0.671 | 0.5035 |     - |     - |    4297 B | </para><para>
/// | FastEnum_Init_UnderlyingOperation | 15,356.57 ns |    NA | 0.837 | 0.7324 |     - |     - |    6257 B | </para><para>
/// |                    Enum_GetValues |  1,222.48 ns |    NA | 0.067 | 0.0420 |     - |     - |     352 B | </para><para>
/// |                     Enum_GetNames |     54.55 ns |    NA | 0.003 | 0.0153 |     - |     - |     128 B | </para><para>
/// |                      Enum_GetName |     65.07 ns |    NA | 0.004 | 0.0029 |     - |     - |      24 B | </para><para>
/// |                    Enum_IsDefined |    145.30 ns |    NA | 0.008 | 0.0029 |     - |     - |      24 B | </para><para>
/// |                        Enum_Parse |    131.59 ns |    NA | 0.007 | 0.0029 |     - |     - |      24 B | </para><para>
/// |                     Enum_ToString |     32.82 ns |    NA | 0.002 | 0.0029 |     - |     - |      24 B | </para><para>
/// </para> </remarks>
public class InitializeBenchmark
{
    #region " fastenum "

    [Benchmark( Baseline = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0072:Add missing cases", Justification = "<Pending>" )]
    public void FastEnumInitAll()
    {
        Cache<Fruits>();

        static void Cache<T>()
            where T : struct, Enum
        {
            Type type = typeof( T );
            Type underlyingType = Enum.GetUnderlyingType( type );
            global::cc.isr.Enums.Internals.ReadOnlyArray<T> values = (( T[] ) Enum.GetValues( type )).AsReadOnly();
            ReadOnlyArray<string> names = Enum.GetNames( type ).ToReadOnlyArray();
            global::cc.isr.Enums.Internals.ReadOnlyArray<global::cc.isr.Enums.Member<T>> members = names.Select( x => new Member<T>( x ) ).ToReadOnlyArray();
            T minValue = values.DefaultIfEmpty().Min();
            T maxValue = values.DefaultIfEmpty().Max();
            bool isEmpty = values.Count == 0;
            bool isFlags = Attribute.IsDefined( type, typeof( FlagsAttribute ) );
            global::cc.isr.Enums.Member<T>[] distinctMembers = members.Distinct( new Member<T>.ValueComparer() ).ToArray();
            global::cc.isr.Enums.Internals.FrozenDictionary<T, global::cc.isr.Enums.Member<T>> memberByValue = distinctMembers.ToFrozenDictionary( x => x.Value );
            global::cc.isr.Enums.Internals.FrozenStringKeyDictionary<global::cc.isr.Enums.Member<T>> memberByName = members.ToFrozenStringKeyDictionary( x => x.Name );
            global::cc.isr.Enums.Internals.IUnderlyingOperation<T> underlyingOperation
                = Type.GetTypeCode( type ) switch
                {
                    TypeCode.SByte => SByteOperation<T>.Create( minValue, maxValue, distinctMembers ),
                    TypeCode.Byte => ByteOperation<T>.Create( minValue, maxValue, distinctMembers ),
                    TypeCode.Int16 => Int16Operation<T>.Create( minValue, maxValue, distinctMembers ),
                    TypeCode.UInt16 => UInt16Operation<T>.Create( minValue, maxValue, distinctMembers ),
                    TypeCode.Int32 => Int32Operation<T>.Create( minValue, maxValue, distinctMembers ),
                    TypeCode.UInt32 => UInt32Operation<T>.Create( minValue, maxValue, distinctMembers ),
                    TypeCode.Int64 => Int64Operation<T>.Create( minValue, maxValue, distinctMembers ),
                    TypeCode.UInt64 => UInt64Operation<T>.Create( minValue, maxValue, distinctMembers ),
                    _ => throw new InvalidOperationException(),
                };
        }
    }


    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void FastEnumInitType()
    {
        Cache<Fruits>();

        static void Cache<T>()
            where T : struct, Enum
        {
            Type type = typeof( T );
            _ = Enum.GetUnderlyingType( type );
        }
    }


    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void FastEnumInitValues()
    {
        Cache<Fruits>();

        static void Cache<T>()
            where T : struct, Enum
        {
            Type type = typeof( T );
            T[] values = ( T[] ) Enum.GetValues( type );
            _ = values.AsReadOnly();
            _ = values.Length == 0;
        }
    }


    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void FastEnumInitNames()
    {
        Cache<Fruits>();

        static void Cache<T>()
            where T : struct, Enum
        {
            Type type = typeof( T );
            _ = Enum.GetNames( type ).ToReadOnlyArray();
        }
    }


    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void FastEnumInitMembers()
    {
        Cache<Fruits>();

        static void Cache<T>()
            where T : struct, Enum
        {
            Type type = typeof( T );
            ReadOnlyArray<string> names = Enum.GetNames( type ).ToReadOnlyArray();
            global::cc.isr.Enums.Internals.ReadOnlyArray<global::cc.isr.Enums.Member<T>> members
                = names
                .Select( x => new Member<T>( x ) )
                .ToReadOnlyArray();
        }
    }


    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void FastEnumInitMinMaxValues()
    {
        Cache<Fruits>();

        static void Cache<T>()
            where T : struct, Enum
        {
            Type type = typeof( T );
            T[] values = ( T[] ) Enum.GetValues( type );
            global::cc.isr.Enums.Internals.ReadOnlyArray<T> values2 = values.AsReadOnly();
            _ = values.Length == 0;
            _ = values2.DefaultIfEmpty().Min();
            _ = values2.DefaultIfEmpty().Max();
        }
    }


    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void FastEnumInitIsFlags()
    {
        Cache<Fruits>();

        static void Cache<T>()
            where T : struct, Enum
        {
            Type type = typeof( T );
            _ = Attribute.IsDefined( type, typeof( FlagsAttribute ) );
        }
    }


    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void FastEnumInitMembersByName()
    {
        Cache<Fruits>();

        static void Cache<T>()
            where T : struct, Enum
        {
            Type type = typeof( T );
            ReadOnlyArray<string> names = Enum.GetNames( type ).ToReadOnlyArray();
            global::cc.isr.Enums.Internals.ReadOnlyArray<global::cc.isr.Enums.Member<T>> members
                = names
                .Select( x => new Member<T>( x ) )
                .ToReadOnlyArray();

            global::cc.isr.Enums.Internals.FrozenStringKeyDictionary<global::cc.isr.Enums.Member<T>> memberByName = members.ToFrozenStringKeyDictionary( x => x.Name );
        }
    }


    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0072:Add missing cases", Justification = "<Pending>" )]
    public void FastEnumInitUnderlyingOperation()
    {
        Cache<Fruits>();

        static void Cache<T>()
            where T : struct, Enum
        {
            Type type = typeof( T );
            T[] values = ( T[] ) Enum.GetValues( type );
            global::cc.isr.Enums.Internals.ReadOnlyArray<T> values2 = values.AsReadOnly();
            bool isEmpty = values.Length == 0;
            T min = values2.DefaultIfEmpty().Min();
            T max = values2.DefaultIfEmpty().Max();
            ReadOnlyArray<string> names = Enum.GetNames( type ).ToReadOnlyArray();
            global::cc.isr.Enums.Internals.ReadOnlyArray<global::cc.isr.Enums.Member<T>> members
                = names
                .Select( x => new Member<T>( x ) )
                .ToReadOnlyArray();
            global::cc.isr.Enums.Member<T>[] distinct = members.OrderBy( x => x.Value ).Distinct( new Member<T>.ValueComparer() ).ToArray();
            global::cc.isr.Enums.Internals.IUnderlyingOperation<T> underlyingOperation
                = Type.GetTypeCode( type ) switch
                {
                    TypeCode.SByte => SByteOperation<T>.Create( min, max, distinct ),
                    TypeCode.Byte => ByteOperation<T>.Create( min, max, distinct ),
                    TypeCode.Int16 => Int16Operation<T>.Create( min, max, distinct ),
                    TypeCode.UInt16 => UInt16Operation<T>.Create( min, max, distinct ),
                    TypeCode.Int32 => Int32Operation<T>.Create( min, max, distinct ),
                    TypeCode.UInt32 => UInt32Operation<T>.Create( min, max, distinct ),
                    TypeCode.Int64 => Int64Operation<T>.Create( min, max, distinct ),
                    TypeCode.UInt64 => UInt64Operation<T>.Create( min, max, distinct ),
                    _ => throw new InvalidOperationException(),
                };
        }
    }

    #endregion

    #region " enum "

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    public void EnumGetValues()
    {
        _ = Enum.GetValues<Fruits>();
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void EnumGetNames()
    {
        _ = Enum.GetNames<Fruits>();
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void EnumGetName()
    {
        _ = Enum.GetName<Fruits>( Fruits.Lemon );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void EnumIsDefined()
    {
        _ = Enum.IsDefined<Fruits>( Fruits.Melon );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void EnumParse()
    {
        _ = Enum.Parse<Fruits>( "Apple" );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void EnumToString()
    {
        _ = Fruits.Grape.ToString();
    }
    #endregion
}
