using System.Collections.ObjectModel;
using BenchmarkDotNet.Attributes;
using cc.isr.Enums.Internals;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   for each benchmark. </summary>
/// <remarks>   David, 2021-02-20. <para>
/// |             Method |      Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
/// |------------------- |----------:|------:|------:|-------:|------:|------:|----------:| </para><para>
/// |              Array |  35.04 ns |    NA |  1.00 |      - |     - |     - |         - | </para><para>
/// |               List | 196.77 ns |    NA |  5.62 |      - |     - |     - |         - | </para><para>
/// | ReadOnlyCollection | 595.99 ns |    NA | 17.01 | 0.0048 |     - |     - |      40 B | </para><para>
/// |      IReadOnlyList | 380.08 ns |    NA | 10.85 | 0.0038 |     - |     - |      32 B | </para><para>
/// |      ReadOnlyArray |  40.88 ns |    NA |  1.17 |      - |     - |     - |         - | </para><para>
/// </para> </remarks>
public class ForEachBenchmark
{
#pragma warning disable CS8618
#pragma warning disable IDE1006 // Naming Styles
    private ReadOnlyArray<int> _readOnlyArray { get; set; }
    private ReadOnlyCollection<int> _readOnlyCollection { get; set; }
    private IReadOnlyList<int> _iReadOnlyList { get; set; }
    private List<int> _list { get; set; }
    private int[] _array { get; set; }
#pragma warning restore CS8618
#pragma warning restore IDE1006 // Naming Styles

    [GlobalSetup]
    public void Setup()
    {
        IEnumerable<int> raw = Enumerable.Range( 1, 100 );
        this._readOnlyArray = new ReadOnlyArray<int>( raw.ToArray() );
        this._readOnlyCollection = new ReadOnlyCollection<int>( raw.ToList() );
        this._iReadOnlyList = raw.ToArray();
        this._list = raw.ToList();
        this._array = raw.ToArray();
    }


    [Benchmark( Baseline = true )]
    public int Array()
    {
        int sum = 0;
        foreach ( int x in this._array )
            sum += x;
        return sum;
    }


    [Benchmark]
    public int List()
    {
        int sum = 0;
        foreach ( int x in this._list )
            sum += x;
        return sum;
    }


    [Benchmark]
    public int ReadOnlyCollection()
    {
        int sum = 0;
        foreach ( int x in this._readOnlyCollection )
            sum += x;
        return sum;
    }


    [Benchmark]
    public int IReadOnlyList()
    {
        int sum = 0;
        foreach ( int x in this._iReadOnlyList )
            sum += x;
        return sum;
    }


    [Benchmark]
    public int ReadOnlyArray()
    {
        int sum = 0;
        foreach ( int x in this._readOnlyArray )
            sum += x;
        return sum;
    }
}
