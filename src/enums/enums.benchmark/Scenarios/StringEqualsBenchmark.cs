using BenchmarkDotNet.Attributes;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   A <see cref="string" /> equals benchmark. </summary>
/// <remarks>   David, 2021-02-20. <para>
/// |                           Method |      Mean | Error | Ratio | Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
/// |--------------------------------- |----------:|------:|------:|------:|------:|------:|----------:| </para><para>
/// |          StringOrdinalIgnoreCase |  16.50 ns |    NA |  1.00 |     - |     - |     - |         - | </para><para>
/// |            SpanOrdinalIgnoreCase |  16.79 ns |    NA |  1.02 |     - |     - |     - |         - | </para><para>
/// | StringInvariantCultureIgnoreCase | 312.58 ns |    NA | 18.95 |     - |     - |     - |         - | </para><para>
/// |   SpanInvariantCultureIgnoreCase | 322.10 ns |    NA | 19.52 |     - |     - |     - |         - | </para><para>
/// </para></remarks>
public class StringEqualsBenchmark
{
    private string Lower { get; set; } = string.Empty;
    private string Upper { get; set; } = string.Empty;

    [GlobalSetup]
    public void Setup()
    {
        string text = Guid.NewGuid().ToString();
        this.Lower = text.ToLower( System.Globalization.CultureInfo.CurrentCulture );
        this.Upper = text.ToUpper( System.Globalization.CultureInfo.CurrentCulture );
    }


    [Benchmark( Baseline = true )]
    public bool StringOrdinalIgnoreCase()
    {
        return this.Lower.Equals( this.Upper, StringComparison.OrdinalIgnoreCase );
    }

    [Benchmark]
    public bool SpanOrdinalIgnoreCase()
    {
        ReadOnlySpan<char> lower = this.Lower.AsSpan();
        ReadOnlySpan<char> upper = this.Upper.AsSpan();
        return lower.Equals( upper, StringComparison.OrdinalIgnoreCase );
    }


    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA1309:Use ordinal string comparison", Justification = "<Pending>" )]
    public bool StringInvariantCultureIgnoreCase()
    {
        return this.Lower.Equals( this.Upper, StringComparison.InvariantCultureIgnoreCase );
    }

    [Benchmark]
    public bool SpanInvariantCultureIgnoreCase()
    {
        ReadOnlySpan<char> lower = this.Lower.AsSpan();
        ReadOnlySpan<char> upper = this.Upper.AsSpan();
        return lower.Equals( upper, StringComparison.InvariantCultureIgnoreCase );
    }
}
