using BenchmarkDotNet.Attributes;
using cc.isr.Enums.Benchmark.Models;
using _fastEnum = cc.isr.Enums.FastEnum;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   A get names benchmark. </summary>
/// <remarks>   David, 2021-02-19. <para>
/// |       Method |       Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
/// |------------- |-----------:|------:|------:|-------:|------:|------:|----------:| </para><para>
/// |      NetCore | 57.9857 ns |    NA | 1.000 | 0.0153 |     - |     - |     128 B | </para><para>
/// |     EnumsNet |  1.5934 ns |    NA | 0.027 |      - |     - |     - |         - | </para><para>
/// |     FastEnum |  0.0148 ns |    NA | 0.000 |      - |     - |     - |         - | </para><para>
/// Legacy:  </para><para>
/// | EnumExtender | 44.8352 ns |    NA | 0.773 | 0.0191 |     - |     - |     160 B | </para><para>
/// |  MelodyEnums |  0.0011 ns |    NA | 0.000 |      - |     - |     - |         - | </para><para>
/// </para></remarks>
public class GetNamesBenchmark
{
    [GlobalSetup]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void Setup()
    {
        _ = Enum.GetNames<Fruits>();
        _ = EnumsNET.Enums.GetNames<Fruits>();
        _ = _fastEnum.GetNames<Fruits>();
    }


    [Benchmark( Baseline = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public IReadOnlyList<string> NetCore()
    {
        return Enum.GetNames<Fruits>();
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public IReadOnlyList<string> EnumsNet()
    {
        return EnumsNET.Enums.GetNames<Fruits>();
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public IReadOnlyList<string> FastEnum()
    {
        return _fastEnum.GetNames<Fruits>();
    }
}
