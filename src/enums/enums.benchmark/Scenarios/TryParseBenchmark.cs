using BenchmarkDotNet.Attributes;
using cc.isr.Enums.Benchmark.Models;
using _fastEnum = cc.isr.Enums.FastEnum;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   A try parse benchmark. </summary>
/// <remarks>   David, 2021-02-19. <para>
/// |       Method |     Mean | Error | Ratio | Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
/// |------------- |---------:|------:|------:|------:|------:|------:|----------:| </para><para>
/// |      NetCore | 77.01 ns |    NA |  1.00 |     - |     - |     - |         - | </para><para>
/// |     EnumsNet | 31.77 ns |    NA |  0.41 |     - |     - |     - |         - | </para><para>
/// |     FastEnum | 17.01 ns |    NA |  0.22 |     - |     - |     - |         - | </para><para>
/// Legacy:  </para><para>
/// | EnumExtender | 23.71 ns |    NA |  0.31 |     - |     - |     - |         - | </para><para>
/// |  MelodyEnums | 51.76 ns |    NA |  0.67 |     - |     - |     - |         - | </para><para>
/// </para></remarks>
public class TryParseBenchmark
{
    private const string FRUIT_NAME = nameof( Fruits.WaterMelon );

    [GlobalSetup]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void Setup()
    {
        _ = Enum.GetNames<Fruits>();
        _ = EnumsNET.Enums.GetValues<Fruits>();
        _ = _fastEnum.GetValues<Fruits>();
    }


    [Benchmark( Baseline = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool NetCore()
    {
        return Enum.TryParse<Fruits>( FRUIT_NAME, out _ );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool EnumsNet()
    {
        return EnumsNET.Enums.TryParse<Fruits>( FRUIT_NAME, out _ );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool FastEnum()
    {
        return _fastEnum.TryParse<Fruits>( FRUIT_NAME, out _ );
    }
}
