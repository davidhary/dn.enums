using BenchmarkDotNet.Attributes;
using cc.isr.Enums.Benchmark.Models;
using _fastEnum = cc.isr.Enums.FastEnum;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   to string benchmark. </summary>
/// <remarks>   David, 2021-02-20. <para>
/// |       Method |        Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | <para></para>
/// |------------- |------------:|------:|------:|-------:|------:|------:|----------:| <para></para>
/// |      NetCore |  31.5195 ns |    NA | 1.000 | 0.0029 |     - |     - |      24 B | <para></para>
/// |     FastEnum |   0.2555 ns |    NA | 0.008 |      - |     - |     - |         - | <para></para>
/// Legacy:  </para><para>
/// | EnumExtender | 273.5365 ns |    NA | 8.678 | 0.0839 |     - |     - |     704 B | <para></para>
/// |  MelodyEnums |   6.8177 ns |    NA | 0.216 |      - |     - |     - |         - | <para></para>
/// </para></remarks>
public class ToStringBenchmark
{
    private const Fruits FRUIT_PINEAPPLE = Fruits.Pineapple;


    [GlobalSetup]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void Setup()
    {
        _ = Enum.GetNames<Fruits>();
        _ = _fastEnum.GetValues<Fruits>();
    }


    [Benchmark( Baseline = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public string NetCore()
    {
        return FRUIT_PINEAPPLE.ToString();
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public string FastEnum()
    {
        return FRUIT_PINEAPPLE.ToName();
    }
}
