using BenchmarkDotNet.Attributes;
using cc.isr.Enums.Benchmark.Models;
using _fastEnum = cc.isr.Enums.FastEnum;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   The is defined byte benchmark. </summary>
/// <remarks>   David, 2021-02-20. <para>
/// |   Method |       Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
/// |--------- |-----------:|------:|------:|-------:|------:|------:|----------:| </para><para>
/// |  NetCore | 120.134 ns |    NA |  1.00 | 0.0029 |     - |     - |      24 B | </para><para>
/// | FastEnum |   3.362 ns |    NA |  0.03 |      - |     - |     - |         - | </para><para>
/// </para> </remarks>
public class IsDefinedByteBenchmark
{
    private const byte FRUIT_CHERRY = ( byte ) Fruits.Cherry;

    [GlobalSetup]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void Setup()
    {
        _ = Enum.GetNames<Fruits>();
        _ = _fastEnum.GetValues<Fruits>();
    }


    [Benchmark( Baseline = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool NetCore()
    {
        return Enum.IsDefined( typeof( Fruits ), FRUIT_CHERRY );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void FastEnum()
    {
        _ = _fastEnum.IsDefined<Fruits>( FRUIT_CHERRY );
    }
}
