using BenchmarkDotNet.Attributes;
using _fastEnum = cc.isr.Enums.FastEnum;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   The is defined enum benchmark. </summary>
/// <remarks>   David, 2021-02-20. <para>
/// |       Method |          Mean | Error |      Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
/// |------------- |--------------:|------:|-----------:|-------:|------:|------:|----------:| </para><para>
/// |      NetCore |     0.0996 ns |    NA |      1.000 |      - |     - |     - |         - | </para><para>
/// |     EnumsNet |     0.1615 ns |    NA |      1.621 |      - |     - |     - |         - | </para><para>
/// | EnumsNetType |    15.6774 ns |    NA |    157.357 | 0.0029 |     - |     - |      24 B | </para><para>
/// |     FastEnum |     0.2309 ns |    NA |      2.317 |      - |     - |     - |         - | </para><para>
/// |       Manual |     0.0000 ns |    NA |      0.000 |      - |     - |     - |         - | </para><para>
/// Legacy:  </para><para>
/// | EnumExtender | 8,228.5461 ns |    NA | 82,591.278 | 0.0763 |     - |     - |     712 B | </para><para>
/// |  MelodyEnums |     7.5806 ns |    NA |     76.087 |      - |     - |     - |         - | </para><para>
///
/// </para><para>
/// </para></remarks>
public class HasFlagsBenchmark
{
    private const AttributeTargets METHOD_ATTRIBUTE_TARGET = AttributeTargets.Method;
    private Type _enumType = typeof( AttributeTargets );


    [GlobalSetup]
    public void Setup()
    {
        this._enumType = typeof( AttributeTargets );
        _ = Enum.GetNames<AttributeTargets>();
        _ = EnumsNET.Enums.GetValues<AttributeTargets>();
        _ = _fastEnum.GetValues<AttributeTargets>();
    }


    [Benchmark( Baseline = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool NetCore()
    {
        return METHOD_ATTRIBUTE_TARGET.HasFlag( METHOD_ATTRIBUTE_TARGET );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool EnumsNet()
    {
        return METHOD_ATTRIBUTE_TARGET.HasAnyFlags( METHOD_ATTRIBUTE_TARGET );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool EnumsNetType()
    {
        return EnumsNET.FlagEnums.HasAnyFlags( this._enumType, METHOD_ATTRIBUTE_TARGET );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool FastEnum()
    {
        return FastEnumExtensions.HasAnyFlags( METHOD_ATTRIBUTE_TARGET );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool Manual()
    {
        return METHOD_ATTRIBUTE_TARGET == (METHOD_ATTRIBUTE_TARGET & METHOD_ATTRIBUTE_TARGET);
    }
}
