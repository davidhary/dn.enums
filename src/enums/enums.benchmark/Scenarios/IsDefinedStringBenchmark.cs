using BenchmarkDotNet.Attributes;
using cc.isr.Enums.Benchmark.Models;
using _fastEnum = cc.isr.Enums.FastEnum;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   The is defined string benchmark. </summary>
/// <remarks>   David, 2021-02-20. <para>
/// |   Method |     Mean | Error | Ratio | Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
/// |--------- |---------:|------:|------:|------:|------:|------:|----------:| </para><para>
/// |  NetCore | 99.52 ns |    NA |  1.00 |     - |     - |     - |         - | </para><para>
/// | FastEnum | 16.38 ns |    NA |  0.16 |     - |     - |     - |         - | </para><para>
/// </para> </remarks>
public class IsDefinedStringBenchmark
{
    private const string FRUIT_PINEAPPLE = nameof( Fruits.Pineapple );


    [GlobalSetup]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public void Setup()
    {
        _ = Enum.GetNames<Fruits>();
        _ = _fastEnum.GetValues<Fruits>();
    }


    [Benchmark( Baseline = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool NetCore()
    {
        return Enum.IsDefined( typeof( Fruits ), FRUIT_PINEAPPLE );
    }

    [Benchmark]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    public bool FastEnum()
    {
        return _fastEnum.IsDefined<Fruits>( FRUIT_PINEAPPLE );
    }
}
