using BenchmarkDotNet.Attributes;
using cc.isr.Enums.Benchmark.Models;
using cc.isr.Enums.Internals;

namespace cc.isr.Enums.Benchmark.Scenarios;

/// <summary>   A dictionary int 32 key benchmark. </summary>
/// <remarks>   David, 2021-02-19. <para>
/// |                 Method |      Mean | Error | Ratio | Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
/// |----------------------- |----------:|------:|------:|------:|------:|------:|----------:| </para><para>
/// |             Dictionary | 4.8877 ns |    NA |  1.00 |     - |     - |     - |         - | </para><para>
/// |       FrozenDictionary | 0.9238 ns |    NA |  0.19 |     - |     - |     - |         - | </para><para>
/// | FrozenIntKeyDictionary | 0.6889 ns |    NA |  0.14 |     - |     - |     - |         - | </para><para>
/// </para></remarks>
public class DictionaryInt32KeyBenchmark
{
    private const int _lookupKey = ( int ) Fruits.Pear;

#pragma warning disable CS8618
    private Dictionary<int, Member<Fruits>> Standard { get; set; }
    private FrozenDictionary<int, Member<Fruits>> GenericsKeyFrozen { get; set; }
    private FrozenInt32KeyDictionary<Member<Fruits>> IntKeyFrozen { get; set; }
#pragma warning restore CS8618


    [GlobalSetup]
    public void Setup()
    {
        IReadOnlyList<Member<Fruits>> members = FastEnum.GetMembers<Fruits>();
        this.Standard = members.ToDictionary( x => ( int ) x.Value );
        this.GenericsKeyFrozen = members.ToFrozenDictionary( x => ( int ) x.Value );
        this.IntKeyFrozen = members.ToFrozenInt32KeyDictionary( x => ( int ) x.Value );
    }


    [Benchmark( Baseline = true )]
    public bool Dictionary()
    {
        return this.Standard.TryGetValue( _lookupKey, out _ );
    }

    [Benchmark]
    public bool FrozenDictionary()
    {
        return this.GenericsKeyFrozen.TryGetValue( _lookupKey, out _ );
    }

    [Benchmark]
    public bool FrozenIntKeyDictionary()
    {
        return this.IntKeyFrozen.TryGetValue( _lookupKey, out _ );
    }
}
